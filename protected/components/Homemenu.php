<?php
Yii::import('zii.widgets.CPortlet');
class Homemenu extends CWidget
{
  
    public function run()
    {
			$model= new  HomeLinks;
			$main=$model->findAll(array('condition'=>'parent_id="0" and status="0"','order'=>'id,priority asc','limit'=>'7'));
			$sub=$model->findAll(array('condition'=>'parent_id!="0" and status="0"','order'=>'priority asc'));
			$this->render('homemenu',array('main'=>$main,'sub'=>$sub));
    }
}
?>
