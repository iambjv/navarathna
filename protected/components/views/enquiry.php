 <div id="sss"></div>
  <?php $form=$this->beginWidget('CActiveForm',  array(
          'id'=>'person-form-edit_person-form',
           'enableClientValidation'=>true,
            'clientOptions' => array(
      'validateOnSubmit'=>true,
      
         ),
                
)); ?>
 
 
	<div class="row">
		<div class="inp">
			<?php echo $form->textField($model,'contact_name',array("class"=>"log_fld","placeholder"=>"Name")); ?>
	     	<div class="erm"><?php echo $form->error($model,'contact_name'); ?></div>
		</div>
	</div>
    <div class="clear"></div>
	<div class="row">
		<div class="inp"><?php echo $form->textField($model,'contact_email',array("class"=>"log_fld","placeholder"=>"Email")); ?>
		<div class="erm"><?php echo $form->error($model,'contact_email'); ?></div>
		</div>
	</div>
    <div class="clear"></div>
    
	<div class="row">
		<div class="inp">
		<?php echo $form->textField($model,'contact_number',array("class"=>"log_fld","placeholder"=>"Contact Number")); ?>
		<div class="erm"><?php echo $form->error($model,'contact_number'); ?></div>
		</div>
	</div>
    <div class="clear"></div>
    
	<div class="row">
		<div class="inp"><?php echo $form->textArea($model,'message',array('style'=>"height:100px;","class"=>"log_fld","placeholder"=>"Comment")); ?>
		<div class="erm"><?php echo $form->error($model,'message'); ?></div>
		</div>
		
	</div>
   <div class="clear"></div>
	 	<?php if(CCaptcha::checkRequirements()): ?>
	<div class="row">
		<div>
		<?php $this->widget('CCaptcha',
                          array('showRefreshButton'=>true,
                                'buttonType'=>'button',
                                'buttonOptions'=>
                                                    array('type'=>'image',
                                                          'src'=>Yii::app()->request->baseUrl."/images/Very-Basic-Refresh-icon.png",
                                                          'style'=>"width:15px!important;;",
                                                    ),                                                            
                                'buttonLabel'=>'Refrescar imagen'),
                          false); ?>
		
		</div>
		<div><?php echo $form->textField($model,'verifyCode',array("class"=>"log_fld","style"=>"max-width:70px;min-width:70px;","placeholder"=>"Code")); ?></div>
		<div class="erm"><?php echo $form->error($model,'verifyCode'); ?></div>
		<div class="hint">Please enter the letters as they are shown in the image above.
		<br/>Letters are not case-sensitive.</div>
	</div>
	<?php endif; ?>
	 

	   <div class="form-group">
						       <label  class = 'control-label col-lg-4'>&nbsp;</label>   
                        <div class="col-lg-8">
						 <?php echo CHtml::button('Submit',array('class'=>'sb',"onclick"=>"send()")); ?>
                        </div>
                         </div>
<?php $this->endWidget(); ?>
