<?php
//echo $linkurl = Yii::app()->request->url;
$url = Yii::app()->request->getPathInfo();
if($url=="")
{
		$url="Home";
}	
if(!empty($main))
{
	foreach($main as $key => $val)
	{
		if($val['link_name']==$url)
		{
			$class = "active";
		}
		else
		{
			$class = "";
		}			
?>
	<li class="<?=$class?>" >
	    <?php
	    if($val['id']=='27')
		{
				$hrefs = $val['link_url'];
				$targets = "_blank";
		}
		else
		{
				$hrefs = Yii::app()->request->baseUrl.'/'.$val['link_url'];
				$targets = "";
		}
	    
	    ?>
		<a href="<?= $hrefs; ?>" target="<?= $targets; ?>" <?php if(count($main)==$key+1){ ?> style="background-image:none;" <?php }else{ ?> style="" <?php } ?>><?php echo $val->link_name; ?></a>
		<?php 
		if(!empty($sub))
		{
				
		?>
		<ul>
			<?php
			foreach($sub as $Skey => $Sval)
			{
				if($val['id']==$Sval['parent_id'])
				{
					if($Sval['parent_id']=='15' || $Sval['id']=='34' || $Sval['id']=='11')
					{
						$href = $Sval['link_url'];
						$target = "_blank";
					}
					else
					{
						$href = Yii::app()->request->baseUrl.'/'.$Sval['link_url'];
						$target = "";
					}
			?>
			<li><a href="<?= $href; ?>" target="<?= $target; ?>"><?=$Sval->link_name?></a></li>
			<?php
		}
		}
		?>
		</ul>
		<?php
	}

	?>
	</li>
	
<?php
	}
}
?>
