<?php
Yii::import('zii.widgets.CPortlet');
class Searchlinks extends  CPortlet
{
  
    public function run()
    {
			$brand= new ProductBrand;
			$fabric= new ProductFabric;
			$category = new ProductCategory;
			$collar = new ProductCollar;
			$ProductOccasion= new ProductOccasion;
			$ProductSleeve=  new ProductSleeve;
			$ProductFit=new ProductFit;
			$ProductBrandfit = new ProductBrandfit;
			$ProductPattern= new ProductPattern;
			$color= new ProductColor;
			$size= new ProductSize;
			$brand= $brand->with('ProductCount')->findAll(array("condition"=>"t.status='1'","order"=>"brand_name"));
			$category = $category->with('ProductCount')->findAll(array("condition"=>"status='1'","order"=>"category_name"));
			$fabric= $fabric->findAll(array("condition"=>"status='1'","order"=>"fabric_name"));
			$collar = $collar->findAll(array("condition"=>"status='1'","order"=>"collar_name"));
			$ProductOccasion =$ProductOccasion->findAll(array("condition"=>"status='1'","order"=>"occasion_name"));  
			$ProductSleeve=$ProductSleeve->findAll(array("condition"=>"status='1'","order"=>"sleeve_name"));  
			$ProductFit=$ProductFit->findAll(array("condition"=>"status='1'","order"=>"fit_name")); 
			$ProductBrandfit = $ProductBrandfit->findAll(array("condition"=>"status='1'","order"=>"brandfit_name"));   
			$ProductPattern   = $ProductPattern->findAll(array("condition"=>"status='1'","order"=>"pattern_name"));   
			$color = $color->findAll(array("condition"=>"status='1'","order"=>"color_name"));  
			$size=$size->findAll(array("condition"=>"status='1'","order"=>"size_name"))  ;
            $this->render('searchlinks',array('brand'=>$brand,'fabric'=>$fabric,'category'=>$category,'collar'=>$collar,'ProductOccasion'=>$ProductOccasion,'ProductSleeve'=>$ProductSleeve,'ProductFit'=>$ProductFit,'ProductBrandfit'=>$ProductBrandfit,'ProductPattern'=>$ProductPattern,'color'=>$color,'size'=>$size));
    }
}
?>
