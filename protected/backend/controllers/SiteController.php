<?php
class SiteController extends Controller
{ 
	public $layout='main';

	/**
	 * Declares class-based actions.
	 */
	public function beforeAction($action)
	{
	
	  //Yii::app()->user->logout(); 
		if(!isset(Yii::app()->user->roleid) and Yii::app()->request->getPathInfo()!='admin')
		{
			 
			//Yii::app()->user->setState("urls",Yii::app()->request->url);
			$this->redirect(Yii::app()->user->loginUrl);
		}
		else
		{
			 
			return true;
		}
    
     
	}
	public function actions()
	{  
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	/* 
	public function actionError()
	{
	    if($error=Yii::app()->errorHandler->error)
	    {
	    	if(Yii::app()->request->isAjaxRequest)
	    		echo $error['message'];
	    	else
	        	$this->render('error', $error);
	    }
	}
    */

	/**
	 * Displays the contact page
	 */
 
	public function actionContact()
	{
		 $this->layout="column2";
		 $model=new ContactForm;
		if(isset($_POST['ContactForm']))
		{
			$model->attributes=$_POST['ContactForm'];
			if($model->validate())
			{
				$headers="From: {$model->email}\r\nReply-To: {$model->email}";
				mail(Yii::app()->params['adminEmail'],$model->subject,$model->body,$headers);
				Yii::app()->user->setFlash('contact','Thank you for contacting us. We will respond to you as soon as possible.');
				$this->refresh();
			}
		}
		$this->render('contact',array('model'=>$model));
	}
	public function actionAdduser($id=null)
	{
		 
		$this->layout="column2";
		$model=new User;
		if($id)
		{
			$model=$model->findByPk($id);
			
			$model->setScenario("update");
			$msg="Edited";
			
		}
		else
		{
			$model=new User;
			$model->setScenario("insert");
			$msg="Added";
	    }
		
		
		if(Yii::app()->request->isPostRequest)
		{
			$model->attributes=$_POST['User'];
			
			$rnd = rand(0,9999);
			$uploadedFile=CUploadedFile::getInstance($model,'image');
			
			if($model->validate())
			{
			 if($model->id)
			 {
				 unset($model->password);
		    
		     }
		     else
		     {
			 $pass=$model->password;
			 $model->password= md5($pass);
			 $model->conpassword= md5($pass);
			 }
			 
			 if(!empty($uploadedFile))
			 {
			 $fileName = "{$rnd}-{$uploadedFile}";   
             $model->image = $fileName;
		     }
		
			 if($model->save())	
			 { 
				if(!empty($uploadedFile))
				{
			    $uploadedFile->saveAs(Yii::app()->basePath.'/../uploads/admin_image/'.$fileName); 
		     	}
				Yii::app()->user->setFlash('success',"Suuccessfully {$msg}");
				$this->redirect('listuser');
			 }
			 else
			 {
				 
				print_r($model->getErrors());exit;
			 }
			}
			 
		}
		
		$this->render("adduser",array("model"=>$model));
	}
	public function actionListuser()
	{
		 
		$this->layout="column2";
		$model=new User;
		$model=$model->findAll();
		$this->render("listuser",array("model"=>$model));
	}
	public function actionDeleteuser($id=null)
	{
		 
		$this->layout="column2";
		$model=new User;
		 
		if($model->deleteByPk($id))
		{
			Yii::app()->user->setFlash('success',"successfully Deleted");
		}
		else
		{
			Yii::app()->user->setFlash('success',"Failed to Deleted");
		}
		$this->redirect(Yii::app()->request->urlReferrer);
	}

    public function actionListuserstatus($id=null,$status=null)
	{
		
		$model=new User;
		if($status=='1')
		{
			$status='0';
		}
		else
		{
			$status='1';
		}
		 
		$model=$model->findByPk($id);
		$model->status=$status;
        $model->update();
		 
		$this->redirect(Yii::app()->request->urlReferrer);
		
	}


	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{
	 
		//if (!defined('CRYPT_BLOWFISH')||!CRYPT_BLOWFISH)
		//throw new CHttpException(500,"This application requires that PHP was compiled with Blowfish support for crypt().");
        $this->layout="login";
		$model=new User;

		// if it is ajax validation request
		 

		// collect user input data
		
		if(isset($_POST['User']))
		{
			if(isset($_POST['yt1']))
			{
				$email = $_POST['User']['email'];
				$pwd = $model->find();
				//print_r($pwd);
				//print_r($pwd['password']);
				//echo 'dsfsd';exit;
			}
			else
			{	
			$model->setScenario("login") ;
			$model->attributes=$_POST['User'];
			 
			 // validate user input and redirect to the previous page if valid
		    	if($model->validate())
				 {
					 if($model->checklogin($model->username,$model->password))
					 {
						 
						  $this->redirect(Yii::app()->request->baseUrl.'/'.Yii::app()->params['adminUrl']."site/adminhome");
					   
					 }
				 }
			  }	 
		  }
		// display the login form
		$this->render('login',array('model'=>$model));
	}
	//===================change password===============================
	
	
	public function actionChangepassword()
	{
	    $this->layout="column2";
		$model=new User;
          $id=Yii::app()->user->roleid;
         

		// if it is ajax validation request
		 

		// collect user input data
		if(isset($_POST['User']))
		{
			$model->setScenario("changepassword") ;
			$model->attributes=$_POST['User'];
			 
			 // validate user input and redirect to the previous page if valid
		    	if($model->validate())
				 {
					 
					 $pass=$model->password;
					 $model=$model->findByPk($id);
			         $model->password= md5($pass);
			         $model->conpassword= md5($pass);
			        if($model->update())
			        {
			         Yii::app()->user->setFlash('success',"Password successfully Changed");
			         $this->redirect(Yii::app()->request->baseUrl.'/'.Yii::app()->params['adminUrl']."site/adminhome");
				     }					
				 }
				 
		  }
		
		$this->render('changepass',array('model'=>$model));
	}
	
	public function actionAdminlinksadd($id=null)
	{
		$this->layout="column2";
		
		if($id)
		{
			$model=new AdminLinks;
			$model=$model->findByPk($id);
			$msg="Edited";
		}
		else
		{
			$model=new AdminLinks;
			$msg="Added";
		}
		if(Yii::app()->request->isPostRequest)
		{
			// print_r($_POST);exit;
			$model->attributes=$_POST['AdminLinks'];
			if($model->parent_id==null) $model->parent_id=0;
			if($model->save())
			{
				Yii::app()->user->setFlash('success',"Suuccessfully {$msg}");
				$this->redirect('adminlinks');
			}
			 
		}
		$this->render('adminlinks',array('model'=>$model));
	}
	public function actionAdminlinksdelete($id=null)
	{
		 
		$this->layout="column2";
		$model=new AdminLinks;
		 
		if($model->deleteByPk($id))
		{
			Yii::app()->user->setFlash('success',"successfully Deleted");
		}
		else
		{
			Yii::app()->user->setFlash('success',"Failed to Deleted");
		}
		$this->redirect(Yii::app()->request->urlReferrer);
	}
	public function actionAdminlinks($id=null)
	{
		  $this->layout="column2";
		  $model=new AdminLinks;
		   if(Yii::app()->request->isPostRequest)
		   {
			   if(isset($_POST['priority']))
			   {
				  foreach($_POST['priority'] as $k=>$v) 
				  {
					$model=new AdminLinks;
					$model=$model->findByPk($k) ;
					$model->priority=$v;
					$model->update(); 
				  }
				  Yii::app()->user->setFlash('success',"Priority Successfully Updated");
				  $this->refresh();
			   }
		   }
		    
			$main=$model->findAll(array('condition'=>'parent_id=0','order'=>'priority asc'));
            $sub=$model->findAll(array('condition'=>'parent_id!=0','order'=>'priority asc'));
		    $this->render('adminlinkslist',array('main'=>$main,'sub'=>$sub));
	}
	public function actionAdminlinksstatus($id=null,$status=null)
	{
		
		$model=new AdminLinks;
		if($status=='1')
		{
			$status='0';
		}
		else
		{
			$status='1';
		}
		 
		$model=$model->findByPk($id);
		$model->status=$status;
        $model->update();
		 
		$this->redirect(Yii::app()->request->urlReferrer);
		
	}
	public function actionAdminhome()
	{
	    $this->layout="column2";
		$this->render('adminhome');
	}

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout()
	{
		 
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->user->loginUrl);
	}
}
