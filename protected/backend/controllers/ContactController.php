<?php

class ContactController extends Controller
{
	public $layout='column2';
 
    public function beforeAction($action)
	{
	
	  //Yii::app()->user->logout(); 
		if(!isset(Yii::app()->user->roleid))
		{
			 
			//Yii::app()->user->setState("urls",Yii::app()->request->url);
			$this->redirect(Yii::app()->user->loginUrl);
		}
		else
		{
			 
			return true;
		}
    
     
	}
	 
	 public function actionIndex()
	 {
		 
		  $soc = new Contact;
		  $soc = $soc->findAll(array("order"=>"contact_id desc"));
		  $this->render("index",array("model"=>$soc));
	 }
 	public function actionDelete($id=null)
	{
		 
		$this->layout="column2";
		$model=new Contact;
		 
		if($model->deleteByPk($id))
		{
			Yii::app()->user->setFlash('success',"successfully Deleted");
		}
		else
		{
			Yii::app()->user->setFlash('success',"Failed to Deleted");
		}
		$this->redirect(Yii::app()->request->urlReferrer);
	}
 
		  
}		   
