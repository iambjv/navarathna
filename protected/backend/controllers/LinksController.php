<?php

class LinksController extends Controller
{
	public $layout='column2';
    public function actionList()
    {
		$links = new Links;
		$model = $links->findAll();
		$this->render("list",array("model"=>$model));
	}
	public function actionAdd($id=null)
    {
		$links = new Links;
		if($id)
		{
		$links = $links->findByPk($id);
		}
		if(Yii::app()->request->isPostRequest)
		{
		 $links->attributes = $_POST['Links'];
		 
		 if($links->save())
		 {
			 Yii::app()->user->setFlash("success","Successfully Saved");
			 $this->redirect("list");
		 }
		 
		}
		$this->render("add",array('model'=>$links));
	}
	 
 
}
