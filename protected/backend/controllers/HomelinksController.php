<?php

class HomelinksController extends Controller
{
    public $layout='column2';
    
	public function beforeAction($action)
	{
		//Yii::app()->user->logout(); 
		if(!isset(Yii::app()->user->roleid))
		{
		//Yii::app()->user->setState("urls",Yii::app()->request->url);
		$this->redirect(Yii::app()->user->loginUrl);
		}
		else
		{

		return true;
		}
	}
	
    public function actionAdd($id=null)
	{
		$this->layout="column2";
		
		if($id)
		{
			$model=new HomeLinks;
			$model=$model->findByPk($id);
			$msg="Edited";
		}
		else
		{
			$model=new HomeLinks;
			$msg="Added";
		}
		if(Yii::app()->request->isPostRequest)
		{
			// print_r($_POST);exit;
			$model->attributes=$_POST['HomeLinks'];
			$model->meta_title = $_POST['HomeLinks']['meta_title'];
            $model->meta_keyword = $_POST['HomeLinks']['meta_keyword'];
            $model->meta_description = $_POST['HomeLinks']['meta_description'];
			if($model->parent_id==null) $model->parent_id=0;
			if($model->save())
			{
				Yii::app()->user->setFlash('success',"Suuccessfully {$msg}");
				$this->redirect('index');
			}
			 
		}
		$this->render('add',array('model'=>$model));
	}
	public function actionDelete($id=null)
	{
		 
		//$this->layout="column2";
		$model=new HomeLinks;
		 
		if($model->deleteByPk($id))
		{
			Yii::app()->user->setFlash('success',"successfully Deleted");
		}
		else
		{
			Yii::app()->user->setFlash('success',"Failed to Deleted");
		}
		$this->redirect(Yii::app()->request->urlReferrer);
	}
	public function actionIndex($id=null)
	{
		 
		  $this->layout="column2";
		  $model=new HomeLinks;
		   if(Yii::app()->request->isPostRequest)
		   {
			   if(isset($_POST['priority']))
			   {
				  foreach($_POST['priority'] as $k=>$v) 
				  {
					$model=new HomeLinks;
					$model=$model->findByPk($k) ;
					$model->priority=$v;
					$model->update(); 
				  }
				  Yii::app()->user->setFlash('success',"Priority Successfully Updated");
				  $this->refresh();
			   }
		   }
		    
			$main=$model->findAll(array('condition'=>'parent_id=0','order'=>'priority asc'));
            $sub=$model->findAll(array('condition'=>'parent_id!=0','order'=>'priority asc'));
		    $this->render('index',array('main'=>$main,'sub'=>$sub));
	}
	public function actionStatus($id=null,$status=null)
	{
		 
		$model=new HomeLinks;
		if($status=='1')
		{
			$status='0';
		}
		else
		{
			$status='1';
		}
		 
		$model=$model->findByPk($id);
		$model->status=$status;
        $model->update();
		 
		$this->redirect(Yii::app()->request->urlReferrer);
		
	}
	public function actionContentedit($id=null)
	{
		$links = new HomeLinks;
		if($id)
		{
		$links = $links->findByPk($id);
		}
		if(Yii::app()->request->isPostRequest)
		{
			 
		//$links->attributes = $_POST['HomeLinks'];
		$links->content_title = $_POST['HomeLinks']['content_title'];
		$links->content_description = $_POST['HomeLinks_content_description'];
		 
		$links->scenario="contentedit";
		 
		 if($links->save())
		 {
			 Yii::app()->user->setFlash("success","Successfully Saved");
			 $this->redirect("index");
		 }
		 
		}
		$this->render("contentedit",array("model"=>$links));
	 }
 
}
