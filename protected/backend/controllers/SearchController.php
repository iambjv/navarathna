<?php
class SearchController extends Controller
{
	public $layout='column2';
 
    public function beforeAction($action)
	{
	
	  //Yii::app()->user->logout(); 
		if(!isset(Yii::app()->user->roleid))
		{
			 
			//Yii::app()->user->setState("urls",Yii::app()->request->url);
			$this->redirect(Yii::app()->user->loginUrl);
		}
		else
		{
			 
			return true;
		}
    
     
	}
	public function actionProductsearch($id=null)
	{
		$model= new Product;
			$model->setScenario("search2") ;
		
		$model=new Product;
		$category=new ProductCategory; 
		$category= $category->listWithStatus();
		$brand=new ProductBrand; 
		$brand= $brand->listWithStatus();
		$fit=new ProductFit;
		$fit= $fit->listWithStatus();
		$color=new ProductColor;
		$color= $color->listWithStatus();
		$fabric=new Productfabric;
		$fabric= $fabric->listWithStatus();
		$collar=new ProductCollar;
		$collar= $collar->listWithStatus();
		$sleeve=new ProductSleeve;
		$sleeve= $sleeve->listWithStatus();
		$brandfit=new ProductBrandfit;
		$brandfit= $brandfit->listWithStatus();
		$occasion= new ProductOccasion;
		$occasion=$occasion->listWithStatus();
        $pattern=new ProductPattern;
        $pattern=  $pattern->listWithStatus();
        $size=new ProductSize;
        $size=$size->listWithStatus();
        $productcolor= new ProductStockColor; 
        $productsize=new ProductStockSize;
        
        $selectedcolor=array();
        $selectedsize=array();
        $errorsize=array();
        $errorcolor=array();
       
      
			
        if(Yii::app()->request->isPostRequest)
		{
			
			$model= new Product;
			$model->setScenario("search2") ;
			
		    $art="";
		    $con=array();
				
			foreach($_POST['Product']as $k=>$v)
			{
			if($v!=null)
			{
				if($k=='pdt_size')
			     {
				 $pdtsize=$_POST['Product']['pdt_size'];
				
			$str='';
			foreach($pdtsize as $k2=>$v2)
				 {
				$str.=$v2.',';	
			     }
			     $str=rtrim($str,',');
				 $art.="size.stock_size in ($str) AND";
			     
				}else
			if($k=='pdt_color')
			     {
				 $pdtcolor=$_POST['Product']['pdt_color'];
				 $str="";
				 foreach($pdtcolor as $k2=>$v2)
				 {
				
			       $str.=$v2.',';	
			     }
			     $str=rtrim($str,',');
				 $art.="color.colorid in ($str) AND";
				 }else	 
				if($k=='pdt_price')
				{
				$art.="pdt_price=:$k AND";
				$x=':'.$k;
			    $con[$x]=$v;
			}else
			if($k=='pdt_gender')
				{
				$art.="pdt_gender=:$k AND";
				$x=':'.$k;
			    $con[$x]=$v;
			}
			else{
			$ke=ltrim($k,'pdt_');
			$id=$ke."_id";
			$art.="$ke.$id=:$k AND ";
			$x=':'.$k;	
			$con[$x]=$v;	
		         }
			
			  }     
		    }
		
			 $art=rtrim($art,' AND ');
			 
			
			    //echo $art."<br>";
				 //print_r($con);
				//die;

		 	$model = $model->with(
		array('category' ,
			'brand',
			'fit',
			'fabric',
			'collar',
			'sleeve',
			'occasion',
			'pattern',
			'color',
			'size'   
		)
		)->findAll(array("condition"=>"$art","params"=>$con));
			 //$model=$model->with('size')->findAll(array("condition"=>"size.stock_size  in (3)","together"=>true,"params"=>array(":catid"=>165)));
			 
 
			 $this->render("productlist",array('model'=>$model)); 
		exit;

			
	 //$this->redirect("productlist",array('model'=>$model));
	 }
	       $model= new Product;
			$model->setScenario("search2") ;
	
	 
		$this->render("productsearch",array(
						"model"=>$model,
						'category'=>$category ,
						'brand'=>$brand,
						'fit'=>$fit,
						'color'=>$color,
						'fabric'=>$fabric,
						'collar'=>$collar,
						'sleeve'=>$sleeve,
						'brandfit'=>$brandfit,
						'occasion'=>$occasion,
						'pattern'=>$pattern,
						
						'size'=>$size,
						'selectedcolor'=>$selectedcolor,
						'selectedsize'=>$selectedsize,
						'errorsize'=>$errorsize,
						'errorcolor'=>$errorcolor,
		));
	
}

///Product List funtion not usign now list
	public function actionProductlist($model=null)
	{
		
	 $this->render("productlist",array('model'=>$model));
		
	 
    }
     public function actionProductstatus($id=null,$status=null)
	 {
		$model=new Product;
		if($status=='0')
		{
			$status='1';
		}
		else
		{
			$status='0';
		}
		 
		$model=$model->findByPk($id);
		$model->status=$status;
        $model->update();
        Yii::app()->user->setFlash("success","Successfully Updated Status");
		$this->redirect(Yii::app()->request->urlReferrer);
	 }
	
	
	
	
	
  
}
