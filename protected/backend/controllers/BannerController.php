<?php
class BannerController extends CController
{
	
	public $layout='column1';
	public function beforeAction($action)
	{
	
	  //Yii::app()->user->logout(); 
		if(!isset(Yii::app()->user->roleid))
		{
			 
			//Yii::app()->user->setState("urls",Yii::app()->request->url);
			$this->redirect(Yii::app()->user->loginUrl);
		}
		else
		{
			 
			return true;
		}
    
     
	}
		
		public function actionIndex($id=null)
	{
		  $this->layout="column2";
		  $model=new Banner_pos;
		   if(Yii::app()->request->isPostRequest)
		   {
			   if(isset($_POST['priority']))
			   {
				  foreach($_POST['priority'] as $k=>$v) 
				  {
					$model=new Banner_pos;
					$model=$model->findByPk($k) ;
					$model->priority=$v;
					$model->update(); 
				  }
				  Yii::app()->user->setFlash('success',"Priority Successfully Updated");
				  $this->refresh();
			   }
		   }
		    
			$main=$model->findAll();
            //$sub=$model->findAll(array('condition'=>'parent_id!=0','order'=>'priority asc'));
		    $this->render('bannerlist',array('main'=>$main));
	}
		//array('main'=>$main)
	public function actionAdd($id=null)
	{
	
	
	$this->layout="column2";
		
		if($id)
		{
		
		
			$model=new Banner_pos;
			$model=$model->findByPk($id);
			$msg="Edited";
				
		}
		else
		{
			$model=new Banner_pos;
			$msg="Added";
		}
		if(Yii::app()->request->isPostRequest)
		{
			//print_r($_POST);exit;
			$model->attributes=$_POST['Banner_pos'];
			//if($model->ban_id==null) $model->ban_id=0;
			if($model->save())
			{
				Yii::app()->user->setFlash('success',"Suuccessfully {$msg}");
				$this->redirect('Index');
			}
			 
		}
		$this->render('banneradd',array('model'=>$model));
		
	}	
	public function actionPos_delete($id=null)
	{
				
		$this->layout="column2";
		$model=new Banner_pos;
		 
		if($model->deleteByPk($id))
		{
			Yii::app()->user->setFlash('success',"successfully Deleted");
		}
		else
		{
			Yii::app()->user->setFlash('success',"Failed to Deleted");
		}
		$this->redirect(Yii::app()->request->urlReferrer);
	}	
		
    public function actionCreate($id=NULL)
    {
		
		$this->layout="column2";
		if($id)
		{
		
		
			$model=new Banner;
			$model=$model->findByPk($id);
			$msg="Edited";
				
		}
		else
		{
			$model=new Banner;
			$msg="Added";
		}
	
		
		 if(Yii::app()->request->isPostRequest)
        {
			
            $model->attributes=$_POST['Banner'];
            $model->image=CUploadedFile::getInstance($model,'image');
            $model->url=str_replace("+","%2B",$model->url);
            if($model->save())
            {
				
				
	        $path =Yii::app()->basePath.'/../uploads/banners/'.$model->image;
			$model->image->saveAs($path);
                
                
                Yii::app()->user->setFlash('success',"Suuccessfully ");
				$this->redirect('view');
                // redirect to success page
            }
            else
            {
				$model->image="";
			}
        }
        $this->render('Create', array('model'=>$model));
    }
    public function actionView()
    {
	
	$this->layout="column2";
	$model=new Banner;
	$join=	$model->with('banner')->findAll(array("order"=>"t.ban_id"));
	//print_r($join);
	 $this->render('bannerpage', array('model'=>$join));
		
	}	
	 public function actionBannerstatus($id=null,$status=null)
	 {
		
		$this->layout="column2";
		$model=new Banner;
		if($status==0)
		    $status=1;
		 else
		    $status=0;
		       
	        $model=$model->findByPk($id);
	        $model->status=$status;
	        
	
		if($model->update())
		{
			Yii::app()->user->setFlash('success',"successfully Updated");
		}
		else
		{
			Yii::app()->user->setFlash('success',"Failed to Update");
		}
		$this->redirect(Yii::app()->request->urlReferrer);
		 
	 }
	 
	 public function actionStatus($id=null,$status=null)
	 {
		
		$this->layout="column2";
		$model=new Banner_pos;
		if($status==0)
		    $status=1;
		 else
		    $status=0;
		       
	        $model=$model->findByPk($id);
	        $model->ban_status=$status;
	        
	
		if($model->update())
		{
			Yii::app()->user->setFlash('success',"successfully Updated");
		}
		else
		{
			Yii::app()->user->setFlash('success',"Failed to Update");
		}
		$this->redirect(Yii::app()->request->urlReferrer);
		 
	 }
		
    
}
?>
