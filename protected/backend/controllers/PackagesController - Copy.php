<?php
class PackagesController extends Controller
{
	public $layout='column2';
 
    public function beforeAction($action)
	{
	
	  //Yii::app()->user->logout(); 
		if(!isset(Yii::app()->user->roleid))
		{
			 
			//Yii::app()->user->setState("urls",Yii::app()->request->url);
			$this->redirect(Yii::app()->user->loginUrl);
		}
		else
		{
			 
			return true;
		}
    
     
	}
	public function actionPackagesdayadd($id=null)
	{
		$model=new PackageDays;
		$package=new Packages; 
	//	$package= $package->listWithStatus();
		$package= $package->with(array('sub_category'))->findAll(array("condition"=>"t.status='1'","order"=>"package_title ASC")); 
		$array= array();
		if($package)
		{
			
			foreach($package as $key => $val)
			{
				$array[$val->package_id] = $val->sub_category->sub_category_name." - ".$val->package_title;
			}
		}
		
        $pimage = new PackageImage;
        $pimg=array();
        $himage = new PackageHotelImage;
        $himg=array();
        
        $msg="added";
         
			if($id)
			{
				$msg="edited";
				$model=$model->findByPk($id);
				$model->image="1";
				//	echo $id;exit;
				$pimg=$pimage->findAll(array("condition"=>"package_day_id=:id","order"=>"priority asc","params"=>array(":id"=>$id)));
				//	print_r($pimg);exit;
				$himg=$himage->findAll(array("condition"=>"package_day_id=:id","order"=>"priority asc","params"=>array(":id"=>$id)));

			}
			else
			{
			
			}
			if(Yii::app()->request->isPostRequest)
			{
			 
				Yii::import('ext.ResizeImage');
		  
				if(!empty($_FILES['image']['name']['0']))
				{
			
					$model->image="1";
 
				} 
				$model->attributes=$_POST['PackageDays'];		   	
				$model->short_description = $_POST['PackageDays_short_description']; 
				$model->end_description = $_POST['PackageDays_end_description']; 

			
				//	print_r($_POST);exit;
				if($model->save())
				{
			  
					$id=$model->primaryKey;
			
					if(!empty($_FILES['image']['name']['0']))
					{
						$times = 1;
						foreach($_FILES['image']['name'] as $key=>$filename)
						{

							$rnd = rand(0,99999);
							$resize = new ResizeImage($_FILES['image']['tmp_name'][$key]);
							$resize->resizeTo(280,179,'maxwidth');
							$resize->saveImage(Yii::app()->basePath."/../uploads/package_day_icon/{$model->package_day_id}_{$rnd}.jpg");
							move_uploaded_file($_FILES['image']['tmp_name'][$key],Yii::app()->basePath."/../uploads/package_day/{$model->package_day_id}_{$rnd}.jpg");
							$pimage->image=$model->package_day_id."_".$rnd.".jpg";
							$pimage->id="";
							$pimage->package_day_id=$id;
							$pimage->isNewRecord = true;
							$pimage->save();
					
							if($times=='1' and $msg!="edited")
							{
								$model->updateByPk($id,array('package_day_image'=>$model->package_day_id."_".$rnd.".jpg"));
							}
							$times++;
					  
						}
					}
					if(!empty($_FILES['hotel_image']['name']['0']))
					{
						$times = 1;
						foreach($_FILES['hotel_image']['name'] as $key=>$filename)
						{

							$rnd = rand(0,99999);
							$resize = new ResizeImage($_FILES['hotel_image']['tmp_name'][$key]);
							$resize->resizeTo(280,179,'maxwidth');
							$resize->saveImage(Yii::app()->basePath."/../uploads/package_hotel_icon/{$model->package_day_id}_{$rnd}.jpg");
							move_uploaded_file($_FILES['hotel_image']['tmp_name'][$key],Yii::app()->basePath."/../uploads/package_hotel/{$model->package_day_id}_{$rnd}.jpg");
							$himage->hotel_image=$model->package_day_id."_".$rnd.".jpg";
							$himage->package_hotel_id="";
							$himage->package_day_id=$id;
							$himage->isNewRecord = true;
							$himage->save();
										
							$times++;
					  
						}
					}
					Yii::app()->user->setFlash("success","Successfully {$msg}");
					$this->redirect("packagesdaylist");
				}
				else
				{
			
		
		      
				}
		 
			
			
			}
	 
		$this->render("packagesdayadd",array("model"=>$model,'package'=>$array,'pimg'=>$pimg,'himg'=>$himg));
	}
	public function actionPackagesdaylist($order=null)
	{
		$sub_category = new PackageSubCategory;
		$sub_category= $sub_category->listWithStatus();

		if($order=="")
		{
			$order="package_day_id DESC";
		}
		else
		{
			
			$order="t.priority is  NULL ,t.priority ASC";
		}
		$model = new PackageDays;
		$model = $model->with(
		array(
			'package'   
		)
		)->findAll(array("order"=>"{$order}"));
		 
	 $this->render("packagesdaylist",array('model'=>$model,'sub_category'=>$sub_category));
		
	 
    }
	
	public function actionPackagesadd($id=null)
	{
		$model=new Packages;
		$main_category=new PackageMainCategory; 
		$sub_category=new PackageSubCategory; 
		$main_category= $main_category->listWithStatus();
		$sub_category= $sub_category->listWithStatus();
		  
        $msg="added";
         
		//print_r($_POST);exit;
        if($id)
		{
			$msg="edited";
			$model=$model->findByPk($id);
		
		}
		else
		{
			
		}
		if(Yii::app()->request->isPostRequest)
		{
			 
			Yii::import('ext.ResizeImage');
			$rnd = rand(0,99999);
			$model->attributes=$_POST['Packages'];
			 	
		   
		   	$model->package_title = $_POST['Packages']['package_title'];
			//$model->package_route = $_POST['Packages']['package_route'];
			$model->package_description = $_POST['Packages_package_description']; 
			$model->package_offer_description = $_POST['Packages_package_offer_description']; 
			$model->package_meta_title = $_POST['Packages']['package_meta_title'];
            $model->package_meta_keyword = $_POST['Packages']['package_meta_keyword'];
            $model->package_meta_description = $_POST['Packages']['package_meta_description'];
          //   if(isset($_POST['package_facilities']))
          //  {
          //  $fac = $_POST['package_facilities'];
          //  $fac_list = implode(',',$fac);
          //  $model->package_facilities = $fac_list;
		  // }
			//else
			//{
				$model->package_facilities = "";
			//}
			
			
            //print_r($_POST);exit;
		//	$model->package_image=CUploadedFile::getInstance($model,'package_image');
			if($_FILES['Packages']['tmp_name']['package_image']!="")
			{
			//echo 'cfgf';exit;
			$model->package_image = $rnd.".jpg";
			}
		//	print_r($_POST);exit;
			if($model->save())
			{
		
				if($_FILES['Packages']['tmp_name']['package_image']!="")
				{
					
					$resize = new ResizeImage($_FILES['Packages']['tmp_name']['package_image']);
					$resize->resizeTo(690,400,'exact');
					$resize->saveImage(Yii::app()->basePath."/../uploads/packages/thumb/{$rnd}.jpg");
					move_uploaded_file($_FILES['Packages']['tmp_name']['package_image'],Yii::app()->basePath."/../uploads/packages/full/{$rnd}.jpg");
				}
										  
			}
		    			
			Yii::app()->user->setFlash("success","Successfully {$msg}");
			$this->redirect("packageslist");
		 }
		 
		$this->render("packagesadd",array(
						"model"=>$model,
						'main_category'=>$main_category ,'sub_category'=>$sub_category,
		));
	}
	public function actionPackageslist($order=null)
	{
		if($order=="")
		{
			$order="package_id DESC";
		}
		else
		{
			
			$order="t.priority is  NULL ,t.priority ASC";
		}
		$model = new Packages;
		$model = $model->with(
		array(
			'main_category','sub_category'   
		)
		)->findAll(array("order"=>"{$order}"));
		 
	 $this->render("packageslist",array('model'=>$model));
		
	 
    }
	
	//MAIN CATEGORY START
	public function actionMaincategorylist()
	{
		 
		$model=new PackageMainCategory;
		$model=$model->findAll(array('order'=>'main_category_name asc'));    
		$this->render('maincategorylist',array('model'=>$model));
		 
	}
	public function actionMaincategoryadd($id=null)
	{
		$model=new PackageMainCategory;
		if($id)
			{
				$model=$model->findByPk($id);
				$msg="edited";
				 
			}
			else
			{
				$msg="saved";
			}
		if(Yii::app()->request->isPostRequest)
		{
			
			Yii::import('ext.ResizeImage');
			$rnd = rand(0,99999);
            $model->attributes=$_POST['PackageMainCategory'];
			$model->main_desc = $_POST['PackageMainCategory_main_desc']; 

		//	$model->main_image=CUploadedFile::getInstance($model,'main_image');
			if($_FILES['PackageMainCategory']['tmp_name']['main_image']!="")
			{
			$model->main_image = $rnd.".jpg";
			}
			
			if($model->save())
			{
			 	if($_FILES['PackageMainCategory']['tmp_name']['main_image']!="")
				{
					
					$resize = new ResizeImage($_FILES['PackageMainCategory']['tmp_name']['main_image']);
					$resize->resizeTo(685,330,'exact');
					$resize->saveImage(Yii::app()->basePath."/../uploads/mainpackages/thumb/{$rnd}.jpg");
					move_uploaded_file($_FILES['PackageMainCategory']['tmp_name']['main_image'],Yii::app()->basePath."/../uploads/mainpackages/full/{$rnd}.jpg");
				}
					  
			}
			else{ print_r($model->getErrors()); exit;}
			
				Yii::app()->user->setFlash('success',"Successfully {$msg}");
			   $this->redirect("maincategorylist");
			
		}
		$this->render("maincategoryadd",array('model'=>$model));
	}
	
	
	public function actionMaincategorystatus($id=null,$status=null)
	{
		
		$model=new PackageMainCategory;
		if($status=='0')
		{
			$status='1';
		}
		else
		{
			$status='0';
		}
		 
		$model=$model->findByPk($id);
		$model->status=$status;
        $model->update();
		$this->redirect(Yii::app()->request->urlReferrer);
		
	}
	public function actionMaincategorydelete($id=null)
	{
		 
		$this->layout="column2";
		$model=new PackageMainCategory;
		 
		if($model->deleteByPk($id))
		{
			Yii::app()->user->setFlash('success',"successfully Deleted");
		}
		else
		{
			Yii::app()->user->setFlash('success',"Failed to Deleted");
		}
		$this->redirect(Yii::app()->request->urlReferrer);
	}
	//MAIN CATEGORY END
	
	//SUB CATEGORY START
	public function actionSubcategorylist()
	{
		 
		$model=new PackageSubCategory;
		$model=$model->with(array('PackageMainCategory'))->findAll(array('order'=>'priority asc'));    
		$this->render('subcategorylist',array('model'=>$model));
		 
	}
	public function actionSubcategoryadd($id=null)
	{
		$model=new PackageSubCategory;
		$category=new PackageMainCategory; 
		$category= $category->listWithStatus();
		 
		if($id)
			{
				$model=$model->findByPk($id);
				$msg="edited";
				 
			}
			else
			{
				$msg="saved";
			}
		if(Yii::app()->request->isPostRequest)
		{
            $model->attributes=$_POST['PackageSubCategory'];
            $model->meta_title = $_POST['PackageSubCategory']['meta_title'];
            $model->meta_keyword = $_POST['PackageSubCategory']['meta_keyword'];
            $model->meta_description = $_POST['PackageSubCategory']['meta_description'];

            if(isset($_POST['sub_category_icon']))
            {
            $icon = $_POST['sub_category_icon'];
            $icon_imp = implode(',',$icon);
            $model->sub_category_icon = $icon_imp;
			}
			else
			{
				$model->sub_category_icon = "";
			}
            if($model->save())
            {
				Yii::app()->user->setFlash('success',"Successfully {$msg}");
			   $this->redirect("subcategorylist");
			}
		}
		$this->render("subcategoryadd",array('model'=>$model,'category'=>$category));
	}
	public function actionSubcategorystatus($id=null,$status=null)
	{
		
		$model=new PackageSubCategory;
		if($status=='0')
		{
			$status='1';
		}
		else
		{
			$status='0';
		}
		 
		$model=$model->findByPk($id);
		$model->status=$status;
        $model->update();
		$this->redirect(Yii::app()->request->urlReferrer);
		
	}
	public function actionSubcategorydelete($id=null)
	{
		 
		$this->layout="column2";
		$model=new PackageSubCategory;
		 
		if($model->deleteByPk($id))
		{
			Yii::app()->user->setFlash('success',"successfully Deleted");
		}
		else
		{
			Yii::app()->user->setFlash('success',"Failed to Deleted");
		}
		$this->redirect(Yii::app()->request->urlReferrer);
	}
	//SUB CATEGORY END
	
	 // Product Image MAnupulattoion
	 public function actionDeleteimage($id=null,$name)
	 {
		 $pimage = new PackageImage;
		 if($id)
		 {
			$del= $pimage->deleteByPk($id);
			if($del)
			{
				 Yii::app()->user->setFlash("success","Successfully Deleted");
				 unlink(Yii::app()->basePath."/../uploads/package_day/".$name);
				 unlink(Yii::app()->basePath."/../uploads/package_day_icon/".$name);
                 $this->redirect(Yii::app()->request->urlReferrer);


			}
			 
		 }
		  
		 
	 }
	 public function actionActiveimage($id=null,$status=null)
	 {
		$model=new PackageImage;
		if($status=='0')
		{
			$status='1';
		}
		else
		{
			$status='0';
		}
		 
		$model=$model->findByPk($id);
		$model->status=$status;
        $model->update();
        Yii::app()->user->setFlash("success","Successfully Updated Status");
		$this->redirect(Yii::app()->request->urlReferrer);
	 }
	 public function actionDeleteimage1($id=null,$name)
	 {
		 $pimage = new PackageHotelImage;
		 if($id)
		 {
			$del= $pimage->deleteByPk($id);
			if($del)
			{
				 Yii::app()->user->setFlash("success","Successfully Deleted");
				 unlink(Yii::app()->basePath."/../uploads/package_hotel/".$name);
				 unlink(Yii::app()->basePath."/../uploads/package_hotel_icon/".$name);
                 $this->redirect(Yii::app()->request->urlReferrer);


			}
			 
		 }
		  
		 
	 }
	 public function actionActiveimage1($id=null,$status=null)
	 {
		$model=new PackageHotelImage;
		if($status=='0')
		{
			$status='1';
		}
		else
		{
			$status='0';
		}
		 
		$model=$model->findByPk($id);
		$model->status=$status;
        $model->update();
        Yii::app()->user->setFlash("success","Successfully Updated Status");
		$this->redirect(Yii::app()->request->urlReferrer);
	 }
	 public function actionImgpriority($id=null,$priority=null)
	 {
		 
		 $model=new PackageImage;
		 $model=$model->findByPk($id);
		 $model->priority=$priority;
         if($model->update())
         {
			 echo '1';
		 }
		 else
		 {
			 echo '2';
		 }
		 
	 }
	  public function actionImgpriority1($id=null,$priority=null)
	 {
		 
		 $model=new PackageHotelImage;
		 $model=$model->findByPk($id);
		 $model->priority=$priority;
         if($model->update())
         {
			 echo '1';
		 }
		 else
		 {
			 echo '2';
		 }
		 
	 }
	 public function actionImgbackground($id=null,$image=null)
	 {
		 
		 $model=new PackageDays;
		 $model=$model->updateByPk($id,array('package_day_image'=>$image));
		 echo "1";
		 
	 }
	  public function actionPackagesstatus($id=null,$status=null)
	 {
		$model=new Packages;
		if($status=='0')
		{
			$status='1';
		}
		else
		{
			$status='0';
		}
		 
		$model=$model->findByPk($id);
		$model->status=$status;
        $model->update();
        Yii::app()->user->setFlash("success","Successfully Updated Status");
		$this->redirect(Yii::app()->request->urlReferrer);
	 }
	 public function actionPackagespriority($id=null,$priority=null)
	 {
		 
		 $model=new Packages;
		 $model=$model->findByPk($id);
		 $model->priority=$priority;
         if($model->update())
         {
			 echo '1';
		 }
		 else
		 {
			 echo '2';
		 }
		 
	 }
	 public function actionSubcatpriority($id=null,$priority=null)
	 {
		 
		 $model=new PackageSubCategory;
		 $model=$model->findByPk($id);
		 $model->priority=$priority;
         if($model->update())
         {
			 echo '1';
		 }
		 else
		 {
			 echo '2';
		 }
		 
	 }
	  public function actionPackagesdaystatus($id=null,$status=null)
	 {
		$model=new PackageDays;
		if($status=='0')
		{
			$status='1';
		}
		else
		{
			$status='0';
		}
		 
		$model=$model->findByPk($id);
		$model->status=$status;
        $model->update();
        Yii::app()->user->setFlash("success","Successfully Updated Status");
		$this->redirect(Yii::app()->request->urlReferrer);
	 }
	 public function actionPackagesdaypriority($id=null,$priority=null)
	 {
		 
		 $model=new PackageDays;
		 $model=$model->findByPk($id);
		 $model->priority=$priority;
         if($model->update())
         {
			 echo '1';
		 }
		 else
		 {
			 echo '2';
		 }
		 
	 }
	 public function actionPackagebookings()
	 {
		  
		  $soc = new PackageBookings;
		  $soc = $soc->findAll(array("order"=>"package_booking_id desc"));
		  $this->render("packagebookings",array("model"=>$soc));
	 }
 	public function actionDelete($id=null)
	{
		 
		$this->layout="column2";
		$model=new PackageBookings;
		 
		if($model->deleteByPk($id))
		{
			Yii::app()->user->setFlash('success',"successfully Deleted");
		}
		else
		{
			Yii::app()->user->setFlash('success',"Failed to Deleted");
		}
		$this->redirect(Yii::app()->request->urlReferrer);
	}

  
}
