    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl;?>/js/bootstrap/dist/assets/lib/datatables/css/demo_page.css">
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl;?>/js/bootstrap/dist/assets/lib/datatables/css/DT_bootstrap.css">
<script type="text/javascript" language="javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jquery.js"></script>
 <script src="<?php echo Yii::app()->request->baseUrl;?>/js/bootstrap/dist/assets/lib/datatables/jquery.dataTables.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl;?>/js/bootstrap/dist/assets/lib/datatables/DT_bootstrap.js"></script>
 
		<script type="text/javascript" charset="utf-8">
			var JQ=jQuery.noConflict()
			JQ(document).ready(function() {
				JQ('#dataTable').dataTable(
				{
					  
						"sDom": "<'pull-right'l>t<'row'<'col-lg-6'f><'col-lg-6'p>>",
						"sPaginationType": "bootstrap",
						"oLanguage": {
						"sLengthMenu": "Show _MENU_ entries"
						}

						}
				);
			} );
		</script>
<div class="row">
<div style="text-align:center;margin-top:3px;" >	<span class="label label-success" style="text-align:center"><?php echo Yii::app()->user->getFlash('success');?></span></div>
              <div class="col-lg-12">
                <div class="box">
                  <header>
                    <div class="icons">
                      <i class="fa fa-table"></i>
                    </div>
                    <h5>List Packages</h5> 
                    <div style="text-align:center; padding: 10px 15px;"> <a href="packagesadd" class="btn btn-metis-3 btn-sm btn-flat">Add New Package</a>
   </div>
                  </header>
                  <div id="collapse4" class="body">
					
                    <table id="dataTable" class="table table-bordered table-condensed table-hover table-striped">
                      <thead>
                        <tr>
                          <th style="text-align:center">Sl.</th>
                          <th>Main Category</th>
                          <th>Sub Category</th>
                          <th>Package Title</th>
                          <th>Priority</th>
                         <th>Edit</th>
                        <!-- <th>Del</th>-->
                         <th>ACT</th>
                          
                        </tr>
                      </thead>
                      <tbody>
                        <?php
					  if(!empty($model))
					  {  
						  
						  $i=1;
						  foreach($model as $k=>$v)
						  {
							  ?>
							  <tr>
							  <td style="text-align:center"><?php echo  $i; ?></td>
							  <td><?php echo $v['main_category']['main_category_name']?></td>
							  <td><?php echo $v['sub_category']['sub_category_name']?></td>
							  <td><?php echo $v['package_title']?></td>


					        <td>
								 <input type="text" style="width:60px;text-align:center" id="priority" maxlength="1"   onchange="updatePriority('<?php echo $v->package_id;?>',this)"  value="<?= $v->priority; ?>">
						         <img class="load" style="display:none;" src="<?php echo Yii::app()->request->baseUrl;?>/images/ajax-loader.gif">
						    </td>
								 
								   <td style="text-align:center">
									   <?
							          echo CHtml::link(CHtml::image( Yii::app()->request->baseUrl.'/images/admin/pencil.png', 'Edit', array('title'=>'Edit')),array('packages/packagesadd','id'=>$v->package_id),array('title'=>'Edit'));  
                                      ?>
                                   </td>
                                   <!--
                                   <td style="text-align:center">
									   <?
                                       // echo CHtml::link(CHtml::image( Yii::app()->request->baseUrl.'/images/admin/cross.png', 'Delete', array('title'=>'Delete')),array('product/branddelete','id'=>$v->product_id),array('title'=>'Delete','confirm'=>'Are you sure'));  
							           ?>
							       </td>
							       -->
							       <td style="text-align:center">
									<?
									 if($v->status=='1')
								  {
									   $image = CHtml::image(Yii::app()->request->baseUrl.'/images/admin/active.gif', 'Status', array('title'=>'Active'));
									   $tit="Active";
								  }
								  else
								  {
									  $image = CHtml::image(Yii::app()->request->baseUrl.'/images/admin/deactive.gif', 'Status', array('title'=>'Deactive'));
									  $tit="Deactive";
								  }
								   
							        echo CHtml::link($image,array('packages/packagesstatus','id'=>$v->package_id,'status'=>$v->status),array('title'=>$tit)); ?>
						 
							  </td>	  
							  
							  
							  
							  
							  
							  
							  
							  
							  </tr>
							  
							  <?
							  $i++;
						  }
					  }
					  else
					  {
						  echo "<tr><td colspan='100%' style='text-align:center'>No Result Found..</td></tr>";
					  }
					  ?>
                      </tbody>
                    </table>
                  
                  </div>
                </div>
              </div>
            </div><!-- /.row -->
<script>
$(document).ready(function(){
    $('#example').dataTable();
});

function updatePriority(id,pri)
{ 
	
	$(pri).parent().find(".load").show();
	 
		$.get("packagespriority?id="+id+"&priority="+$(pri).val(),function(data){
			$(pri).parent().find(".load").hide();
			if(data=='1')
			{
				alert("Successfully Updated");
				 
			}
			else
			{
				alert("Failed to Update");
			}
			
			});
	 
}
</script>
   
