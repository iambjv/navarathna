<script src="<?= Yii::app()->request->baseUrl;?>/ckeditor/ckeditor.js"></script>
 
<script src="<?= Yii::app()->request->baseUrl;?>/js/jquery.numeric.js"></script>
 <style>
 .form-control{ max-width:300px;}
.form-control2{ max-width:200px; background-color: #fff;
    background-image: none;
    border: 1px solid #ccc;
    border-radius: 4px;
    box-shadow: 0 1px 1px rgba(0, 0, 0, 0.075) inset;
    color: #555;
    display: block;
    font-size: 14px;
    height: 34px;
    line-height: 1.42857;
    padding: 6px 12px;
    transition: border-color 0.15s ease-in-out 0s, box-shadow 0.15s ease-in-out 0s;
    width: 100%;float:left}
.form-control3{ max-width:100px; background-color: #fff;
    background-image: none;
    border: 1px solid #ccc;
    border-radius: 4px;
    box-shadow: 0 1px 1px rgba(0, 0, 0, 0.075) inset;
    color: #555;
    display: block;
    font-size: 14px;
    height: 34px;
    line-height: 1.42857;
    padding: 6px 12px;
    transition: border-color 0.15s ease-in-out 0s, box-shadow 0.15s ease-in-out 0s;
    width: 100%;float:left}
.form-control4{ max-width:150px; background-color: #fff;
    background-image: none;
    border: 1px solid #ccc;
    border-radius: 4px;
    box-shadow: 0 1px 1px rgba(0, 0, 0, 0.075) inset;
    color: #555;
    display: block;
    font-size: 14px;
    height: 34px;
    line-height: 1.42857;
    padding: 6px 12px;
    transition: border-color 0.15s ease-in-out 0s, box-shadow 0.15s ease-in-out 0s;
    width: 100%;float:left}
 </style>

  <script type="text/javascript" src="<?= Yii::app()->request->baseUrl;?>/fckeditor/fckeditor.js"></script>

              <div class="col-lg-6" style="width:100%">
                <div class="box dark">
                  <header>
                    <div class="icons">
                      <i class="fa fa-edit"></i>
                    </div>
                    <h5><?php if($model->package_id==null){ ?>Add<? }else {?>Edit<?}?>  Package</h5>
                     <div style="float:right;padding: 10px 15px;"> <a href="packageslist" class="btn btn-metis-3 btn-sm btn-flat">View All Packages</a>
                  </div>

                  </header>
                  <div id="div-1" class="body">
					  <?php
					  if(Yii::app()->user->hasFlash("success"))
					  {
						  ?>
               <div style="text-align:center;margin-top:3px;margin-bottom:8px;" >	<span class="label label-danger" style="text-align:center"><?php echo Yii::app()->user->getFlash('success');?></span><br /></div>
					<? } ?>
					<?php
					$form=$this->beginWidget('CActiveForm', array(
					'htmlOptions'=>array('class'=>'form-horizontal','enctype' => 'multipart/form-data'),
					'enableClientValidation'=>true,
					//   'enableAjaxValidation'=>true,
					'clientOptions'=>array(
					'validateOnSubmit'=>true,
					),
					)); 

					?>
				 

              
						<div class="form-group">
						<?php echo $form->labelEx($model,'main_category_id',array('class'=>'control-label col-lg-4')); ?>               
						<div class="col-lg-8">
						
						<?php echo $form->dropDownList($model,'main_category_id',CHtml::listData($main_category,'main_category_id','main_category_name'),array('class'=>'form-control','empty'=>"Select Main Category","required"=>true)); ?>
						 <?php echo $form->error($model,'main_category_id',array('class'=>'text-danger')); ?>
						</div>
						</div>
                        
						<div class="form-group">
						<?php echo $form->labelEx($model,'sub_category_id',array('class'=>'control-label col-lg-4')); ?>               
						<div class="col-lg-8">
						
						<?php echo $form->dropDownList($model,'sub_category_id',CHtml::listData($sub_category,'sub_category_id','sub_category_name'),array('class'=>'form-control','empty'=>"Select Sub Category","required"=>true)); ?>
						 <?php echo $form->error($model,'sub_category_id',array('class'=>'text-danger')); ?>
						</div>
						</div>
						<div class="form-group" >
						<?php echo $form->labelEx($model,'package_title',array('class'=>'control-label col-lg-4')); ?>               
						<div class="col-lg-8">
						<?php echo $form->textField($model,'package_title',array('class'=>'form-control',"required"=>true)); ?>
						<?php echo $form->error($model,'package_title',array('class'=>'text-danger')); ?>
						</div>
						</div>
						
                        
                        <div class="form-group">
						<?php echo $form->labelEx($model,'days',array('class'=>'control-label col-lg-4')); ?>               
						<div class="col-lg-8">
						<?php echo $form->dropDownList($model,'days',array('1'=>'1 Day','2'=>'2 Days','3'=>'3 Days','4'=>'4 Days','5'=>'5 Days','6'=>'6 Days','7'=>'7 Days','8'=>'8 Days','9'=>'9 Days','10'=>'10 Days','11'=>'11 Days','12'=>'12 Days','13'=>'13 Days','14'=>'14 Days','15'=>'15 Days','16'=>'16 Days','17'=>'17 Days'),array('class'=>'form-control4',"required"=>true));?>
						 <?php echo $form->error($model,'days',array('class'=>'text-danger')); ?>
                         
						<?php echo $form->dropDownList($model,'nights',array('1'=>'1 Night','2'=>'2 Nights','3'=>'3 Nights','4'=>'4 Nights','5'=>'5 Nights','6'=>'6 Nights','7'=>'7 Nights','8'=>'8 Nights','9'=>'9 Nights','10'=>'10 Nights','11'=>'11 Nights','12'=>'12 Nights','13'=>'13 Nights','14'=>'14 Nights','15'=>'15 Nights','16'=>'16 Nights','17'=>'17 Nights'),array('class'=>'form-control4',"required"=>true)); ?>
						 <?php echo $form->error($model,'nights',array('class'=>'text-danger')); ?>
						</div>
						</div>   
                        
						<div class="form-group">
						<?php echo $form->labelEx($model,'package_meta_title',array('class'=>'control-label col-lg-4')); ?>               
						<div class="col-lg-8">
						<?php echo $form->textField($model,'package_meta_title',array('class'=>'form-control','placeholder'=>"Meta Title")); ?>
						 <?php echo $form->error($model,'package_meta_title',array('class'=>'text-danger')); ?>
						</div>
						</div>
                        
                        
						<div class="form-group">
						<?php echo $form->labelEx($model,'package_meta_keyword',array('class'=>'control-label col-lg-4')); ?>               
						<div class="col-lg-8">
						<?php echo $form->textField($model,'package_meta_keyword',array('class'=>'form-control','placeholder'=>"Meta Keyword")); ?>
						 <?php echo $form->error($model,'package_meta_keyword',array('class'=>'text-danger')); ?>
						</div>
						</div>
						<div class="form-group">
						<?php echo $form->labelEx($model,'package_meta_description',array('class'=>'control-label col-lg-4')); ?>               
						<div class="col-lg-8">
						<?php echo $form->textField($model,'package_meta_description',array('class'=>'form-control','placeholder'=>"Meta Description")); ?>
						 <?php echo $form->error($model,'package_meta_description',array('class'=>'text-danger')); ?>
						</div>
						</div>
						
					
                    
                    
                         
						<div class="form-group">
						<?php echo $form->labelEx($model,'package_image',array('class'=>'control-label col-lg-4')); ?>               
						<div class="col-lg-8">
						<?
                         echo $form->fileField($model, 'package_image');
                         if($model->package_image!="") 
                         {
                        ?>
                         <img src="<?= Yii::app()->request->baseUrl;?>/images/admin/zoom copy.png" style="width:20px;color:red"  id="tonus"  /> 
                         <?
                         }
                         echo $form->error($model, 'package_image',array('class'=>'text-danger'));
                         ?>
                         </div>
                         </div>
						
                     
                     
                     
                     
                     
                     
                     
						<div class="form-group">
						<?php echo $form->labelEx($model,'package_price',array('class'=>'control-label col-lg-4')); ?>               
						<div class="col-lg-8">
						<?php echo $form->textField($model,'package_price',array('class'=>'form-control2','placeholder'=>"Package Price")); ?>
						 <?php echo $form->error($model,'package_price',array('class'=>'text-danger')); ?>
                         
						<?php echo $form->dropDownList($model,'package_price_in',array('INR'=>'INR','USD'=>'USD'),array('class'=>'form-control3',"required"=>true)); ?>
						 <?php echo $form->error($model,'package_price_in',array('class'=>'text-danger')); ?>
						</div>
						</div>   
                        
                 
 
                        
						<div class="form-group">
						<?php echo $form->labelEx($model,'package_route',array('class'=>'control-label col-lg-4')); ?>               
						<div class="col-lg-8">
						<?php echo $form->textField($model,'package_route',array('class'=>'form-control','placeholder'=>"Package Route","required"=>true)); ?>
						 <?php echo $form->error($model,'package_route',array('class'=>'text-danger')); ?>
						</div>
						</div>
						
                        
                        
						<div class="form-group">
						<?php echo $form->labelEx($model,'package_description',array('class'=>'control-label col-lg-4')); ?>               
						<div class="col-lg-8">
						<?php echo $form->textField($model,'package_description',array('class'=>'form-control','placeholder'=>"Package Description")); ?>
						 <?php echo $form->error($model,'package_description',array('class'=>'text-danger')); ?>
						</div>
						</div>
						
                        
                        
                        
						
                         
						  <div class="form-group">
						<div><?php echo $form->labelEx($model,'what_to_see',array('class'=>'control-label col-lg-4')); ?>   </div> 
						<div style="clear:both"></div>           
						<div class="col-lg-8" style="width:100%;">
						
				
						<script type="text/javascript">
		var oFCKeditor = new FCKeditor('Packages_what_to_see','95%','400',null,null) ;
		oFCKeditor.BasePath = "<?= Yii::app()->request->baseUrl;?>/fckeditor/" ;
		var newval=escape('<?=preg_replace("#(\r\n|\n|\r)#s", ' ',addslashes($model->what_to_see))?>');
		oFCKeditor.Value=unescape(newval);
		oFCKeditor.Create() ;
	</script>
						<?php echo $form->error($model,'what_to_see',array('class'=>'text-danger')); ?>
						</div>
						</div>
						
                  
                         
                         
						  <div class="form-group">
						<div><?php echo $form->labelEx($model,'what_to_do',array('class'=>'control-label col-lg-4')); ?>   </div> 
						<div style="clear:both"></div>           
						<div class="col-lg-8" style="width:100%;">
						
				
						<script type="text/javascript">
		var oFCKeditor = new FCKeditor('Packages_what_to_do','95%','400',null,null) ;
		oFCKeditor.BasePath = "<?= Yii::app()->request->baseUrl;?>/fckeditor/" ;
		var newval=escape('<?=preg_replace("#(\r\n|\n|\r)#s", ' ',addslashes($model->what_to_do))?>');
		oFCKeditor.Value=unescape(newval);
		oFCKeditor.Create() ;
	</script>
						<?php echo $form->error($model,'what_to_do',array('class'=>'text-danger')); ?>
						</div>
						</div>
						

                         
						
						  <div class="form-group">
						<div><?php echo $form->labelEx($model,'inclusion',array('class'=>'control-label col-lg-4')); ?>   </div> 
						<div style="clear:both"></div>           
						<div class="col-lg-8" style="width:100%;">
						
				
						<script type="text/javascript">
		var oFCKeditor = new FCKeditor('Packages_inclusion','95%','400',null,null) ;
		oFCKeditor.BasePath = "<?= Yii::app()->request->baseUrl;?>/fckeditor/" ;
		var newval=escape('<?=preg_replace("#(\r\n|\n|\r)#s", ' ',addslashes($model->inclusion))?>');
		oFCKeditor.Value=unescape(newval);
		oFCKeditor.Create() ;
	</script>
						<?php echo $form->error($model,'inclusion',array('class'=>'text-danger')); ?>
						</div>
						</div>
						
                        
                        
						
						  <div class="form-group">
						<div><?php echo $form->labelEx($model,'exclusion',array('class'=>'control-label col-lg-4')); ?>   </div> 
						<div style="clear:both"></div>           
						<div class="col-lg-8" style="width:100%;">
						
				
						<script type="text/javascript">
		var oFCKeditor = new FCKeditor('Packages_exclusion','95%','400',null,null) ;
		oFCKeditor.BasePath = "<?= Yii::app()->request->baseUrl;?>/fckeditor/" ;
		var newval=escape('<?=preg_replace("#(\r\n|\n|\r)#s", ' ',addslashes($model->exclusion))?>');
		oFCKeditor.Value=unescape(newval);
		oFCKeditor.Create() ;
	</script>
						<?php echo $form->error($model,'exclusion',array('class'=>'text-danger')); ?>
						</div>
						</div>
						
                        
                        
                        
                        <div class="form-group">
						       <label  class = 'control-label col-lg-4'>&nbsp;</label>   
                        <div class="col-lg-8">
						 <?php echo CHtml::submitButton('Save',array('class'=>'btn btn-metis-5 btn-sm btn-flat')); ?>
                        </div>
                         </div>
                         
                         
                         
                    
                         
                         
                         
       
                     <?php $this->endWidget(); ?>
                  </div>
                </div>
              </div>
  <script type="text/javascript">
 $('#tonus').tooltip({
	delay: 0,
	showURL: false,
	bodyHandler: function() {
		return $("<img style='z-index:96000' />").attr("src", '<?= Yii::app()->request->baseUrl;?>/uploads/packages/full/<?= $model->package_image ?>');
	}
});
 </script>

             
