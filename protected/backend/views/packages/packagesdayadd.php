<script src="<?= Yii::app()->request->baseUrl;?>/ckeditor/ckeditor.js"></script>
 
<script src="<?= Yii::app()->request->baseUrl;?>/js/jquery.numeric.js"></script>
 <style>
 .form-control{ max-width:300px;}
 </style>
<script type="text/javascript" language="javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jquery.js"></script>
 <script type="text/javascript" language="javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jquery.multifile.js"></script>
   <script type="text/javascript" src="<?= Yii::app()->request->baseUrl;?>/fckeditor/fckeditor.js"></script>

              <div class="col-lg-6" style="width:100%">
                <div class="box dark">
                  <header>
                    <div class="icons">
                      <i class="fa fa-edit"></i>
                    </div>
                    <h5><?php if($model->package_day_id==null){ ?>Add<? }else {?>Edit<?}?>  Package Day</h5>
                     <div style="float:right;padding: 10px 15px;"> <a href="packagesdaylist" class="btn btn-metis-3 btn-sm btn-flat">View All Package Days</a>
                  </div>

                  </header>
                  <div id="div-1" class="body">
					  <?php
					  if(Yii::app()->user->hasFlash("success"))
					  {
						  ?>
               <div style="text-align:center;margin-top:3px;margin-bottom:8px;" >	<span class="label label-danger" style="text-align:center"><?php echo Yii::app()->user->getFlash('success');?></span><br /></div>
					<? } ?>
					<?php
					$form=$this->beginWidget('CActiveForm', array(
					'htmlOptions'=>array('class'=>'form-horizontal','enctype' => 'multipart/form-data'),
					'enableClientValidation'=>true,
					//   'enableAjaxValidation'=>true,
					'clientOptions'=>array(
					'validateOnSubmit'=>true,
					),
					)); 

					?>
				 

						<div class="form-group">
						<?php echo $form->labelEx($model,'package_id',array('class'=>'control-label col-lg-4')); ?>               
						<div class="col-lg-8">
						
						<?php echo $form->dropDownList($model,'package_id',$package,array('class'=>'form-control','empty'=>"Select Package","required"=>true)); ?>
						 <?php echo $form->error($model,'package_id',array('class'=>'text-danger')); ?>
						</div>
						</div>
						
						<div class="form-group" >
						<?php echo $form->labelEx($model,'package_day_title',array('class'=>'control-label col-lg-4')); ?>               
						<div class="col-lg-8">
						<?php echo $form->textField($model,'package_day_title',array('class'=>'form-control',"required"=>true)); ?>
						<?php echo $form->error($model,'package_day_title',array('class'=>'text-danger')); ?>
						</div>
						</div>
						                       
                         
                         
                          
						
                    
                      <div class="form-group">
						<div><?php echo $form->labelEx($model,'short_description',array('class'=>'control-label col-lg-4')); ?>   </div> 
						<div style="clear:both"></div>           
						<div class="col-lg-8" style="width:100%;">
						
				
						<script type="text/javascript">
		var oFCKeditor = new FCKeditor('PackageDays_short_description','95%','400',null,null) ;
		oFCKeditor.BasePath = "<?= Yii::app()->request->baseUrl;?>/fckeditor/" ;
		var newval=escape('<?=preg_replace("#(\r\n|\n|\r)#s", ' ',addslashes($model->short_description))?>');
		oFCKeditor.Value=unescape(newval);
		oFCKeditor.Create() ;
	</script>
						<?php echo $form->error($model,'short_description',array('class'=>'text-danger')); ?>
						</div>
						</div>
						
						  <div class="form-group">
					
						
						
                        <div class="form-group">
						       <label  class = 'control-label col-lg-4'>&nbsp;</label>   
                        <div class="col-lg-8">
						 <?php echo CHtml::submitButton('Save',array('class'=>'btn btn-metis-5 btn-sm btn-flat')); ?>
                        </div>
                         </div>
                         
                         
                         
                    
                         
                         
                         
       
                     <?php $this->endWidget(); ?>
                  </div>
                </div>
              </div>
              <script>
				 
				 
					  
              function addmore()
              {
				   
				  $("#last").before($(".clone").html());
				  $(".size:last,.ins:last").val("");
				  disableValue2();
			  }
			  
			  
			  function disableValue()
			  {
				  
					 
					      $(".size option").removeAttr('disabled', true );
						  $(".size").each(function()
						  {
						 
						  $(".size").not(this).find("option[value='"+ $(this).val() + "']").attr('disabled', true );
					      }
					      );
						   
						   
				  
			  }
			   function disableValue2()
			  {
				  
					 
					      $(".size option").removeAttr('disabled', true );
						  $(".size").each(function()
						  {
						 
						  $(".size option[value='"+ $(this).val() + "']").attr('disabled', true );
					      }
					      );
						   
						   
				  
			  }
			  $(function()
			  {
				  disableValue()
			  });
			  $('#up').MultiFile({
 
});

function updatePriority(id,pri)
{ 
	
	$("#load").show();
	 
		$.get("imgpriority?id="+id+"&priority="+$(pri).val(),function(data){
			$("#load").hide();
			if(data=='1')
			{
				alert("Successfully Updated");
				 
			}
			else
			{
				alert("Failed to Update");
			}
			
			});
	 
}
function updatePriority1(id,pri)
{ 
	
	$("#load").show();
	 
		$.get("imgpriority1?id="+id+"&priority="+$(pri).val(),function(data){
			$("#load").hide();
			if(data=='1')
			{
				alert("Successfully Updated");
				 
			}
			else
			{
				alert("Failed to Update");
			}
			
			});
	 
}
function updatebackground(image,id)
{
        $("#load").show();
		$.get("imgbackground?id="+id+"&image="+image,function(data){
			$("#load").hide();
			if(data=='1')
			{
				alert("Successfully Updated");
				 
			}
			else
			{
				alert("Failed to Update");
			}
			
			});
}
 
              </script>
             
