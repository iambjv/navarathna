<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl;?>/js/bootstrap/dist/assets/lib/datatables/css/demo_page.css">

<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl;?>/js/bootstrap/dist/assets/lib/datatables/css/DT_bootstrap.css">

<script type="text/javascript" language="javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jquery.js"></script>

<script src="<?php echo Yii::app()->request->baseUrl;?>/js/bootstrap/dist/assets/lib/datatables/jquery.dataTables.js"></script>

<script src="<?php echo Yii::app()->request->baseUrl;?>/js/bootstrap/dist/assets/lib/datatables/DT_bootstrap.js"></script>

 

		<script type="text/javascript" charset="utf-8">

			var JQ=jQuery.noConflict()

			JQ(document).ready(function() {

				JQ('#dataTable').dataTable(

				{

					  

						"sDom": "<'pull-right'l>t<'row'<'col-lg-6'f><'col-lg-6'p>>",

						"sPaginationType": "bootstrap",

						"oLanguage": {

						"sLengthMenu": "Show _MENU_ entries"

						}



						}

				);

			} );

			function updatePriority(id,pri)

{ 

	 

	JQ(pri).parent().find(".load").show();

	 

		JQ.get("priority?id="+id+"&priority="+JQ(pri).val(),function(data){

			 

			JQ(pri).parent().find(".load").hide();

			if(data=='1')

			{

				alert("Successfully Updated");

				 

			}

			else

			{

				alert("Failed to Update");

			}

			

			});

	 

}

		</script>

<div class="row">

<div style="text-align:center;margin-top:3px;" >	<span class="label label-success" style="text-align:center"><?php echo Yii::app()->user->getFlash('success');?></span></div>

              <div class="col-lg-12">

                <div class="box">

                  <header>

                    <div class="icons">

                      <i class="fa fa-table"></i>

                    </div>

                    <h5>Manage Contact</h5>

                    <div style="text-align:center; padding: 10px 15px;"> 

   </div>

                  </header>

                  <div id="collapse4" class="body">

					

                    <table id="dataTable" class="table table-bordered table-condensed table-hover table-striped">

                      <thead>

                        <tr>

							<th style="text-align:center">Sl.</th>
							<th>Package Name</th>
							<th>Name</th>

							<th>Email</th>

							<th style="width:50px;">Contact Number</th> 

							<th style="width:50px;">Adults</th> 

							<th style="width:50px;">Childs</th> 

							<th style="width:50px;">Comments </th> 

							<th>Delete</th>

                        </tr>

                      </thead>

                      <tbody>

                        <?php

					  if(!empty($model))

					  {  

						  

						  $i=1;

						  foreach($model as $k=>$v)

						  {

							  ?>

							  <tr>

							  <td style="text-align:center"><?php echo  $i; ?></td>
                              <?php
								$package=new Packages; 
								$id = $v['package_id'];
								$package= $package->with(array('sub_category'))->find(array("condition"=>"t.status='1' and t.package_id=$id","order"=>"package_title ASC")); 
			
								$pack_name = $package->sub_category->sub_category_name." - ".$package->package_title." - ".$package->days." Days | ".$package->nights." Nights";
								?>
                                 <td><?php echo $pack_name; ?></td>
							  <td><?php echo $v['name']?></td>

							  <td><?php echo $v['email'] ?></td>

							  <td><?php echo $v['phone'] ?></td>

							  <td><?php echo $v['adults'] ?></td>

							  <td><?php echo $v['childs'] ?></td>

							  <td><?php echo $v['comments'] ?></td>

							  <td style="width:50px;text-align:center">    

								  <?

							          echo CHtml::link(CHtml::image( Yii::app()->request->baseUrl.'/images/admin/cross.png', 'Edit', array('title'=>'Edit')),array('delete','id'=>$v->package_booking_id),array('title'=>'Delete'));  

                                  ?>

                              </td> 

							  

							  

							  

							  

							  </tr>

							  

							  <?

							  $i++;

						  }

					  }

					  else

					  {

						  echo "<tr><td colspan='100%' style='text-align:center'>No Result Found..</td></tr>";

					  }

					  ?>

                      </tbody>

                    </table>

                  

                  </div>

                </div>

              </div>

            </div><!-- /.row -->

 

   

