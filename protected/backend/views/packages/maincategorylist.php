
<div class="row">
<div style="text-align:center;margin-top:3px;" >	<span class="label label-success" style="text-align:center"><?php echo Yii::app()->user->getFlash('success');?></span></div>
              <div class="col-lg-12">
                <div class="box">
                  <header>
                    <div class="icons">
                      <i class="fa fa-table"></i>
                    </div>
                    <h5>List Main Category</h5><div style="float:right;padding: 10px 15px;"> <a href="maincategoryadd" class='btn btn-metis-3 btn-sm btn-flat'> Add New Main Category
                  </a>
                  </div>
                  </header>
                  <div id="collapse4" class="body">
					
                    <table id="dataTable" class="table table-bordered table-condensed table-hover table-striped">
                      <thead>
                        <tr>
                          <th style="text-align:center">Sl.</th>
                          <th>Category Name</th>
                          <th>Category Description</th>
                           
                         <th colspan="3" style="text-align:center">Action</th>
                          
                        </tr>
                      </thead>
                      <tbody>
                        <?php
					  if(!empty($model))
					  {  
						  
						  $i=1;
						  foreach($model as $k=>$v)
						  {
							  ?>
							  <tr>
							  <td style="text-align:center"><?php echo  $i; ?></td>
							  <td><?php echo $v->main_category_name;?></td>
							  <td><?php echo $v->main_desc;?></td>
							  
							  
							   
								  <?php
								  if($v->status=='1')
								  {
									   $image = CHtml::image(Yii::app()->request->baseUrl.'/images/admin/active.gif', 'Status', array('title'=>'Active'));
									   $tit="Active";
								  }
								  else
								  {
									  $image = CHtml::image(Yii::app()->request->baseUrl.'/images/admin/deactive.gif', 'Status', array('title'=>'Deactive'));
									  $tit="Deactive";
								  }
								  ?>
								   <td style="text-align:center">
									   <?
							          echo CHtml::link(CHtml::image( Yii::app()->request->baseUrl.'/images/admin/pencil.png', 'Edit', array('title'=>'Edit')),array('packages/maincategoryadd','id'=>$v->main_category_id),array('title'=>'Edit'));  
                                      ?>
                                   </td>
                                   <td style="text-align:center">
									   <?
                                        echo CHtml::link(CHtml::image( Yii::app()->request->baseUrl.'/images/admin/cross.png', 'Delete', array('title'=>'Delete')),array('packages/categorydelete','id'=>$v->main_category_id),array('title'=>'Delete','confirm'=>'Are you sure'));  
							           ?>
							       </td>
							       <td style="text-align:center">
									<?
							        echo CHtml::link($image,array('packages/maincategorystatus','id'=>$v->main_category_id,'status'=>$v->status),array('title'=>$tit)); ?>
						 
							  </td>	  
							  
							  
							  
							  
							  
							  
							  
							  
							  </tr>
							  
							  <?
							  $i++;
						  }
					  }
					  else
					  {
						  echo "<tr><td colspan='100%' style='text-align:center'>No Result Found..</td></tr>";
					  }
					  ?>
                      </tbody>
                    </table>
                  
                  </div>
                </div>
              </div>
            </div><!-- /.row -->

   
