<script src="<?= Yii::app()->request->baseUrl;?>/ckeditor/ckeditor.js"></script>
 
<script src="<?= Yii::app()->request->baseUrl;?>/js/jquery.numeric.js"></script>
 <style>
 .form-control{ max-width:300px;}
 </style>

  <script type="text/javascript" src="<?= Yii::app()->request->baseUrl;?>/fckeditor/fckeditor.js"></script>
              <div class="col-lg-6" style="width:100%">
                <div class="box dark">
                  <header>
                    <div class="icons">
                      <i class="fa fa-edit"></i>
                    </div>
                    <h5><?php if($model->main_category_id==null){ ?>Add<? }else {?>Edit<?}?> Main Category</h5>
                     <div style="float:right;padding: 10px 15px;"> <a href="maincategorylist" class="btn btn-metis-3 btn-sm btn-flat">View All Main Category</a>
                  </div>

                    <!-- 
                    <div class="toolbar">
                      <nav style="padding: 8px;">
                        <a href="javascript:;" class="btn btn-default btn-xs collapse-box">
                          <i class="fa fa-minus"></i>
                        </a> 
                        <a href="javascript:;" class="btn btn-default btn-xs full-box">
                          <i class="fa fa-expand"></i>
                        </a> 
                        <a href="javascript:;" class="btn btn-danger btn-xs close-box">
                          <i class="fa fa-times"></i>
                        </a> 
                      </nav>
                    </div><!-- /.toolbar -->
                  </header>
                  <div id="div-1" class="body">
              
					<?php
					$form=$this->beginWidget('CActiveForm', array(
					'htmlOptions'=>array('class'=>'form-horizontal','enctype' => 'multipart/form-data'),
					'enableClientValidation'=>true,
					//   'enableAjaxValidation'=>true,
					'clientOptions'=>array(
					'validateOnSubmit'=>true,
					),
					)); 

					?>
				 
                         
						<div class="form-group">
						<?php echo $form->labelEx($model,'main_category_name',array('class'=>'control-label col-lg-4')); ?>               
						<div class="col-lg-8">
						<?php echo $form->textField($model,'main_category_name',array('class'=>'form-control','placeholder'=>"Main Category Name")); ?>
						 <?php echo $form->error($model,'main_category_name',array('class'=>'text-danger')); ?>
						</div>
						</div>
                        
                        
                        
                        
                        
						<div class="form-group">
						<?php echo $form->labelEx($model,'main_image',array('class'=>'control-label col-lg-4')); ?>               
						<div class="col-lg-8">
						<?
                         echo $form->fileField($model, 'main_image');
                         if($model->main_image!="") 
                         {
                        ?>
                         <img src="<?= Yii::app()->request->baseUrl;?>/images/admin/zoom copy.png" style="width:20px;color:red"  id="tonus"  /> 
                         <?
                         }
                         echo $form->error($model, 'main_image',array('class'=>'text-danger'));
                         ?>
                         </div>
                         </div>
						
                     
                     
                     
                         
                         <div class="form-group">
						<div><?php echo $form->labelEx($model,'main_desc',array('class'=>'control-label col-lg-4')); ?>   </div> 
						<div style="clear:both"></div>           
						<div class="col-lg-8" style="width:100%;">
						<script type="text/javascript">
						var oFCKeditor = new FCKeditor('PackageMainCategory_main_desc','95%','400',null,null) ;
						oFCKeditor.BasePath = "<?= Yii::app()->request->baseUrl;?>/fckeditor/" ;
						var newval=escape('<?=preg_replace("#(\r\n|\n|\r)#s", ' ',addslashes($model->main_desc))?>');
						oFCKeditor.Value=unescape(newval);
						oFCKeditor.Create() ;
						</script>
						<?php echo $form->error($model,'main_desc',array('class'=>'text-danger')); ?>
						</div>
						</div>
                         
                         
						 
                        <div class="form-group">
						       <label  class = 'control-label col-lg-4'>&nbsp;</label>   
                        <div class="col-lg-8">
						 <?php echo CHtml::submitButton('Save',array('class'=>'btn btn-metis-5 btn-sm btn-flat')); ?>
                        </div>
                         </div>
       
                     <?php $this->endWidget(); ?>
                  </div>
                </div>
              </div>
  <script type="text/javascript">
 $('#tonus').tooltip({
	delay: 0,
	showURL: false,
	bodyHandler: function() {
		return $("<img style='z-index:96000' />").attr("src", '<?= Yii::app()->request->baseUrl;?>/uploads/mainpackages/full/<?= $model->main_image ?>');
	}
});
 </script>