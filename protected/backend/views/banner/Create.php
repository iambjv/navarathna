 
<style>
 .form-control{ max-width:300px;}
 </style>
              <div style="text-align:center;margin-top:3px;" >	<span class="label label-success" style="text-align:center"><?php echo Yii::app()->user->getFlash('success');?></span></div>
              <div class="col-lg-6" style="width:100%">
                <div class="box dark">
                  <header>
                    <div class="icons">
						<i class="fa fa-edit"></i>
                    </div>
                    <h5><?php if($model->ban_id==null){ ?>Add<? }else {?>Edit<?}?>  Banner</h5>
                     <div style="float:right;padding: 10px 15px;"> <a href="view" class="btn btn-metis-3 btn-sm btn-flat" >View All Banners</a>
                  </div>

                    <!-- 
                    <div class="toolbar">
                      <nav style="padding: 8px;">
                        <a href="javascript:;" class="btn btn-default btn-xs collapse-box">
                          <i class="fa fa-minus"></i>
                        </a> 
                        <a href="javascript:;" class="btn btn-default btn-xs full-box">
                          <i class="fa fa-expand"></i>
                        </a> 
                        <a href="javascript:;" class="btn btn-danger btn-xs close-box">
                          <i class="fa fa-times"></i>
                        </a> 
                      </nav>
                    </div><!-- /.toolbar -->
                  </header>
                  <div id="div-1" class="body">
              
					<?php
					$form=$this->beginWidget('CActiveForm', array(
					'htmlOptions'=>array('class'=>'form-horizontal','enctype' => 'multipart/form-data'),
					'enableClientValidation'=>true,
					//   'enableAjaxValidation'=>true,
					'clientOptions'=>array(
					'validateOnSubmit'=>true,
					),
					)); 

				     ?>                       
						<div class="form-group">
						<?php echo $form->labelEx($model,'ban_id',array('class'=>'control-label col-lg-4')); ?>               
						<div class="col-lg-8">
							<?php
							$pos=new Banner_pos;
							?>
						<?php echo $form->dropDownList($model,'ban_id',CHtml::listData($pos->findAll(),'ban_id','ban_pos'),array("empty"=>"Select",'class'=>'form-control')); ?>
						 <?php echo $form->error($model,'ban_id',array('class'=>'text-danger')); ?>
						</div>
						</div>
						<div class="form-group">
						<?php echo $form->labelEx($model,'image',array('class'=>'control-label col-lg-4')); ?>               
						<div class="col-lg-8">
					<?
                     echo $form->fileField($model, 'image');
                     if($model->image!="") 
                     {
                    ?>
								<img src="<?= Yii::app()->request->baseUrl;?>/images/admin/zoom copy.png" style="width:20px;color:red"  id="tonus"  /> 
                  
                    <?
				}
                     echo $form->error($model, 'image',array('class'=>'text-danger'));
                     ?>
                     </div>
                     </div>
						<div class="form-group">
						<?php echo $form->labelEx($model,'url',array('class'=>'control-label col-lg-4')); ?>               
						<div class="col-lg-8">
					<?
                     echo $form->textField($model, 'url',array('class'=>'form-control'));
                     echo $form->error($model, 'url',array('class'=>'text-danger'));
                     ?>
                     </div>
						</div>
						
						 

                         
                         
                        <div class="form-group">
						       <label  class = 'control-label col-lg-4'>&nbsp;</label>   
                        <div class="col-lg-8">
						 <?php echo CHtml::submitButton('Submit',array('class'=>'btn btn-metis-5 btn-sm btn-flat')); ?>
                        </div>
                         </div>
       
                     <?php $this->endWidget(); ?>
                  </div>
                </div>
              </div>
  <script type="text/javascript">
 $('#tonus').tooltip({
	delay: 0,
	showURL: false,
	bodyHandler: function() {
		return $("<img style='z-index:96000' />").attr("src", '<?= Yii::app()->request->baseUrl;?>/uploads/banners/<?= $model->image ?>');
	}
});
 </script>

