<?php

   function addtoglobal($id,$name)
						   {
							   global $ar;
							   $ar[$id]=$name;
						   }
						   function checksub($id,$parent,$name)
						   {
							   
							    global $ar;
							    global $level;
							    //global $space;
							    $space=array("1"=>"","2"=>" --- ","3"=>" --- -- ","4"=>" --- -- -- ","5"=>" --- -- -- -- ","6"=>"------- -- ");
							    
 
								$ar[$id]= $space[$level].$name;$level++;
							   // echo  $name;echo "<br />";
							   
							    foreach($parent[$id] as $k=>$v)
							    {
									 
									if(array_key_exists($k,$parent))
									{
									
									checksub($k,$parent,$v);
									
									}
									else
									{
									//$level++;
									//echo  '&nbsp;&nbsp;&nbsp;'.$v;echo "<br />";
									$ar[$k]=  $space[$level].$v;
								    }
								}
							   
							    $level--;
						   }
?>
 <style>
 .form-control{ max-width:300px;}
 </style>
              <div class="col-lg-6" style="width:100%">
                <div class="box dark">
                  <header>
                    <div class="icons">
                      <i class="fa fa-edit"></i>
                    </div>
                    <h5><?php if($model->id==null){ ?>Add<? }else {?>Edit<?}?>  Links</h5>
                     <div style="float:right;padding: 10px 15px;"> <a href="index" <?php echo CHtml::submitButton('',array('class'=>'btn btn-metis-3 btn-sm btn-flat')); ?>View All Links
                  </a>
                  </div>

                    <!-- 
                    <div class="toolbar">
                      <nav style="padding: 8px;">
                        <a href="javascript:;" class="btn btn-default btn-xs collapse-box">
                          <i class="fa fa-minus"></i>
                        </a> 
                        <a href="javascript:;" class="btn btn-default btn-xs full-box">
                          <i class="fa fa-expand"></i>
                        </a> 
                        <a href="javascript:;" class="btn btn-danger btn-xs close-box">
                          <i class="fa fa-times"></i>
                        </a> 
                      </nav>
                    </div><!-- /.toolbar -->
                  </header>
                  <div id="div-1" class="body">
              
					<?php
					$form=$this->beginWidget('CActiveForm', array(
					'htmlOptions'=>array('class'=>'form-horizontal'),
					'enableClientValidation'=>true,
					//   'enableAjaxValidation'=>true,
					'clientOptions'=>array(
					'validateOnSubmit'=>true,
					),
					)); 

					?>
						<div class="form-group">
						<?php echo $form->labelEx($model,'parent_id',array('class'=>'control-label col-lg-4')); ?>               
						<div class="col-lg-8">
                        <?php
                         
                           $main=$model->findAll(array('condition'=>"parent_id=0 and status='0'",'order'=>'priority asc'));
                           $sub=$model->findAll(array('condition'=>"parent_id!=0 and status='0'",'order'=>'priority asc'));
                          
						   if($main)
						   {
							   
						   foreach($main as $k=>$v)
						   {
							   $mains[$v->id]=$v->link_name;
						   }
						   
						   foreach($sub as $k=>$v)
						   {
							   $parent[$v->parent_id][$v->id]= $v->link_name; ;
						   }
						   if(!isset($parent))
						   {
							  $parent=array();
						   }
						   
						  
						   foreach($main as $k=>$v)
						   {
							   $GLOBALS['level']=1;
							   
							   if(array_key_exists($v->id,$parent))
							   {
								   
								   checksub($v->id,$parent,$v->link_name);
							   }
							   else
							   {
								 //  $ar[$v->id]=$v->link_name;
								   addtoglobal($v->id,$v->link_name);
								  // echo $level.$v->link_name.'___id:'.$v->id;echo "<br />";
							   }
						   }
						
						  }
						    if(!isset($GLOBALS['ar']))
						    {
							 
								$GLOBALS['ar']=array();
							}
						    
                        ?>
						<?php echo $form->dropDownList($model,'parent_id',$GLOBALS['ar'],array("empty"=>"Main Link",'class'=>'form-control')); ?>
						<?php echo $form->error($model,'parent_id',array('class'=>'text-danger')); ?>
						</div>
						</div>
                         
						<div class="form-group">
						<?php echo $form->labelEx($model,'link_name',array('class'=>'control-label col-lg-4')); ?>               
						<div class="col-lg-8">
						<?php echo $form->textField($model,'link_name',array('class'=>'form-control','placeholder'=>"Link Name")); ?>
						 <?php echo $form->error($model,'link_name',array('class'=>'text-danger')); ?>
						</div>
						</div>
						
						<div class="form-group">
						<?php echo $form->labelEx($model,'link_url',array('class'=>'control-label col-lg-4')); ?>               
						<div class="col-lg-8">
						<?php echo $form->textField($model,'link_url',array('class'=>'form-control','placeholder'=>"Link Url")); ?>
						 <?php echo $form->error($model,'link_url',array('class'=>'text-danger')); ?>
						</div> 
						</div>
						<div class="form-group">
						<?php echo $form->labelEx($model,'meta_title',array('class'=>'control-label col-lg-4')); ?>               
						<div class="col-lg-8">
						<?php echo $form->textField($model,'meta_title',array('class'=>'form-control','placeholder'=>"Meta Title")); ?>
						 <?php echo $form->error($model,'meta_title',array('class'=>'text-danger')); ?>
						</div>
						</div>
						<div class="form-group">
						<?php echo $form->labelEx($model,'meta_keyword',array('class'=>'control-label col-lg-4')); ?>               
						<div class="col-lg-8">
						<?php echo $form->textField($model,'meta_keyword',array('class'=>'form-control','placeholder'=>"Meta Keyword")); ?>
						 <?php echo $form->error($model,'meta_keyword',array('class'=>'text-danger')); ?>
						</div>
						</div>
						<div class="form-group">
						<?php echo $form->labelEx($model,'meta_description',array('class'=>'control-label col-lg-4')); ?>               
						<div class="col-lg-8">
						<?php echo $form->textField($model,'meta_description',array('class'=>'form-control','placeholder'=>"Meta Description")); ?>
						 <?php echo $form->error($model,'meta_description',array('class'=>'text-danger')); ?>
						</div>
						</div>
						<?php $linkimage=array(
						'0'=>'No',
						'1'=>'Yes',
						);?>
						<div class="form-group">
						<?php echo $form->labelEx($model,'content',array('class'=>'control-label col-lg-4')); ?>               
						<div class="col-lg-8">
						<?php echo $form->dropDownList($model,'content',$linkimage,array('class'=>'form-control')); ?>
						<?php echo $form->error($model,'content',array('class'=>'text-danger')); ?>
						</div> 
						</div>
						 

                         
                         
                        <div class="form-group">
						       <label  class = 'control-label col-lg-4'>&nbsp;</label>   
                        <div class="col-lg-8">
						 <?php echo CHtml::submitButton('Save',array('class'=>'btn btn-metis-5 btn-sm btn-flat')); ?>
                        </div>
                         </div>
       
                     <?php $this->endWidget(); ?>
                  </div>
                </div>
              </div>
