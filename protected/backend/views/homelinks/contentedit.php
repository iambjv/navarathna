<script type="text/javascript" language="javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jquery.js"></script>
<script src="<?= Yii::app()->request->baseUrl;?>/ckeditor/ckeditor.js"></script>
<script src="<?= Yii::app()->request->baseUrl;?>/js/jquery.numeric.js"></script>
               <?php

   ?>
   
 <style>
 .form-control{ max-width:300px;}
 </style>
  <script type="text/javascript" src="<?= Yii::app()->request->baseUrl;?>/fckeditor/fckeditor.js"></script>
              <div class="col-lg-6" style="width:100%">
                <div class="box dark">
                  <header>
                    <div class="icons">
                      <i class="fa fa-edit"></i>
                    </div>
                    <h5><?php if($model->id==null){ ?>Add<? }else {?>Edit<?}?>  Links</h5>
                     <div style="float:right;padding: 10px 15px;"> <a href="index" <?php echo CHtml::submitButton('',array('class'=>'btn btn-metis-3 btn-sm btn-flat')); ?>View All
                  </a>
                  </div>

                    <!-- 
                    <div class="toolbar">
                      <nav style="padding: 8px;">
                        <a href="javascript:;" class="btn btn-default btn-xs collapse-box">
                          <i class="fa fa-minus"></i>
                        </a> 
                        <a href="javascript:;" class="btn btn-default btn-xs full-box">
                          <i class="fa fa-expand"></i>
                        </a> 
                        <a href="javascript:;" class="btn btn-danger btn-xs close-box">
                          <i class="fa fa-times"></i>
                        </a> 
                      </nav>
                    </div><!-- /.toolbar -->
                  </header>
                  <div id="div-1" class="body">
              
					<?php
					$form=$this->beginWidget('CActiveForm', array(
					'htmlOptions'=>array('class'=>'form-horizontal'),
					'enableClientValidation'=>true,
					//   'enableAjaxValidation'=>true,
					'clientOptions'=>array(
					'validateOnSubmit'=>true,
					),
					)); 

				     ?>
                         
					 
					 
						<div class="form-group">
						<?php echo $form->labelEx($model,'content_title',array('class'=>'control-label col-lg-4')); ?>               
						<div class="col-lg-8">
						<?php echo $form->textField($model,'content_title',array('class'=>'form-control')); ?>
						 <?php echo $form->error($model,'content_title',array('class'=>'text-danger')); ?>
						</div>
						</div>
					 
					<div class="form-group">
						<div><?php echo $form->labelEx($model,'content_description',array('class'=>'control-label col-lg-4')); ?>   </div> 
						<div style="clear:both"></div>           
						<div class="col-lg-8" style="width:100%;">
						
				
						<script type="text/javascript">
		var oFCKeditor = new FCKeditor('HomeLinks_content_description','95%','400',null,null) ;
		oFCKeditor.BasePath = "<?= Yii::app()->request->baseUrl;?>/fckeditor/" ;
		var newval=escape('<?=preg_replace("#(\r\n|\n|\r)#s", ' ',addslashes($model->content_description))?>');
		oFCKeditor.Value=unescape(newval);
		oFCKeditor.Create() ;
	</script>
						<?php echo $form->error($model,'content_description',array('class'=>'text-danger')); ?>
						</div>
						</div>
                         
                        <div class="form-group">
						       <label  class = 'control-label col-lg-4'>&nbsp;</label>   
                        <div class="col-lg-8">
						 <?php echo CHtml::submitButton('Save',array('class'=>'btn btn-metis-5 btn-sm btn-flat')); ?>
                        </div>
                         </div>
       
                     <?php $this->endWidget(); ?>
                  </div>
                </div>
              </div>
