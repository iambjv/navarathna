<div class="row">
<div style="text-align:center;margin-top:3px;" >	<span class="label label-success" style="text-align:center"><?php echo Yii::app()->user->getFlash('success');?></span></div>
              <div class="col-lg-12">
                <div class="box">
                  <header>
                    <div class="icons">
                      <i class="fa fa-table"></i>
                    </div>
                    <h5>List User</h5><div style="float:right;padding: 10px 15px;"> <a href="adduser" <?php echo CHtml::submitButton('',array('class'=>'btn btn-metis-3 btn-sm btn-flat')); ?>Add New User
                  </a>
                  </div>
                  </header>
                  <div id="collapse4" class="body">
					
                    <table id="dataTable" class="table table-bordered table-condensed table-hover table-striped">
                      <thead>
                        <tr>
                          <th style="text-align:center">Sl.</th>
                          <th>Name</th>
                          <th>Emal</th>
                          <th>Role</th>
                         <th colspan="3" style="text-align:center">Action</th>
                          
                        </tr>
                      </thead>
                      <tbody>
                        <?php
					  if(!empty($model))
					  {  
						  
						  $i=1;
						  foreach($model as $k=>$v)
						  {
							  ?>
							  <tr>
							  <td style="text-align:center"><?php echo  $i; ?></td>
							  <td><?php echo $v->first_name;?>&nbsp;<?php echo $v->last_name;?> </td>
							  <td><?php echo $v->email; ?></td>
							  <?php $role=array("A"=>"Admin","U"=>"User");?>
							   <td><?php echo $role[$v->role]; ?></td>
							   
								  <?php
								  if($v->status=='0')
								  {
									   $image = CHtml::image(Yii::app()->request->baseUrl.'/images/admin/active.gif', 'Status', array('title'=>'Active'));
									   $tit="Active";
								  }
								  else
								  {
									  $image = CHtml::image(Yii::app()->request->baseUrl.'/images/admin/deactive.gif', 'Status', array('title'=>'Deactive'));
									  $tit="Deactive";
								  }
								  ?>
								   <td style="text-align:center">
									   <?
							          echo CHtml::link(CHtml::image( Yii::app()->request->baseUrl.'/images/admin/pencil.png', 'Edit', array('title'=>'Edit')),array('site/adduser','id'=>$v->id),array('title'=>'Edit'));  
                                      ?>
                                   </td>
                                   <td style="text-align:center">
									   <?
                                        echo CHtml::link(CHtml::image( Yii::app()->request->baseUrl.'/images/admin/cross.png', 'Delete', array('title'=>'Delete')),array('site/deleteuser','id'=>$v->id),array('title'=>'Delete','confirm'=>'Are you sure'));  
							           ?>
							       </td>
							       <td style="text-align:center">
									<?
							        echo CHtml::link($image,array('site/listuserstatus','id'=>$v->id,'status'=>$v->status),array('title'=>$tit)); ?>
						 
							  </td>	  
							  
							  
							  
							  
							  
							  
							  
							  
							  </tr>
							  
							  <?
							  $i++;
						  }
					  }
					  else
					  {
						  echo "<tr><td colspan='100%' style='text-align:center'>No Result Found..</td></tr>";
					  }
					  ?>
                      </tbody>
                    </table>
                  
                  </div>
                </div>
              </div>
            </div><!-- /.row -->

   
