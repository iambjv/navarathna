   <div class="tab-content">
        <div id="login" class="tab-pane active">
         
         <?php
			
			$form=$this->beginWidget('CActiveForm', array(
			'id'=>'login-form',
			'htmlOptions'=>array('class'=>'form-signin'),
			'enableClientValidation'=>true,
			//   'enableAjaxValidation'=>true,
			'clientOptions'=>array(
			'validateOnSubmit'=>true,
			),
			)); 
			
		 ?>
            <p class="text-muted text-center">
              Enter your username and password
            </p>
            <?php echo $form->textField($model,'username',array('class'=>'form-control','placeholder'=>"Username")); ?>
            
            <?php echo $form->passwordField($model,'password',array('class'=>'form-control','placeholder'=>"Password")); ?>
             <p class="text-muted text-center" style="color:red">
            <?php echo $form->error($model,'username',array('class'=>'text-danger' )); ?>
            </p>
              <p class="text-muted text-center" style="color:red!i">
            <?php echo $form->error($model,'password',array('class'=>'text-danger')); ?>
            </p>
		    
             <?php echo CHtml::submitButton('Sign in',array('class'=>'btn btn-lg btn-primary btn-block','onclick'=>'btnSubmit(this)')); ?>
           
          
          <?php $this->endWidget(); ?>
        </div>
        <div id="forgot" class="tab-pane">
                  <?php
			
			$form=$this->beginWidget('CActiveForm', array(
			'id'=>'login-form2',
			'htmlOptions'=>array('class'=>'form-signin'),
			'enableClientValidation'=>true,
			//   'enableAjaxValidation'=>true,
			'clientOptions'=>array(
			'validateOnSubmit'=>true,
			),
			)); 
			
		 ?>
            <p class="text-muted text-center">Enter your valid e-mail</p>
            
            <?php echo $form->textField($model,'email',array('class'=>'form-control','placeholder'=>"Email")); ?>
            
            <br>
            <?php echo CHtml::submitButton('Recover Password',array('class'=>'btn btn-lg btn-danger btn-block')); ?>
       
           <?php $this->endWidget(); ?>
        </div>
      
      </div>
 
