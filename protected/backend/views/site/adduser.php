 <style>
 .form-control{ max-width:300px;}
 </style>
              <div class="col-lg-6" style="width:100%">
                <div class="box dark">
                  <header>
                    <div class="icons">
                      <i class="fa fa-edit"></i>
                    </div>
                    <h5><?php if($model->id==null){ ?>Add<? }else {?>Edit<?}?>  User</h5>
                   <div style="float:right;padding: 10px 15px;"> <a href="listuser" class='btn btn-metis-3 btn-sm btn-flat'>All Users
                  </a>
                  </div>
                    <!-- .toolbar 
                    <div class="toolbar">
                      <nav style="padding: 8px;">
                        <a href="javascript:;" class="btn btn-default btn-xs collapse-box">
                          <i class="fa fa-minus"></i>
                        </a> 
                        <a href="javascript:;" class="btn btn-default btn-xs full-box">
                          <i class="fa fa-expand"></i>
                        </a> 
                        <a href="javascript:;" class="btn btn-danger btn-xs close-box">
                          <i class="fa fa-times"></i>
                        </a> 
                      </nav>
                    </div><!-- /.toolbar -->
                  </header>
                  <div id="div-1" class="body">
              
					<?php
					$form=$this->beginWidget('CActiveForm', array(
					'htmlOptions'=>array('class'=>'form-horizontal','enctype' => 'multipart/form-data'),
					'enableClientValidation'=>true,
					//   'enableAjaxValidation'=>true,
					'clientOptions'=>array(
					'validateOnSubmit'=>true,
					),
					)); 

					?>
						<div class="form-group">
						<?php echo $form->labelEx($model,'first_name',array('class'=>'control-label col-lg-4')); ?>               
						<div class="col-lg-8">
						<?php echo $form->textField($model,'first_name',array('class'=>'form-control','placeholder'=>"First Name")); ?>
						 <?php echo $form->error($model,'first_name',array('class'=>'text-danger')); ?>
						</div>
						</div>
						
						<div class="form-group">
						<?php echo $form->labelEx($model,'last_name',array('class'=>'control-label col-lg-4')); ?>               
						<div class="col-lg-8">
						<?php echo $form->textField($model,'last_name',array('class'=>'form-control','placeholder'=>"Last Name")); ?>
						 <?php echo $form->error($model,'last_name',array('class'=>'text-danger')); ?>
						</div> 
						</div>
						
						<div class="form-group">
						<?php echo $form->labelEx($model,'email',array('class'=>'control-label col-lg-4')); ?>               
						<div class="col-lg-8">
						<?php echo $form->textField($model,'email',array('class'=>'form-control','placeholder'=>"Email")); ?>
						<?php echo $form->error($model,'email',array('class'=>'text-danger')); ?>
						</div> 
						</div>
						
                        <div class="form-group">
						<?php echo $form->labelEx($model,'username',array('class'=>'control-label col-lg-4')); ?>               
                        <div class="col-lg-8">
						<?php echo $form->textField($model,'username',array('class'=>'form-control','placeholder'=>"User Name")); ?>
                        <?php echo $form->error($model,'username',array('class'=>'text-danger')); ?>
                        </div>
                         </div>
                         <?php if($model->id==null)
                         {
							 ?>
                         <div class="form-group">
						<?php echo $form->labelEx($model,'password',array('class'=>'control-label col-lg-4')); ?>               
                        <div class="col-lg-8">
						<?php echo $form->passwordField($model,'password',array('class'=>'form-control','placeholder'=>"Password")); ?>
                        <?php echo $form->error($model,'password',array('class'=>'text-danger')); ?>
                        </div>
                         </div>
                         <div class="form-group">
						<?php echo $form->labelEx($model,'conpassword',array('class'=>'control-label col-lg-4')); ?>               
                        <div class="col-lg-8">
						<?php echo $form->passwordField($model,'conpassword',array('class'=>'form-control','placeholder'=>"Confirm Password")); ?>
                        <?php echo $form->error($model,'conpassword',array('class'=>'text-danger')); ?>
                        </div>
                         </div>
                        <?php 
					    
					    }
					    ?>
                        <div class="form-group">
						<?php echo $form->labelEx($model,'role',array('class'=>'control-label col-lg-4')); ?>               
                        <div class="col-lg-8">
						 
						 <?php echo $form->dropDownList($model,'role',array("A"=>"Admin","U"=>"User"),array("empty"=>"Select Role",'class'=>'form-control')); ?>
						 <?php echo $form->error($model,'role',array('class'=>'text-danger')); ?>
                        </div>
                         </div>
                           <div class="form-group">
						<?php echo $form->labelEx($model,'image',array('class'=>'control-label col-lg-4')); ?>               
                        <div class="col-lg-8">
						 <?php echo $form->fileField($model,'image'); ?>
						 <?php echo $form->error($model,'image',array('class'=>'text-danger')); ?>
						 <?php echo CHtml::image(Yii::app()->request->baseUrl.'/uploads/admin_image/'.$model->image, '',array("style"=>"width:100px;margin:10px 0px;")); ?>
                        </div>
                         </div>
                          
                         
                         
                        <div class="form-group">
						       <label  class = 'control-label col-lg-4'>&nbsp;</label>   
                        <div class="col-lg-8">
						 <?php echo CHtml::submitButton('Save',array('class'=>'btn btn-metis-5 btn-sm btn-flat')); ?>
                        </div>
                         </div>
       
                     <?php $this->endWidget(); ?>
                  </div>
                </div>
              </div>
