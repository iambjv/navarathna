<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <title><?php echo Yii::app()->name;?></title>

    <!--Mobile first-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!--IE Compatibility modes-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="msapplication-TileColor" content="#5bc0de">
    <meta name="msapplication-TileImage" content="<?= Yii::app()->request->baseUrl;?>/images/logo.gif">

    <!-- Bootstrap -->
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl;?>/js/bootstrap/dist/assets/lib/bootstrap/css/bootstrap.min.css">

    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl;?>/js/bootstrap/dist/assets/lib/Font-Awesome/css/font-awesome.min.css">

    <!-- Metis core stylesheet -->
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl;?>/js/bootstrap/dist/assets/css/main.css">
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl;?>/js/bootstrap/dist/assets/css/theme.css">


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->

    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->

    <!--[if lt IE 9]>
      <script src="assets/lib/html5shiv/html5shiv.js"></script>
	      <script src="assets/lib/respond/respond.min.js"></script>
	    <![endif]-->

    <!--Modernizr 3.0-->
    <script src="<?php echo Yii::app()->request->baseUrl;?>/js/bootstrap/dist/assets/lib/modernizr-build.min.js"></script>
	<link rel="stylesheet" type="text/css" href="<?=Yii::app()->request->baseUrl;?>/js/jquery.tooltip.css" />
	<script src="<?=Yii::app()->request->baseUrl;?>/js/jquery.tooltip.js"></script>
	 
  </head>
  <body>
    <div id="wrap">
      <div id="top">

        <!-- .navbar -->
        <nav class="navbar navbar-inverse navbar-static-top">

          <!-- Brand and toggle get grouped for better mobile display -->
          <header class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
              <span class="sr-only">Toggle navigation</span> 
              <span class="icon-bar"></span> 
              <span class="icon-bar"></span> 
              <span class="icon-bar"></span> 
            </button>
          
          </header>
          <div class="topnav">
            <div class="btn-toolbar">
			
              <div class="btn-group">
                <a data-placement="bottom" data-original-title="Fullscreen" data-toggle="tooltip" class="btn btn-default btn-sm" id="toggleFullScreen">
                  <i class="glyphicon glyphicon-fullscreen"></i>
                </a> 
              </div>
              <div class="btn-group">
                <a data-placement="bottom" data-original-title="Show / Hide Sidebar" data-toggle="tooltip" class="btn btn-success btn-sm" id="changeSidebarPos">
                  <i class="fa fa-expand"></i>
                </a> 
              </div>
              <!--
              <div class="btn-group">
                <a data-placement="bottom" data-original-title="E-mail" data-toggle="tooltip" class="btn btn-default btn-sm">
                  <i class="fa fa-envelope"></i>
                  <span class="label label-warning">5</span> 
                </a> 
                <a data-placement="bottom" data-original-title="Messages" href="#" data-toggle="tooltip" class="btn btn-default btn-sm">
                  <i class="fa fa-comments"></i>
                  <span class="label label-danger">4</span> 
                </a> 
              </div>
              <div class="btn-group">
                <a data-placement="bottom" data-original-title="Document" href="#" data-toggle="tooltip" class="btn btn-default btn-sm">
                  <i class="fa fa-file"></i>
                </a> 
                <a data-toggle="modal" data-original-title="Help" data-placement="bottom" class="btn btn-default btn-sm" href="#helpModal">
                  <i class="fa fa-question"></i>
                </a> 
              </div>
              -->
              <div class="btn-group">
                <a href="<?php echo Yii::app()->request->baseUrl;?>/<?= Yii::app()->params['adminUrl'];?>site/logout" data-toggle="tooltip" data-original-title="Logout" data-placement="bottom" class="btn btn-metis-1 btn-sm">
                  <i class="fa fa-power-off"></i>
                </a> 
              </div>
             </div>
                 
          </div>
          <!-- /.topnav -->
          <div class="collapse navbar-collapse navbar-ex1-collapse">
<div style="float:left;margin-left:30px;margin-top:10px;width:100px;"></div>
         
            <!-- .nav -->
           <!-- /.nav -->
          </div>
        </nav><!-- /.navbar -->
       <!--
        <!-- header.head -->
        
        <!-- end header.head -->
      </div><!-- /#top -->
      <div id="left">
        <div class="media user-media">
			 
          <a class="user-link" href="javascript:void(0)">
			  <?php 
			// echo 'http://localhost/'.Yii::app()->request->baseUrl.'/uploads/admin_image/'.Yii::app()->user->image;exit;
			  if(Yii::app()->user->image)
			  {
				  ?>
				    <img class="media-object img-thumbnail user-img" alt="" src="<?= Yii::app()->request->baseUrl.'/uploads/admin_image/'.Yii::app()->user->image ?>" style="width:40px;">
                   <?
			  }
			  else
			  {
				  ?>
                   <img class="media-object img-thumbnail user-img" alt="" src="<?php echo Yii::app()->request->baseUrl;?>/uploads/admin_image/noimage.jpg" style="width:40px;">
            <?
		      }
		      ?>
          </a> 
          <div class="media-body">
            <h5 class="media-heading">Welcome</h5>
            <ul class="list-unstyled user-info">
              <li> <a href=""><?= Yii::app()->user->name; ?></a>  </li>
             
            </ul>
          </div>
        </div>
        <!-- #menu -->
      <?php $this->widget('SideLinks');?>
         
      </div><!-- /#left -->
      <div id="content">
        <div class="outer">
          <div class="inner">
    <?= $content; ?>

          <!-- end .inner -->
        </div>

        <!-- end .outer -->
      </div>

      <!-- end #content -->
    </div><!-- /#wrap -->
    <div id="footer">
      <p><?= Yii::app()->params['copyrightInfo'];?></p>
    </div>

    <!-- #helpModal -->
    <div id="helpModal" class="modal fade">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title">Modal title</h4>
          </div>
          <div class="modal-body">
            <p>
              Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor
              in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
            </p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal --><!-- /#helpModal -->
    <script src="<?php echo Yii::app()->request->baseUrl;?>/js/bootstrap/dist/assets/lib/jquery.min.js"></script>
    
    <script src="<?php echo Yii::app()->request->baseUrl;?>/js/bootstrap/dist/assets/lib/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl;?>/js/bootstrap/dist/assets/lib/screenfull/screenfull.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl;?>/js/bootstrap/dist/assets/js/main.min.js"></script>

    <!--For Demo Only. Not required -->
    <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/bootstrap/dist/assets/js/style-switcher.js"></script>
  </body>
</html>
