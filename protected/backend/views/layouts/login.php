<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <title><?php echo Yii::app()->name; ?></title>
    <meta name="msapplication-TileColor" content="#5bc0de" />
    <meta name="msapplication-TileImage" content="assets/img/metis-tile.png" />
   
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl;?>/js/bootstrap/dist/assets/lib/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl;?>/js/bootstrap/dist/assets/css/main.css">
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl;?>/js/bootstrap/dist/assets/lib/magic/magic.css">
    <script>
      (function(i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function() {
          (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date();
        a = s.createElement(o),
        m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
      })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
      ga('create', 'UA-1669764-16', 'onokumus.com');
      ga('send', 'pageview');
    </script>
  </head>
  <body class="login">
	  <div class="container">
      <div class="text-center">
        <img src="<?php echo Yii::app()->request->baseUrl;?>/images/logo.png" style="width:270px;" alt="">
      </div>
   <?php echo $content;?>
      <div class="text-center">
        <ul class="list-inline">
          <li> <a class="text-muted" href="#login" data-toggle="tab">Login</a>  </li>
          <li> <a class="text-muted" href="#forgot" data-toggle="tab">Forgot Password</a>  </li>
          
        </ul>
      </div>
    </div>
    <!-- /container -->
    <script src="<?php echo Yii::app()->request->baseUrl;?>/js/bootstrap/dist/assets/lib/jquery.min.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl;?>/js/bootstrap/dist/assets/lib/bootstrap/js/bootstrap.js"></script>
    <script>
      $('.list-inline li > a').click(function() {
        var activeForm = $(this).attr('href') + ' > form';
        //console.log(activeForm);
        $(activeForm).addClass('magictime swap');
        //set timer to 1 seconds, after that, unload the magic animation
        setTimeout(function() {
          $(activeForm).removeClass('magictime swap');
        }, 1000);
      });
    </script>
  </body>
</html>
