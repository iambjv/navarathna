 <style>
 .form-control{ max-width:300px;}
 </style>
  
<script type="text/javascript" language="javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jquery.js"></script>
 <script type="text/javascript" language="javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jquery.multifile.js"></script>
             <div style="float:right;padding: 10px 15px;"> <a href="../product/productlist" class="btn btn-metis-3 btn-sm btn-flat">View All Product</a>
   </div>
              <div class="col-lg-6" style="width:100%">
                <div class="box dark">
					 
                  <header>
                    <div class="icons">
                      <i class="fa fa-edit"></i>
                    </div>
                    <h5>Search Products</h5>

                    <!-- -->
                    <div class="toolbar">
                      <nav style="padding: 8px;">
                        <a href="javascript:;" class="btn btn-default btn-xs collapse-box">
                          <i class="fa fa-minus"></i>
                        </a> 
                        <a href="javascript:;" class="btn btn-default btn-xs full-box">
                          <i class="fa fa-expand"></i>
                        </a> 
                        <a href="javascript:;" class="btn btn-danger btn-xs close-box">
                          <i class="fa fa-times"></i>
                        </a> 
                      </nav>
                    </div><!-- /.toolbar -->
                  </header>
                  <div id="div-1" class="body">
					  <?php
					  if(Yii::app()->user->hasFlash("success"))
					  {
						  ?>
               <div style="text-align:center;margin-top:3px;margin-bottom:8px;" >	<span class="label label-danger" style="text-align:center"><?php echo Yii::app()->user->getFlash('success');?></span><br /></div>
					<? } ?>
					<?php
					$form=$this->beginWidget('CActiveForm', array(
					'htmlOptions'=>array('class'=>'form-horizontal'),
					'enableClientValidation'=>false,
					//   'enableAjaxValidation'=>true,
					'clientOptions'=>array(
					'validateOnSubmit'=>false,
					),
					)); 

					?>
				 

              
						<div class="form-group">
						<?php echo $form->labelEx($model,'pdt_category',array('class'=>'control-label col-lg-4')); ?>               
						<div class="col-lg-8">
						
						<?php echo $form->dropDownList($model,'pdt_category',CHtml::listData($category,'category_id','category_name'),array('class'=>'form-control','empty'=>"Select Category")); ?>
						 <?php echo $form->error($model,'pdt_category',array('class'=>'text-danger')); ?>
						</div>
						</div>
						
						<div class="form-group">
						<?php echo $form->labelEx($model,'pdt_brand',array('class'=>'control-label col-lg-4')); ?>               
						<div class="col-lg-8">
						 
						<?php echo $form->dropDownList($model,'pdt_brand',CHtml::listData($brand,'brand_id','brand_name'),array('class'=>'form-control','empty'=>"Select Brand")); ?>
						 <?php echo $form->error($model,'pdt_brand',array('class'=>'text-danger')); ?>
						</div>
						</div>
						
						
						<div class="clone">
						<div class="form-group" >
						<?php echo $form->labelEx($model,'pdt_size',array('class'=>'control-label col-lg-4')); ?>               
						<div class="col-lg-8">
						<?php echo $form->dropDownList($model,'pdt_size[]',CHtml::listData($size,'size_id','size_name'),array('class'=>'form-control size','multiple' => 'multiple')); ?>
						
						</div>
						</div>
						</div>
						

						<div class="form-group">
						<?php echo $form->labelEx($model,'pdt_color',array('class'=>'control-label col-lg-4')); ?>               
						<div class="col-lg-8">
						<?php 
						
						
						 if(!empty($errorcolor))
						 {
							 foreach($errorcolor as $k)
							 {
								 $selected[$k]['selected']= "selected";
							 }
						 }
						  else if(!empty($selectedcolor))
						 {
							 foreach($selectedcolor as $k=>$v)
							 {
								 $selected[ $v['colorid']]['selected']= "selected";
							 }
						 
						 }
						 else
						 {
							 $selected=array();
						 }
						 
						 
						 echo $form->dropDownList($model,'pdt_color',CHtml::listData($color,'color_id','color_name'),array('class'=>'form-control chzn-select', 'multiple' => 'multiple',"tabindex"=>4, 'options' => $selected)); ?>
						 <?php echo $form->error($model,'pdt_color',array('class'=>'text-danger')); ?>
						</div>
						</div>
						 
						 <div class="form-group">
						<?php echo $form->labelEx($model,'pdt_fit',array('class'=>'control-label col-lg-4')); ?>               
						<div class="col-lg-8">
						 
						<?php echo $form->dropDownList($model,'pdt_fit',CHtml::listData($fit,'fit_id','fit_name'),array('class'=>'form-control','empty'=>"Select Fit")); ?>
						 <?php echo $form->error($model,'pdt_fit',array('class'=>'text-danger')); ?>
						</div>
						</div>
						<div class="form-group">
						<?php echo $form->labelEx($model,'pdt_brandfit',array('class'=>'control-label col-lg-4')); ?>               
						<div class="col-lg-8">
						 
						<?php echo $form->dropDownList($model,'pdt_brandfit',CHtml::listData($brandfit,'fit_id','fit_name'),array('class'=>'form-control','empty'=>"Select Brand Fit")); ?>
						 <?php echo $form->error($model,'pdt_brandfit',array('class'=>'text-danger')); ?>
						</div>
						</div>
							
						<div class="form-group">
						<?php echo $form->labelEx($model,'pdt_fabric',array('class'=>'control-label col-lg-4')); ?>               
						<div class="col-lg-8">
						 
						<?php echo $form->dropDownList($model,'pdt_fabric',CHtml::listData($fabric,'fabric_id','fabric_name'),array('class'=>'form-control','empty'=>"Select Fabric")); ?>
						 <?php echo $form->error($model,'pdt_fabric',array('class'=>'text-danger')); ?>
						</div>
						</div>
						
						<div class="form-group">
						<?php echo $form->labelEx($model,'pdt_color',array('class'=>'control-label col-lg-4')); ?>               
						<div class="col-lg-8">
						<?php 
						
						
						 if(!empty($errorcolor))
						 {
							 foreach($errorcolor as $k)
							 {
								 $selected[$k]['selected']= "selected";
							 }
						 }
						  else if(!empty($selectedcolor))
						 {
							 foreach($selectedcolor as $k=>$v)
							 {
								 $selected[ $v['colorid']]['selected']= "selected";
							 }
						 
						 }
						 else
						 {
							 $selected=array();
						 }
						 
						 
						 echo $form->dropDownList($model,'pdt_color',CHtml::listData($color,'color_id','color_name'),array('class'=>'form-control chzn-select', 'multiple' => 'multiple',"tabindex"=>4, 'options' => $selected)); ?>
						 <?php echo $form->error($model,'pdt_color',array('class'=>'text-danger')); ?>
						</div>
						</div>
						
						<div class="form-group">
						<?php echo $form->labelEx($model,'pdt_collar',array('class'=>'control-label col-lg-4')); ?>               
						<div class="col-lg-8">
						<?php echo $form->dropDownList($model,'pdt_collar',CHtml::listData($collar,'collar_id','collar_name'),array('class'=>'form-control','empty'=>"Select Collar")); ?>
						 <?php echo $form->error($model,'pdt_collar',array('class'=>'text-danger')); ?>
						</div>
						</div>
						
					   <div class="form-group">
						<?php echo $form->labelEx($model,'pdt_sleeve',array('class'=>'control-label col-lg-4')); ?>               
						<div class="col-lg-8">
						<?php echo $form->dropDownList($model,'pdt_sleeve',CHtml::listData($sleeve,'sleeve_id','sleeve_name'),array('class'=>'form-control','empty'=>"Select Sleeve")); ?>
						 <?php echo $form->error($model,'pdt_sleeve',array('class'=>'text-danger')); ?>
						</div>
						</div>
						 
					     <div class="form-group">
						<?php echo $form->labelEx($model,'pdt_occasion',array('class'=>'control-label col-lg-4')); ?>               
						<div class="col-lg-8">
						<?php echo $form->dropDownList($model,'pdt_occasion',CHtml::listData($occasion,'occasion_id','occasion_name'),array('class'=>'form-control','empty'=>"Select Occasion")); ?>
						 <?php echo $form->error($model,'pdt_occasion',array('class'=>'text-danger')); ?>
						</div>
						</div>
						
						<div class="form-group">
						<?php echo $form->labelEx($model,'pdt_pattern',array('class'=>'control-label col-lg-4')); ?>               
						<div class="col-lg-8">
						<?php echo $form->dropDownList($model,'pdt_pattern',CHtml::listData($pattern,'pattern_id','pattern_name'),array('class'=>'form-control','empty'=>"Select Patern")); ?>
						 <?php echo $form->error($model,'pdt_pattern',array('class'=>'text-danger')); ?>
						</div>
						</div>
						
						
						<div class="form-group" >
						<?php echo $form->labelEx($model,'pdt_gender',array('class'=>'control-label col-lg-4')); ?>               
						<div class="col-lg-8">
						<?php echo $form->dropDownList($model,'pdt_gender',array("M"=>"Male","W"=>"Women","k"=>"Kids"),array('class'=>'form-control','empty'=>"Select Gender")); ?>
					 
						<?php echo $form->error($model,'pdt_gender',array('class'=>'text-danger')); ?>

						</div>
						</div>
						
						
						<div class="form-group" >
						<?php echo $form->labelEx($model,'pdt_price',array('class'=>'control-label col-lg-4')); ?>               
						<div class="col-lg-8">
						<?php echo $form->textField($model,'pdt_price',array('class'=>'form-control','empty'=>"Select Gender")); ?>
						<?php echo $form->error($model,'pdt_price',array('class'=>'text-danger')); ?>
						</div>
						</div>
						
						
					 					
                        <div class="form-group">
						       <label  class = 'control-label col-lg-4'>&nbsp;</label>   
                        <div class="col-lg-8">
						 <?php echo CHtml::submitButton('Search',array('class'=>'btn btn-metis-5 btn-sm btn-flat')); ?>
                        </div>
                         </div>
       
                     <?php $this->endWidget(); ?>
                  </div>
                </div>
              </div>
              <script>
              function addmore()
              {
				   
				  $("#last").before($(".clone").html());
				  $(".size:last,.ins:last").val("");
				  disableValue2();
			  }
			  function disableValue()
			  {
				  
					 
					      $(".size option").removeAttr('disabled', true );
						  $(".size").each(function()
						  {
						 
						  $(".size").not(this).find("option[value='"+ $(this).val() + "']").attr('disabled', true );
					      }
					      );
						   
						   
				  
			  }
			   function disableValue2()
			  {
				  
					 
					      $(".size option").removeAttr('disabled', true );
						  $(".size").each(function()
						  {
						 
						  $(".size option[value='"+ $(this).val() + "']").attr('disabled', true );
					      }
					      );
						   
						   
				  
			  }
			  $(function()
			  {
				  disableValue()
			  });
			  $('#up').MultiFile({
 
});


              </script>
