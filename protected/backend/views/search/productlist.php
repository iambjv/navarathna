    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl;?>/js/bootstrap/dist/assets/lib/datatables/css/demo_page.css">
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl;?>/js/bootstrap/dist/assets/lib/datatables/css/DT_bootstrap.css">
<script type="text/javascript" language="javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jquery.js"></script>
 <script src="<?php echo Yii::app()->request->baseUrl;?>/js/bootstrap/dist/assets/lib/datatables/jquery.dataTables.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl;?>/js/bootstrap/dist/assets/lib/datatables/DT_bootstrap.js"></script>
 
		<script type="text/javascript" charset="utf-8">
			var JQ=jQuery.noConflict()
			JQ(document).ready(function() {
				JQ('#dataTable').dataTable(
				{
					  
						"sDom": "<'pull-right'l>t<'row'<'col-lg-6'f><'col-lg-6'p>>",
						"sPaginationType": "bootstrap",
						"oLanguage": {
						"sLengthMenu": "Show _MENU_ entries"
						}

						}
				);
			} );
		</script>
<div class="row">
<div style="text-align:center;margin-top:3px;" >	<span class="label label-success" style="text-align:center"><?php echo Yii::app()->user->getFlash('success');?></span></div>
               <div style="float:right;padding: 10px 15px;"> <a href="../product/productlist" class="btn btn-metis-3 btn-sm btn-flat">View All Product</a>
   </div>
              <div class="col-lg-12">
                <div class="box">
                  <header>
                    <div class="icons">
                      <i class="fa fa-table"></i>
                    </div>
                    <h5>List Product</h5> 
                     <div style="text-align:center;padding: 10px 15px;"> <a href="../product/productadd" class="btn btn-metis-3 btn-sm btn-flat">Add New Product</a>
   </div>
                  </header>
                  <div id="collapse4" class="body">
					
                    <table id="dataTable" class="table table-bordered table-condensed table-hover table-striped">
                      <thead>
                        <tr>
                          <th style="text-align:center">Sl.</th>
                          <th>Category</th>
                          <th>Brand</th>
                          <th>Gender</th>
                          <th>Color</th>
                          <th>Size-Stock</th>
                          <th>Fit</th>
                          <th>Brand Fit</th>
                          <th>Fabric</th>
                           
                          <th>Collar</th>
                          <th>Sleeve</th>
                          <th>Occassion</th>
                          <th>Pattern</th>
                          <th>Price</th>
                         <th>Edit</th>
                        <!-- <th>Del</th>-->
                         <th>ACT</th>
                          
                        </tr>
                      </thead>
                      <tbody>
                        <?php
					  if(!empty($model))
					  {  
						  
						  $i=1;
						  foreach($model as $k=>$v)
						  {
							  ?>
							  <tr>
							  <td style="text-align:center"><?php echo  $i; ?></td>
							  <td><?php echo $v['brand']['brand_name']?></td>
							  <td><?php echo $v['category']['category_name']?></td>
							  <td><?php
							   $array=array("W"=>"Women","M"=>"Men","K"=>"Kids");
							   echo  @$array[$v['pdt_gender']];
							   
							     ?></td> 
							  <td>
							  <?php
										$color="";
										foreach($v['color'] as $k1=>$v1)
										{
										$color.=$v1['colorname']['color_name'].'<br>';
										}
										$color=rtrim($color,'<br>');
										echo $color;
							  ?>
							  </td>
							  <td>
							  <?php
									$size="";
									foreach($v['size'] as $k1=>$v1)
									{
									$size.=$v1['size']['size_name']."-".$v1['stock'].'<br>';
									}
									$size=rtrim($size,'&');
									echo $size;
							  ?>
							  </td>
							<td><?php echo  $v['fit']['fit_name'];  ?></td>
							<td><?php echo  $v['brandfit']['brandfit_name'];?></td>
							<td><?php echo  $v['fabric']['fabric_name']; ?></td>
							<td><?php echo  $v['collar']['collar_name']; ?></td>
							<td><?php echo  $v['sleeve']['sleeve_name'];  ?></td>
							<td><?php echo  $v['occasion']['occasion_name']; ?></td>
							<td><?php echo  $v['pattern']['pattern_name']; ?></td>
							<td><?php echo  $v['pdt_price']; ?></td>
					        <!-- <td>
								 <input type="text" style="width:60px;text-align:center" id="priority" maxlength="1"   onchange="updatePriority('<?php echo $v->product_id;?>',this)"  value="<?= $v->priority; ?>">
						         <img class="load" style="display:none;" src="<?php echo Yii::app()->request->baseUrl;?>/images/ajax-loader.gif">
						    </td>-->
								 
								   <td style="text-align:center">
									   <?
							          echo CHtml::link(CHtml::image( Yii::app()->request->baseUrl.'/images/admin/pencil.png', 'Edit', array('title'=>'Edit')),array('product/productadd','id'=>$v->product_id),array('title'=>'Edit'));  
                                      ?>
                                   </td>
                                   <!--
                                   <td style="text-align:center">
									   <?
                                       // echo CHtml::link(CHtml::image( Yii::app()->request->baseUrl.'/images/admin/cross.png', 'Delete', array('title'=>'Delete')),array('product/branddelete','id'=>$v->product_id),array('title'=>'Delete','confirm'=>'Are you sure'));  
							           ?>
							       </td>
							       -->
							       <td style="text-align:center">
									<?
									 if($v->status=='1')
								  {
									   $image = CHtml::image(Yii::app()->request->baseUrl.'/images/admin/active.gif', 'Status', array('title'=>'Active'));
									   $tit="Active";
								  }
								  else
								  {
									  $image = CHtml::image(Yii::app()->request->baseUrl.'/images/admin/deactive.gif', 'Status', array('title'=>'Deactive'));
									  $tit="Deactive";
								  }
								   
							        echo CHtml::link($image,array('search/productstatus','id'=>$v->product_id,'status'=>$v->status),array('title'=>$tit)); ?>
						 
							  </td>	  
							  
							  
							  
							  
							  
							  
							  
							  
							  </tr>
							  
							  <?
							  $i++;
						  }
					  }
					  else
					  {
						  echo "<tr><td colspan='100%' style='text-align:center'>No Result Found..</td></tr>";
					  }
					  ?>
                      </tbody>
                    </table>
                  
                  </div>
                </div>
              </div>
            </div><!-- /.row -->
<script>
$(document).ready(function(){
    $('#example').dataTable();
});

function updatePriority(id,pri)
{ 
	
	$(pri).parent().find(".load").show();
	 
		$.get("productpriority?id="+id+"&priority="+$(pri).val(),function(data){
			$(pri).parent().find(".load").hide();
			if(data=='1')
			{
				alert("Successfully Updated");
				 
			}
			else
			{
				alert("Failed to Update");
			}
			
			});
	 
}
</script>
   
