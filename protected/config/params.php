<?php

// this contains the application parameters that can be maintained via GUI
return array(
	// this is displayed in the header section
	'title'=>'My Yii Blog',
	'displaylimit'=>'8',
	'pricelimitstart'=>'100',
	'pricelimitend'=>'5000',
	// this is used in error pages
	'adminEmail'=>'webmaster@example.com',
	// number of posts displayed per page
	'postsPerPage'=>10,
	// maximum number of comments that can be displayed in recent comments portlet
	'recentCommentCount'=>10,
	// maximum number of tags that can be displayed in tag cloud portlet
	'tagCloudCount'=>20,
	// whether post comments need to be approved before published
	'commentNeedApproval'=>true,
	// the copyright information displayed in the footer section
	'copyrightInfo'=>'Copyright © 2014 udaysinger.com. All Rights Reserved.',
	'premiumOfferYear'=>'5',
	'premiumLimit'=>'3',
	'Offername'=>"Premium Purchase",
	'normalpurchase'=>"Normal Purchase",
	"Contactphone"=>"+91-484-4033540",
	"ContactEmail"=>"info@brammait.com",
	"Emailregisteration"=>"Uday Singer",
	"registrationemail"=>"njalilvineeth@gmail.com",
	"forgotemail"=>"njalilvineeth@gmail.com",
	"jobemail"=>"njalilvineeth@gmail.com",
);
