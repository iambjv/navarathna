<?php
class LoginController extends Controller
{
	public function actionIndex()
	{
		$user = new SiteUser;
		if(Yii::app()->request->isPostRequest)
		{
			if(isset($_POST['register']))
			{
				$user->setScenario("register");
				$user->attributes=$_POST;
				if($user->validate())
				{
				$user->password=md5($_POST['password']);
				$user->save() ;
				$u= $user->find(array("condition"=>"email=:email and status='1'","params"=>array(":email"=>$_POST['email'])));
				if($u)
				{
					if($user->password==$u->password)
					{
						Yii::app()->user->setstate("cartuserid",$u->user_id);
						Yii::app()->user->setstate("cartusername",$u->first_name);
						Yii::app()->user->setFlash("success","Successfully Logged in");
						 
					}
				}
					echo "1";exit;
				 
			    }
				else
				{
				  echo 	json_encode( $user->getErrors());exit;
				}
			}
			if(isset($_POST['login']))
			{
				$user->setScenario("login");
				$user->attributes=$_POST;
				if($user->validate())
				{
				$user->password=md5($_POST['password']);
				
				$u= $user->find(array("condition"=>"email=:email and status='1'","params"=>array(":email"=>$_POST['email'])));
				if($u)
				{
					if($user->password==$u->password)
					{
						Yii::app()->user->setstate("cartuserid",$u->user_id);
						Yii::app()->user->setstate("cartusername",$u->first_name);
						Yii::app()->user->setFlash("success","Successfully Logged in");
						echo "1";exit;
					}
					else
					{
						echo "2";exit;
					}
				}
				else
					{
						echo "2";exit;
					}
			    }
				else
				{
				  echo 	json_encode( $user->getErrors());exit;
				}
			}
		}
		$this->renderPartial("index",array('model'=>$user));
	}
	public function actionLogin()
	{
		$this->render("login");
	}
	public function actionHoauth()
	{
		$this->render("hoauth");
	}
	public function actionProfile()
	{
		 
	 
		$this->render("profile");
	}
	public function actionAccount()
	{
		if(!Yii::app()->user->getState("cartuserid"))
		{
			$this->redirect(Yii::app()->request->baseUrl.'/');
		}
		$this->layout="layout1";
		$user = new SiteUser;
		$user = $user->findByPk(Yii::app()->user->getState("cartuserid"));
		if(Yii::app()->request->isPostRequest)
		{
			
			 $user->attributes =$_POST['SiteUser'];
			 $user->conpassword=$_POST['SiteUser']['conpassword']; 
			 
		     if($user->conpassword !="")
		     {
				 
				 $user->password = md5($user->conpassword);
			 }
			 
			 
			 if($user->save())
			 {
			//	 print_r("SDFSDF");exit;
			 }
			 
		}
		else
		{
	
	     }
	     $user->conpassword="";
	     //$user->phone="";
		$this->render("account", array('model'=>$user));
	}
	public function actionOrder()
	{
		if(!Yii::app()->user->getState("cartuserid"))
		{
			$this->redirect(Yii::app()->request->baseUrl.'/');
		}
		$this->layout="layout1";
		$order = new Order;
		 
		$or = $order->findAll(array("condition"=>"userid=:id","params"=>array(":id"=>Yii::app()->user->getState("cartuserid")),"order"=>"order_date DESC"));
		 
		$this->render('order',array("order"=>$or));
	}
	public function actionReference()
	{
		 
		if(!Yii::app()->user->getState("cartuserid"))
		{
			$this->redirect(Yii::app()->request->baseUrl.'/');
		}
		$this->layout="layout2";
		$order = new Order;
		$or = $order->findAll(array("condition"=>"refererid=:id","params"=>array(":id"=>Yii::app()->user->getState("cartuserid")),"order"=>"order_date DESC"));
		 
		$this->render('reference',array("order"=>$or));
	}
	
	public function actionPremium()
	{
		 
		if(!Yii::app()->user->getState("cartuserid"))
		{
			$this->redirect(Yii::app()->request->baseUrl.'/');
		}
		$this->layout="layout2";
		$order = new Order;
		$or = $order->findAll(array("condition"=>"userid=:id and purchasetype='1'","params"=>array(":id"=>Yii::app()->user->getState("cartuserid")),"order"=>"order_date DESC"));
		 
		$this->render('premium',array("order"=>$or));
	}
}
