<?php

class ReferenceController extends Controller
{
	public $layout="data";
	public function actions()
	{  
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
				'foreColor'=>0x5F2C5E,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}
   
	public function beforeAction($action)
	{
		// Yii::app()->user->logout();
		if(!isset(Yii::app()->user->userid))
		{
			//Yii::app()->user->setState("urls",Yii::app()->request->url);
			$this->redirect(Yii::app()->request->baseUrl.'/registration/login');
		}
		else
		{

			return true;
		}


	}
	 
	 public function actionIndex($id=null)
	 {
		  $model= new  CustomerRefernce;
		  $criteria=new CDbCriteria();
		  $userid = Yii::app()->user->userid;
		  $criteria->condition="reference_to='{$userid}'";
		  $criteria->order="ref_id desc";
		  $count=$model->count($criteria);
          $pages=new CPagination($count);
    	  $pages->pageSize=3;
		  $pages->applyLimit($criteria);
		  $news=$model->findAll($criteria);
		  
		 
		  $this->render("index",array('model'=>$news, 'pages' => $pages));
	 }
	 
	 
		  
}		   
