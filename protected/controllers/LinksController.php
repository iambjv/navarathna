<?php
class LinksController extends Controller
{
	public $layout='layout1';
	public $meta_keywords = "";
	public $meta_description = "";
	public function actions()
	{  
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
				'foreColor'=>0x5F2C5E,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}
	public function actionIndex()
	{
		 
		$term = Yii::app()->request->getPathInfo() ;
		$links = new HomeLinks;
		$links = $links->find(array("condition"=>"link_url=:term","params"=>array(":term"=>$term)));
		$this->pageTitle = $links['meta_title'];
		$this->meta_keywords = $links['meta_keyword'];
		$this->meta_description = $links['meta_description'];
		$this->render("links", array("links"=>$links) );
    }
    public function actionSitemap()
    {
		$model= new  HomeLinks;
		$main=$model->findAll(array('condition'=>'parent_id="0" and status="0"','order'=>'priority asc'));
		$sub=$model->findAll(array('condition'=>'parent_id!="0" and status="0"','order'=>'priority asc'));		
		$this->render("sitemap",array('main'=>$main,'sub'=>$sub));
		
	}
}
