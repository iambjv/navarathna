<?php

class ImagegalleryController extends Controller
{
	 public $layout="layout2";
	 public function actions()
	{  
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
				'foreColor'=>0x5F2C5E,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}
   
	 public function actionIndex($id=null)
	 {
		 $model= new  PhotoGallery;
		  $criteria=new CDbCriteria();
		  
		  if($id!=null)
		  {
			  $criteria->condition="t.status='1' and t.category_id={$id}";
		  }
		  else
		  {
			  $criteria->condition="t.status='1'";
		  }	  
		  $criteria->order="id desc";
		  $count=$model->count($criteria);
          $pages=new CPagination($count);

		  // results per page
		  $pages->pageSize=10;
		  $pages->applyLimit($criteria);
		  $gallery=$model->findAll($criteria);
		  $this->render("index",array('model'=>$gallery, 'pages' => $pages ,'id'=>$id));
	 }
	 
	 
		  
}		   
