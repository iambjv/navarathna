<?php

class CartController extends Controller
{
 public function actionIndex()
 {  
	
	 $shop= new Shop;
	 
	 if(Yii::app()->request->isPostRequest)
	 {
		if(isset($_POST['id']))
		{
			
			$id=base64_decode($_POST['id']);
			$productid="";$stockid  ="";
			$poduct=explode("split",$id); 
			if(isset($poduct['0'])and isset($poduct['1']))
			{
				$productid= $poduct['0'];
				$stockid  = $poduct['1'];
			}
			
			$product = new Product;
			$item = $product->find(array("condition"=>"t.product_id=:id and t.status='1' and t.pdt_stock>0 ","params"=>array(":id"=>$productid)));
		
			if($item)
			{
				$cart_array =  $shop->getCartContent();
				if(isset($_POST['remove']))
				{
 
					unset($cart_array[$_POST['remove']]);
				}
				if(isset($_POST['qty']))
				{
					$qty=$_POST['qty'];
				}
				else
				{
					$qty=1;
				}
				$cart_array[$_POST['id']]=$qty; 
				$shop->setCartContent($cart_array);
				echo "1";
			}
			else
			{
				echo "2";
			}
		}
	 }
	 else
	 {
		 echo "2";
	 }
     ;exit;
	 $this->render("index");
 }
 public function actionCartlist()
 {
	 $order = new Order;
	 $user=  $order->with('orderuser')->findAll(array("condition"=>"purchasetype='1'" , "group"=>"userid"));
	 $this->layout="layout1";
	 $shop= new Shop;
	 $product= new Product;
	 $itm = array();
	 $amount=0;
	 if($items=$shop->getCartContent()) 
	 {
		 
		 foreach($items as $k=>$v)
		 {
			  $productid="";$stockid  ="";
			 $poduct=explode("split",base64_decode($k)); 
			 if(isset($poduct['0'])and isset($poduct['1']))
			 {
				$productid= $poduct['0'];
				$stockid  = $poduct['1'];
			 }
		  $v1 =	$product->with('category')->find(array("condition"=>"t.product_id=:id","params"=>array(":id" => $productid)));
 	  
		  if($v1)
		  {
				$name = $v1['pdt_title'];
				$name .= "&nbsp;".'-';
				
				
				if($v1['category']['category_name']!="")
				{
					$name .= "&nbsp;".$v1['category']['category_name'];
				}
             
			  if($v1->pdt_stock==0)
			  {
				  $stk=0;
				  	 
			  }
			  else if((int)$v > (int)$v1->pdt_stock )
			  {
				   $stk=  $v1->stock -  $v ;
				    
			  }
			  else
			  {
			
				  $stk=$v;
				  
			  }
			  $amount +=  $v1->pdt_price*$stk;
			  
		 
			 
			  $itm[]=array("pdtid"=>$v1->product_id, "stock" => (int) $v1->pdt_stock,"allowedstock"=>(int) $stk ,"name"=>$name,"status"=>$v1->status,"price"=>$v1->pdt_price,"image"=>$v1->pdt_image,"qty"=>(int) $v,"id"=>base64_encode($v1->product_id."split".$v1->pdt_stock));
			  
			  
		  }
		 }
	 }
	 
	 
     $this->render( "cartlist",array("itemlist"=>$itm,"total"=>$amount,"shop"=>$shop ,'user'=>$user));
  }
  public function  actionCartfromhome()
  {
	  
	  if(Yii::app()->request->isPostRequest)
	  {
		if(isset($_POST['id']) and $_POST['id']!="")
		{
			 $shop= new Shop;
			$id=base64_decode($_POST['id']);
			$size = new ProductSize;
			$sizeid = $size->find(array("condition"=>"size_name=:name","params"=>array(":name"=>$_POST['sizename'])));
			if($sizeid)
			{
			$product = new Product;
			$v1 = $product->with('category','brand','fit','fabric','collar','sleeve','occasion','pattern','sizeone.sizetwo')->find(array("condition"=>"sizeone.stock_size=:id and t.status='1' and sizeone.stock>0 and t.product_id=:productid ","params"=>array(":productid"=>$id,":id"=>$sizeid->size_id)));
			 
			 
			 
			if($v1)
			{
				 $gender=array("M" => "Men", "W"=>"Women" , "K" => "Kids");
				$color=$v1->pdt_colorname;
			    $name="";
				$name .= @$gender[$v1->pdt_gender];
				$name .= "&nbsp;".$color;
				
				if($v1['fit']['fit_name']!="")
				{
					$name .= "&nbsp;".$v1['fit']['fit_name'];
				}
				if($v1['brandfit']['brandfit_name']!="")
				{
					$name .= "&nbsp;".$v1['brandfit']['brandfit_name'];
				}
				if($v1['fabric']['fabric_name']!="")
				{
					$name .= "&nbsp;".$v1['fabric']['fabric_name'];
				}
				if($v1['collar']['collar_name']!="")
				{
					$name .= "&nbsp;".$v1['collar']['collar_name'];
				}
				if($v1['sleeve']['sleeve_name']!="")
				{
					$name .= "&nbsp;".$v1['sleeve']['sleeve_name'];
				}
				if($v1['occasion']['occasion_name']!="")
				{
					$name .= "&nbsp;".$v1['occasion']['occasion_name'];
				}

				if($v1['pattern']['pattern_name']!="")
				{
					$name .= "&nbsp;".$v1['pattern']['pattern_name'];
				}
				if($v1['category']['category_name']!="")
				{
					$name .= "&nbsp;".$v1['category']['category_name'];
				}
                 if($v1->pdt_offerpercentegae>0)
                 {
					 $price=$v1->pdt_offerprice;
				 }
				 else
				 {
					 $price=$v1->pdt_price;
				 }
				 $image = $v1->pdt_image;
				
				$cart_array =  $shop->getCartContent();
				 
				$cart_array[base64_encode($v1->sizeone->productid."split".$v1->sizeone->stock_size)]=1; 
				$shop->setCartContent($cart_array);
				$count=sizeof($shop->getCartContent());
				echo json_encode(array("name"=>$name,"price"=>number_format($price),'image'=>$image,"brand"=>$v1['brand']['brand_name'],"count"=>$count,"cartprice"=> $v1->pdt_price ,"carofferprice"=> $v1->pdt_offerprice ,"cartpercentage"=> $v1->pdt_offerpercentegae,"id"=>base64_encode($v1->sizeone->productid."split".$v1->sizeone->stock_size),"size_name"=>$v1->sizeone->sizetwo->size_name ));
			}
			else
			{
				echo "2";
			}
		    }
		    else
		    {
				echo "2";
			}
		}
	  }
	  else
	  {
		  echo "2";
	  }
	  exit;
  }
  public function actionRemove()
  {
	  if(Yii::app()->request->isPostRequest)
	  {
	    $id= $_POST['id'] ;
	    
		$productid=""; $stockid  ="";
		$poduct=explode("split",base64_decode($_POST['id'])); 
		if(isset($poduct['0'])and isset($poduct['1']))
		{
		$productid= $poduct['0'];
		$stockid  = $poduct['1'];
		}
		   
	   $shop= new Shop;
	   $ar=$shop->getCartContent();
	   $count=0;
	   if(isset($ar[$id]))
	   {
		 $count  = $ar[$id]; 
	   }
	   unset($ar[$id]);
	   $product = new Product;
	   $amount=0;
	   $v1=$product->find(array("condition"=>"product_id=:id","params"=>array(":id"=>$productid)));
	   
	   if($v1)
	   {
		
		$amount+=$v1->pdt_price;
		
	   }
	   $shop->setCartContent($ar);
	    echo json_encode(array("amount"=>$amount*$count));
      }
      else
      {
		  echo "2";
	  }
	  exit;
  }
  public function actionStocks()
   {
	  if(Yii::app()->request->isPostRequest)
	  {
		 
		  	 $productid="";$stockid  ="";
			 $poduct=explode("split",base64_decode($_POST['stockid'])); 
			 if(isset($poduct['0'])and isset($poduct['1']))
			 {
				$productid= $poduct['0'];
				$stockid  = $poduct['1'];
			 }
		   
		$product = new Product;
		$pr=$product->findAll(array("condition"=>"product_id=:id","params"=>array(":id" => $productid))); 
	    $ar =array();
	    if($pr)
	    {
			 
			foreach($pr as $k=>$v)
			{    
				//$ar[] = array("maxstock"=>$v->pdt_stock,"id"=>base64_encode($k->product_id."split".$k->pdt_stock));					 
				 
				 
			}
		}
		echo json_encode($ar);
		exit;
	  }
	 
  }
}
