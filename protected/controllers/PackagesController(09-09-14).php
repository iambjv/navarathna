<?php

class PackagesController extends Controller
{
	public $layout="layout2";
	public function actions()
	{  
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xe4e4e4,
				'foreColor'=>0x000000,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}
   
   
	 public function actionIndex($sub_id=null,$main_id=null)
	 {
		  $model= new Packages;
		  $sub_category = new PackageSubCategory;
		  $sub_category = $sub_category->find(array("condition"=>"status='1' and sub_category_id=$sub_id"));
		  $criteria=new CDbCriteria();
		  $criteria->condition="t.status='1' and main_category_id=$main_id and sub_category_id=$sub_id"; 
		  $criteria->order="package_id ASC";
		  $count=$model->count($criteria);
          $pages=new CPagination($count);

		  // results per page
		  $pages->pageSize=6;
		  $pages->applyLimit($criteria);
		  $pdt=$model->findAll($criteria);
		  $this->render("index",array('model'=>$pdt, 'pages' => $pages ,'main_id' => $main_id ,'sub_id' => $sub_id,'sub_category'=>$sub_category));

	 }
	 public function actionDetails($id=null,$sub_id=null,$main_id=null)
	 {
		 $this->layout="layout2";
		 $package = new Packages;
		 $packages = $package->findAll(array("condition"=>"status='1' and main_category_id=$main_id and sub_category_id=$sub_id"));
		 $pack_count = count($packages);
		 $model =$package->with('sub_category','package_days')->findAll(array("condition"=>"t.package_id=:id and package_days.package_id=:id","params"=>array(":id"=>$id),"order"=>"package_days.package_day_title ASC"));
		 $this->render("details",array('model'=>$model,'pack_count'=>$pack_count,'main_id' => $main_id ,'sub_id' => $sub_id,'packages'=>$packages));
		 
	 }
	 public function actionGallery($id=null)
	 {
		$this->layout = "layout2";
		$package_days = new PackageDays;
		$model =$package_days->with('imagesdq','imagehotel')->findAll(array("condition"=>"t.package_day_id=:id","params"=>array(":id"=>$id)));
		$this->render("gallery",array('model'=>$model));

	 }
	 public function actionPackageBookings($id=null)
	{
		$this->layout="layout1";
		$model=new PackageBookings;
		$countries = new Countries;
		$countries = $countries->findAll(array("order"=>"name ASC"));
		$states = new States;
		$states = $states->findAll(array("order"=>"name ASC"));
		if(isset($_POST['PackageBookings']))
		{
			$model->attributes=$_POST['PackageBookings'];
			//print_r($_POST);exit;
			$model->package_id = $id;
			$model->childs = $_POST['PackageBookings']['childs'];
			$model->state_id = $_POST['PackageBookings']['state_id'];
			$model->comments = $_POST['PackageBookings']['comments'];

			$package=new Packages; 
			$package= $package->with(array('sub_category'))->find(array("condition"=>"t.status='1' and t.package_id=$id","order"=>"package_title ASC")); 
			//echo $package->package_title;
			//echo $package->sub_category->sub_category_name;
			$pack_name = $package->sub_category->sub_category_name." - ".$package->package_title;
			
			if($model->validate())
			{
				if($model->save())
				{
				
				//MAil To Process	
				/*
				Yii::import("ext.YiiMailer.YiiMailer");
				$mail = new YiiMailer();
				$mail->setView('package_booking');
				$mail->setData(array('pack_name' => $pack_name));
				$mail->setFrom($_POST['PackageBookings']['email'], $_POST['PackageBookings']['name']);
				$mail->setTo(array('tour@ttpkerala.com'));
				$mail->setSubject('Kerala Honeymoon Package Booking');
				if($mail->send())
				{
					
					Yii::app()->user->setFlash('success','Thank you for contacting us. We will respond to you as soon as possible.');
					$this->refresh();
				}
				else 
				{
					// print_r($mail->getError());exit;	
					
					Yii::app()->user->setFlash('success','Email not send.');
				}
				*/
                //
					
				/*$headers="From: test@gamil.com\r\nReply-To: {$model->contact_email}";
				mail(Yii::app()->params['adminEmail'],$model->subject,$model->body,$headers);
				Yii::app()->user->setFlash('contact','Thank you for contacting us. We will respond to you as soon as possible.');
				*/
				Yii::app()->user->setFlash('success','Thank you for contacting us. We will respond to you as soon as possible.');
				$this->refresh();
			    }
			   
			}
			 
		}
		$this->render('package_bookings',array('model'=>$model,'countries'=>$countries,'states'=>$states));
	}	
	
		  
}		   
