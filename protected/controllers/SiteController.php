<?php

class SiteController extends Controller
{
	public $layout='main';
	/**

	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex()
	{
		// renders the view file 'protected/views/site/index.php'
		// using the default layout 'protected/views/layouts/main.php'
		 //$this->layout="main";
		$this->render('index');
	}
	public function actionAboutUs()
	{
		// renders the view file 'protected/views/site/index.php'
		// using the default layout 'protected/views/layouts/main.php'
		$this->render('about-us');
	}
	public function actionVideos()
	{
		// renders the view file 'protected/views/site/index.php'
		// using the default layout 'protected/views/layouts/main.php'
		$this->render('videos');
	}
	public function actionBusiness()
	{
		// renders the view file 'protected/views/site/index.php'
		// using the default layout 'protected/views/layouts/main.php'
		$this->render('business');
	}
	public function actionBranches()
	{
		// renders the view file 'protected/views/site/index.php'
		// using the default layout 'protected/views/layouts/main.php'
		$this->render('branches');
	}
	public function actionBrands()
	{
		// renders the view file 'protected/views/site/index.php'
		// using the default layout 'protected/views/layouts/main.php'
		$this->render('brands');
	}
	public function actionCareer()
	{
		// renders the view file 'protected/views/site/index.php'
		// using the default layout 'protected/views/layouts/main.php'
		$this->render('career');
	}
	public function actionContactUs()
	{
		// renders the view file 'protected/views/site/index.php'
		// using the default layout 'protected/views/layouts/main.php'
		$this->render('contact-us');
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}

	/**
	 * Displays the contact page
	 */
	/*public function actionContact()
	{
		$model=new ContactForm;
		if(isset($_POST['ContactForm']))
		{
			$model->attributes=$_POST['ContactForm'];
			if($model->validate())
			{
				$name='=?UTF-8?B?'.base64_encode($model->name).'?=';
				$subject='=?UTF-8?B?'.base64_encode($model->subject).'?=';
				$headers="From: $name <{$model->email}>\r\n".
					"Reply-To: {$model->email}\r\n".
					"MIME-Version: 1.0\r\n".
					"Content-Type: text/plain; charset=UTF-8";

				mail(Yii::app()->params['adminEmail'],$subject,$model->body,$headers);
				Yii::app()->user->setFlash('contact','Thank you for contacting us. We will respond to you as soon as possible.');
				$this->refresh();
			}
		}
		$this->render('contact',array('model'=>$model));
	}*/

	/**
	 * Displays the login page
	 */
	public function actionContact()
	{
		$this->layout="layout3";
		$model=new Contact;
		if(isset($_POST['Contact']))
		{
			//print_r($_POST['Contact']);exit;
			$model->attributes=$_POST['Contact'];
			if($model->validate())
			{
				if($model->save())
				{
				$email=$_POST['Contact']['contact_email'];
				$name=$_POST['Contact']['contact_name'];
				//MAil To Process
				Yii::import("ext.YiiMailer.YiiMailer");
				$mail = new YiiMailer();
				$mail->setView('contact');
				$mail->setData(array('message' => 'Message to send', 'name' => '$name', 'description' => 'Contact form'));
				$mail->setFrom($_POST['Contact']['contact_email'],$_POST['Contact']['contact_name']);
				$mail->setTo(array('jayadev@brammait.com'));
				$mail->setSubject('Holiday Kerala - Contact');
				if ($mail->send())
				{
					Yii::app()->user->setFlash('success','Thank you for contacting us. We will respond to you as soon as possible.');
					$this->refresh();
				}
				else 
				{
					Yii::app()->user->setFlash('success','Email not send.');
				}
				$headers="From: test@gamil.com\r\nReply-To: {$model->contact_email}";
				mail(Yii::app()->params['adminEmail'],$model->subject,$model->body,$headers);
				
				Yii::app()->user->setFlash('success','Thank you for contacting us. We will respond to you as soon as possible.');
				$this->refresh();
			    }//else{ echo "not saved";}
			}	//else{ echo "no validation";}
		}        //else{ echo "POST is empty";}
		$this->render('contact',array('model'=>$model));
	}


	//serach
    public function actionSearch()
    {
		//print_r($_GET);exit;
		$this->layout="layout4";
		$model = new Packages;
		
		if(isset($_GET))
		{	
			$main_cat=$_GET['main_cat'];
			$dayy=$_GET['dayy'];
			if($main_cat!='0' && $dayy!='0')
			{
			$model = $model->findAll(array("condition"=>"status='1' and sub_category_id='$main_cat' and days='$dayy'")); 
			}
			else if($main_cat!='0' && $dayy=='0')
			{
			$model = $model->findAll(array("condition"=>"status='1' and sub_category_id='$main_cat'")); 
			}
			else if($main_cat=='0' && $dayy!='0')
			{
			$model = $model->findAll(array("condition"=>"status='1' and days='$dayy'")); 
			}
			else if($main_cat=='0' && $dayy=='0')
			{
			$model = ""; 
			}
		}
	   $this->render("search", array("model"=>$model));
	}
	
	public function actionBook()
	{
		$model=new PackageBookings;
		$booking;
		if(isset($_POST['PackageBookings']))
		{
			
				print_r($_POST['PackageBookings']);
			
			$model->attributes=$_POST['PackageBookings'];
			if($model->validate())
			{
				if($model->save())
				{
				//print_r($_POST['Quickcontact']['contact_name']);exit;
				
				//MAil To Process	
				
				Yii::import("ext.YiiMailer.YiiMailer");
				$mail = new YiiMailer();
				$mail->setView('contact');
				$mail->setData(array('description' => ''));
				$mail->setFrom( $_POST['PackageBookings']['email']);	
				$mail->setTo(array('jayadev@brammait.com'));		
				////$mail->setTo(array('info@gempac.com'));
				//$mail->setBcc(array('jayadev@brammait.com'));
				$mail->setSubject('Enquiry');
				if ($mail->send())
				{
					//Yii::app()->user->setFlash('success','Thank you for contacting us. We will respond to you as soon as possible.');
					$this->redirect(Yii::app()->request->urlReferrer);
				}
				else 
				{
					Yii::app()->user->setFlash('success','Email not send.');
					$this->redirect(Yii::app()->request->urlReferrer);
				}
				
               
			    }
			   
			}
			 
		}
		
		$this->render($this->redirect(Yii::app()->request->urlReferrer),array('booking'=>$booking));
	 }
	

	//beach, wildlife, houseboat.....
    public function actionKerala($id=null)
    {
		$this->layout="layout4";
		$model_d = new PackageDays;
		$model = new Package;
		if(isset($_GET))
		{	
			$id=$_GET['id'];
			
			if($id==1)
			{
			$model_d = $model_d->findAll(array("condition"=>"status='1' and package_day_title LIKE 'ayurveda' or short_description LIKE 'ayurveda'")); 
			$catgry="Ayurveda";
			}
			else if($id==2)
			{
			$model_d = $model_d->findAll(array("condition"=>"status='1' and package_day_title LIKE 'houseboat' or short_description LIKE 'houseboat'")); 
			$catgry="Houseboat";
			}
			else if($id==3)
			{
			$model_d = $model_d->findAll(array("condition"=>"status='1' AND short_description LIKE 'National Park'")); 
			$catgry="Wildlife";
			}
			else if($id==4)
			{
			$model_d = $model_d->findAll(array("condition"=>"status='1' AND short_description LIKE 'beach'")); 
			$catgry="Beach";
			}
		}
	   $this->render("kerala", array("model_d"=>$model_d, "catgry"=>$catgry));
	}
	




	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{
		//if (!defined('CRYPT_BLOWFISH')||!CRYPT_BLOWFISH)
			//throw new CHttpException(500,"This application requires that PHP was compiled with Blowfish support for crypt().");

		$model=new SiteUser;

		// if it is ajax validation request
		

		// collect user input data
		if(isset($_POST['SiteUser']))
		{
			 $model->setScenario("login") ;
		    $model->attributes=$_POST['SiteUser'];
			
			 // validate user input and redirect to the previous page if valid
			if($model->validate())
			{
				if($model->cheakpass($model->email,$model->password))
				{
				
				$this->render('siteuser_home'); }
		       }
		      
		    
	     }
	          $this->render('login',array('model'=>$model));
	    
		

		
	}

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}
	
	//===========================SIGN UP ======*********8
	
	
	public function actionSignup()
	{

		$model=new SiteUser;

		// if it is ajax validation request
		
		// collect user input data
		if(isset($_POST['SiteUser']))
		{
			 
			$model->attributes=$_POST['SiteUser'];
			
			$pass=$model->password;
			$model->password= md5($pass);
			$model->conpassword=md5($pass);
			if($model->save())
			{
				   Yii::app()->user->setFlash('success','Thank you for Sign Up');
				    $this->redirect('login');
			}else 
			{
				
				print_r($model->getErrors());
				echo "err";
			}	
		}
		
		// display the login form
		
		$this->render('signup',array('model'=>$model));
	}
     public function actionContact1()
	{
		  
		$model=new Contact;
		if(isset($_POST['Contact']))
		{
			$model->attributes = $_POST['Contact'];
			 if($model->save())
			 {
				 echo "1";exit;
			 }
			 else
			 {
			echo 	json_encode($model->getErrors());exit;
			 }
		}
		 
	}
	

	
	
	
	public function actionSettings()
	{
		$this->layout="data";
		$active=0;
		$model= new MemberRegistartion;
		if(isset(Yii::app()->user->userid))
		{
	    	$model = $model->findByPk(Yii::app()->user->userid);
     	}
		if(Yii::app()->request->isPostRequest)
		{
			
			//echo Yii::app()->user->userid;exit;
			
		    if(isset($_POST['MemberRegistartion']['password']))
		    {       
				    $active=1;
					$model->setScenario('changepassord'); 
					$model->attributes= $_POST['MemberRegistartion'];
					if($model->validate())
					{
						$pass= $model->password;
						$passcon= $model->conpassword;
						
						$model->password=md5($model->password);
						$model->conpassword=md5($model->conpassword);
						if($model->save())
						{
							Yii::app()->user->setFlash("success","Successfully changed password..");
							$model->password="";
							$model->conpassword="";
						}
						 
					}
					else
					{
						 
						$model->password = $pass;
						$model->conpassword = $passcon;
					}
		   }
		    if(isset($_POST['MemberRegistartion']['outlet_name']))
			{
				 
				$model->conpassword=$model->password;
				$model->attributes=$_POST['MemberRegistartion'];
				if($model->save())
				{
					 Yii::app()->user->setFlash("success5","Successfully Updated");
				}
				$model->password="";
				$model->conpassword="";
				 
			}
			 if(isset($_POST['MemberRegistartion']['contact_email']) and !isset($_POST['MemberRegistartion']['outlet_name'] ))
			{
				 
				 
				$model->attributes=$_POST['MemberRegistartion'];
				$model->setScenario("forgot");
				if($model->validate())
				{
					$for =  $model->find(array("condition"=>"contact_email=:email","params"=>array(":email"=>$model->contact_email)));
					if($for)
					{
						//	echo $this->randomPassword();exit;
						Yii::import('ext.YiiMailer.YiiMailer');
						$mail = new YiiMailer();
						$mail->setView('forgot');
						$password = $this->randomPassword(); 
						 
						$model->updateByPk($for->id,array('password'=>md5($password)));
						$_POST['MemberRegistartion']['password'] = $password ;
						 
						
						$mail->setData($_POST['MemberRegistartion']);
						$mail->setFrom($reg->contact_email, $reg->outlet_name);
						$mail->setTo(array(Yii::app()->params['forgotemail']));
						$mail->setSubject('Password forgot  for the account of  Data');
						if($mail->send())
						{
							Yii::app()->user->setFlash("success3"," Your password send    to your account . Please check your email ");
						}
						else
						{
							Yii::app()->user->setFlash("success3"," Failed to send email  to your account . Please  try again  ".$mail->getError());
						}
					}
					else
					{
						Yii::app()->user->setFlash("success3","Not found any account under this username . Please  try again  ");
					}
				}
			}
		 
			 
		 
		}
		else
		{
				$model->password ="";
				$model->conpassword = "";
		}
		
		$this->render("settings",array('model'=>$model,'active'=>$active));
	}
	
	public function randomPassword() {
		$alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
		$pass = array(); //remember to declare $pass as an array
		$alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
		for ($i = 0; $i < 8; $i++) {
			$n = rand(0, $alphaLength);
			$pass[] = $alphabet[$n];
		}
		return implode($pass); //turn the array into a string
    }
}