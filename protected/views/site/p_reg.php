<html>
<head>
<title>
Hospital Management
</title>
<script>
function validate()
{
	var name=document.forms["reg"]["name"].value;
	if(name == null || name == "")
	{
		alert("Name Must be required");
		return false;
	}
	var dob=document.forms["reg"]["dob"].value;
	if(dob == null || dob == "")
	{
		alert("date of birth Must be required");
		return false;
	}
	var mail = document.forms["reg"]["mail"].value;
    var atpos = mail.indexOf("@");
    var dotpos = mail.lastIndexOf(".");
    if (atpos<1 || dotpos<atpos+2 || dotpos+2>=mail.length) 
    {
        alert("Not a valid e-mail address");
        return false;
    }
    var username=document.forms["reg"]["username"].value;
	if(username == null || username == "")
	{
		alert("username Must be required");
		return false;
	}
	var mobileno=document.forms['reg']['mobileno'].value;	
    if(mobileno=="") 
	{
		alert('enter the mobile number');
		return false; 
	}
	else if(mobileno==""|| !mobileno.match(/^[0-9]{10,10}$/))
	{
		alert('enter the mobile correctly');
		return false;
	}
	var pass=document.forms['reg']['password'].value;	
    if(pass=="") 
	{
		alert('enter the password');
		return false; 
	}
	else if(pass==""|| !pass.match(/^{10,10}/))
	{
		alert('enter the password correctly');
		return false;
	}
	/*if((reg.gender[0].checked == false) && (reg.gender[1].checked == false))
	{
		alert ( "Please choose your Gender: Male or Female" );
		return false;
	}*/
}
</script>
<style>
body
{
	margin:0px;
	padding:0px;
}
.main_header{
	width:100%;
	margin:0px;
	padding:0px;
	background-color:#000000;
	height:800px;
	}
.main_footer{
	width:100%;
	margin:0px;
	padding:0px;
	background-color:#000000;
	height:800px;
	}
.auto{
	margin:auto;
	padding:0px;
	width:1266px;
	height:800px;
	background-color:green;
	}
.subbox1{
	width:100%;
	height:100px;
	background-color:#fff;
	}
.subbox2{
	width:100%;
	height:100px;
	background-color:#fff;
	}
h1{
	font:5;
	color:#fff;
	}
</style>
</head>
<body>
	<form action="reg_code.php" method="post" name="reg" onsubmit="return validate()">
		<div class="main_header">
			<div class="auto">
				<div class="subbox1"></div>
					<p align="center">
					<br>
					<h1 align="center">
						 Patient Registration
					</h1>
					<br>
					<br>
					<table align="center">
						<tr>
							<td>
								Name:
							</td>
							<td>
								<input type="text" name="name">
							</td>
						</tr>
						<tr>
							<td>
								Date of Birth:
							</td>
							<td>
								<input type="text" name="dob">
							</td>
						</tr>
						<tr>
							<td>
								Address:
							</td>
							<td>
								<textarea name="address"></textarea>
							</td>
						</tr>
						<tr>
							<td>
								Distict:
							</td>
							<td>
								<select name="distict">
									<option value="thrissur">Thrissur</option>
									<option value="ernakulam">Ernakulam</option>
									<option value="kotayam">Kottayam</option>
									<option value="palakkad">Palakkad</option>
									<option value="idukki">Idukki</option>
									<option value="alapuzha">Alappuzha</option>
									<option value="malapuram">Malapuram</option>
									<option value="kozhokode">Kozhikode</option>
									<option value="trivandram">Trivandram</option>
									<option value="kannur">Kannur</option>
								</select>
							</td>
						</tr>
						<tr>
							<td>
								Gender:
							</td>
							<td>
								<input type="radio" name="gender" value="male">Male
								<input type="radio" name="gender" value="female">Female
							</td>
						</tr>
						<tr>
							<td>
								Mobile no:
							</td>
							<td>
								<input type="text" name="mobileno">
							</td>
						</tr>
						<tr>
							<td>
								E-mail:
							</td>
							<td>
								<input type="text" name="mail">
							</td>
						</tr>
						<tr>
							<td>
								Username:
							</td>
							<td>
								<input type="text" name="username">
							</td>
						</tr>
						<tr>
							<td>
								Password:
							</td>
							<td>
								<input type="password" name="password">
							</td>
						</tr>
						<tr align="center">
							<td colspan="2">
								<input type="submit" name="submit" value="submit">
							</td>
						</tr>
						
					</table>
					</p>
			</div>
		</div>
		<div class="subbox2"></div>
	</form>
</body>
</html>
