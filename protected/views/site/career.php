<!DOCTYPE html>
<html lang="en">
<meta http-equiv="content-type" content="text/html;charset=utf-8" />
<head>
    </head>
  <body>
<div class="container"> 
<div class="clear-margin"></div>
  <div class="row">
  <div class="col-md-12">  
  <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/inner-banner1.jpg" class="img-responsive">
  </div>
  </div>
  </div>
<div class="container">
<div class="row">
<div class="container"><h2>Career</h2></div>
<div class="col-md-12">
<p>NAVARTHA, a name often chanted by the majority of housewives in Kerala, has a story of success that rolls back to 1974. It is in this year, a young vibrant Engineer, leaving his lucrative job, came out of it to pursue his passion for Entrepreneurship, which time proved to be a wise step taken at a heaven-sent moment.

The eyes of Mr.Yousuf wells up with joy even today, when he happens to mention the name of his brother, Late. Mr. Ibrahimkutty, who had motivated him since the demise of his father during his infantile. He was the mentor and guiding spirit in all walks of the chairman's life. He honored Mr.Ibrahimkutty by making him inaugurate his new venture as a distributor of LPG, NAVARTHA GAS, in 1984.

Transparency in dealings and prompt execution and services, earned him reputation and an impetus to go ahead with more innovative ideas. The harmonious blending of the business expertise of Mr.Yousuf, the Chairman and the business acumen of Mr.V.A Ajmal, the Managing Director gave a tremendous and faster growth to the company. He cherished the ideas of having a Home Appliances wing for the company in 2003 that brought about a great evolution...</p>
</div>
</div>
</div>
<div class="container">
<hr>
<div class="row">
<div class="container"><h2>Our Branches</h2></div>
<div class="col-xs-6 col-sm-6 col-md-3 col-lg-3 pad-botom">
<img src="<?php echo Yii::app()->request->baseUrl; ?>/images/b1.jpg" class="img-responsive" />
<div class="box-st">
Navaratna Hypermarket, Mughal 
Mall, kodungallur
</div>
</div>
<div class="col-xs-6 col-sm-6 col-md-3 col-lg-3 pad-botom">
<img src="<?php echo Yii::app()->request->baseUrl; ?>/images/b2.jpg" class="img-responsive" />
<div class="box-st">
Navaratna Hypermarket, Mughal Mall, Moonupeedika
</div>
</div>
<div class="col-xs-6 col-sm-6 col-md-3 col-lg-3 pad-botom">
<img src="<?php echo Yii::app()->request->baseUrl; ?>/images/b3.jpg" class="img-responsive" />
<div class="box-st">
Navaratna Hypermarket, Mughal
Mall, Chalakkudy
</div>
</div>
<div class="col-xs-6 col-sm-6 col-md-3 col-lg-3 pad-botom">
<img src="<?php echo Yii::app()->request->baseUrl; ?>/images/b4.jpg" class="img-responsive" />
<div class="box-st">
Navaratna Hypermarket, Mughal
Mall, Mala
</div>
</div>
</div>
</div>
  </body>
</html>
