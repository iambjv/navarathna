<!DOCTYPE html>
<html lang="en">
<meta http-equiv="content-type" content="text/html;charset=utf-8" />
<head>
    </head>
  <body>
<div class="container"> 
<div class="clear-margin"></div>
  <div class="row">
  <div class="col-md-12">
  
  <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/inner-banner1.jpg" class="img-responsive">
  </div>
  </div>
  </div>

<div class=" container-fluid ">
<div class="container">
<h2>Our Brands</h2>
<div class="clear-margin">

<div class="row">

<div class="col-xs-6 col-sm-2 col-md-2 col-lg-2 brand-b-margin"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/brand1.jpg" class="img-responsive" title="" alt="" /></div>
<div class="col-xs-6 col-sm-2 col-md-2 col-lg-2 brand-b-margin"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/brand2.jpg" class="img-responsive" title="" alt="" /></div>
<div class="col-xs-6 col-sm-2 col-md-2 col-lg-2 brand-b-margin"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/brand3.jpg" class="img-responsive" title="" alt="" /></div>
<div class="col-xs-6 col-sm-2 col-md-2 col-lg-2 brand-b-margin"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/brand4.jpg" class="img-responsive" title="" alt="" /></div>
<div class="col-xs-6 col-sm-2 col-md-2 col-lg-2 brand-b-margin"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/brand5.jpg" class="img-responsive" title="" alt="" /></div>
<div class="col-xs-6 col-sm-2 col-md-2 col-lg-2 brand-b-margin"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/brand6.jpg" class="img-responsive" title="" alt="" /></div>

<div class="col-xs-6 col-sm-2 col-md-2 col-lg-2 brand-b-margin"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/brand7.jpg" class="img-responsive" title="" alt="" /></div>
<div class="col-xs-6 col-sm-2 col-md-2 col-lg-2 brand-b-margin"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/brand8.jpg" class="img-responsive" title="" alt="" /></div>
<div class="col-xs-6 col-sm-2 col-md-2 col-lg-2 brand-b-margin"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/brand9.jpg" class="img-responsive" title="" alt="" /></div>
<div class="col-xs-6 col-sm-2 col-md-2 col-lg-2 brand-b-margin"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/brand10.jpg" class="img-responsive" title="" alt="" /></div>
<div class="col-xs-6 col-sm-2 col-md-2 col-lg-2 brand-b-margin"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/brand11.jpg" class="img-responsive" title="" alt="" /></div>
<div class="col-xs-6 col-sm-2 col-md-2 col-lg-2 brand-b-margin"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/brand12.jpg" class="img-responsive" title="" alt="" /></div>

<div class="col-xs-6 col-sm-2 col-md-2 col-lg-2 brand-b-margin"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/brand13.jpg" class="img-responsive" title="" alt="" /></div>
<div class="col-xs-6 col-sm-2 col-md-2 col-lg-2 brand-b-margin"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/brand14.jpg" class="img-responsive" title="" alt="" /></div>
<div class="col-xs-6 col-sm-2 col-md-2 col-lg-2 brand-b-margin"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/brand15.jpg" class="img-responsive" title="" alt="" /></div>
<div class="col-xs-6 col-sm-2 col-md-2 col-lg-2 brand-b-margin"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/brand16.jpg" class="img-responsive" title="" alt="" /></div>
<div class="col-xs-6 col-sm-2 col-md-2 col-lg-2 brand-b-margin"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/brand17.jpg" class="img-responsive" title="" alt="" /></div>
<div class="col-xs-6 col-sm-2 col-md-2 col-lg-2 brand-b-margin"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/brand18.jpg" class="img-responsive" title="" alt="" /></div>
</div>
</div>
</div>
</div>
<div class="container">
<hr>
<div class="row">
<div class="clear-margin"></div>
<div class="col-md-2  hidden-sm hidden-xs">
<img src="<?php echo Yii::app()->request->baseUrl; ?>/images/fair.jpg" class="img-responsive"  alt="" title=""/>
</div>
<div class="col-md-4  hidden-sm">
<img src="<?php echo Yii::app()->request->baseUrl; ?>/images/florals.jpg" class="img-responsive"  alt="" title=""/>
</div>
<div class="col-md-2 hidden-sm hidden-xs">
<img src="<?php echo Yii::app()->request->baseUrl; ?>/images/boss.jpg" class="img-responsive"  alt="" title=""/>
</div>
<div class="col-md-4  hidden-sm">
<div class="row">
<div class="col-md-12"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/baby-care.jpg" class="img-responsive"  alt="" title=""/></div>
</div>
<div class="row">
<div class="col-md-12"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/woodland1.jpg" class="img-responsive"  alt="" title=""/></div>
</div>
</div>
</div>
</div> 
  </body>
</html>
