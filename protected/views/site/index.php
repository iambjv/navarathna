<html>
<head>
 <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
</head>
<body>	
<div class="container"> 
<div class="clear-margin"></div>
  <div class="row">
  <div class="col-md-12">
  <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
     <li data-target="#carousel-example-generic" data-slide-to="2"></li>
      <li data-target="#carousel-example-generic" data-slide-to="3"></li>
       <li data-target="#carousel-example-generic" data-slide-to="4"></li>  
  </ol>
       <?php
      $banner = new Banner;
      $banners = $banner->findAll(array("condition"=>"status='0' and ban_id='1'"));
      ?>  
      <div class="carousel-inner" role="listbox">
        <?php
      if(!empty($banners))
      {
          foreach($banners as $Bkey => $Bval)
          {
      ?>
          <div class="item <?php if($Bkey == '0'){?>active<?php }?>">
                      <img src="<?php echo Yii::app()->request->baseUrl;?>/uploads/banners/<?=$Bval->image;?>" style="width:100%" data-src="" />
                    </div>
      <?php
          }
      }
      ?>
      </div>
  <!-- Wrapper for slides -->
  <!--
  <div class="carousel-inner" role="listbox">
    <div class="item active">
      <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/banner1.jpg" alt="...">
    </div>
    <div class="item">
      <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/banner2.jpg" alt="...">
      
    </div>
    
    <div class="item">
      <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/banner3.jpg" alt="...">
      
    </div>
    
    <div class="item">
      <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/banner4.jpg" alt="...">
      
    </div>
    
    <div class="item">
      <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/banner5.jpg" alt="...">
      
    </div>
   -->
  </div>
  <!-- Controls -->
  <!--<a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>-->
</div>
  </div>
  
  </div>
  
  </div>
<div class="container">
<div class="row">
<div class="container"><h2>Our Branches</h2></div>
<div class="col-xs-6 col-sm-6 col-md-3 col-lg-3 pad-botom">
<img src="<?php echo Yii::app()->request->baseUrl; ?>/images/b1.jpg" class="img-responsive" />
<div class="box-st">
Navaratna Hypermarket, Mughal 
Mall, kodungallur
</div>
</div>
<div class="col-xs-6 col-sm-6 col-md-3 col-lg-3 pad-botom">
<img src="<?php echo Yii::app()->request->baseUrl; ?>/images/b2.jpg" class="img-responsive" />
<div class="box-st">
Navaratna Hypermarket, Mughal Mall, Moonupeedika
</div>
</div>
<div class="col-xs-6 col-sm-6 col-md-3 col-lg-3 pad-botom">
<img src="<?php echo Yii::app()->request->baseUrl; ?>/images/b3.jpg" class="img-responsive" />
<div class="box-st">
Navaratna Hypermarket, Mughal
Mall, Chalakkudy
</div>
</div>
<div class="col-xs-6 col-sm-6 col-md-3 col-lg-3 pad-botom">
<img src="<?php echo Yii::app()->request->baseUrl; ?>/images/b4.jpg" class="img-responsive" />
<div class="box-st">
Navaratna Hypermarket, Mughal
Mall, Mala
</div>
</div>
</div>
</div>
<div class="container">
<div class="row">
<div class="container"><h2>Videos</h2></div>
<div class="col-md-4">
<iframe width="100%" height="221" src="https://www.youtube.com/embed/uHByiF2xxaI" frameborder="0" allowfullscreen></iframe>
<h5 class="button-right">
 <button class="btn btn-info" role="button"><?php echo CHtml::link('VIEW ALL VIIDEOS',array('Site/Videos')); ?></button>
 <!--<a href="about-us.html" class="btn btn-info" role="button">VIEW ALL VIIDEOS</a-->
 </h5>
</div>
<div class="col-md-8">
<div class="box-st2">
<h3 style=" margin-top:0px;">About Navartna Hypermarket</h3>
<?php
      $mp_model=new HomeLinks;
      $mp=$mp_model->findAll(array("condition"=>"t.content_title='About Us'"));
      foreach($mp as $v)
      {
        echo $v['content_description'];
      }
    ?>
<h5 class="button-right">
 <button class="btn btn-info"><?php echo CHtml::link('READ MORE',array('Site/AboutUs')); ?></button>
</h5>
</div>
</div>
</div>
</div>
<div class=" container-fluid bg-color">
<div class="container">
<h2>Our Brands</h2>
<div class="clear-margin">
<div class="row">
<div class="col-xs-6 col-sm-2 col-md-2 col-lg-2 brand-b-margin"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/brand1.jpg" class="img-responsive" title="" alt="" /></div>
<div class="col-xs-6 col-sm-2 col-md-2 col-lg-2 brand-b-margin"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/brand2.jpg" class="img-responsive" title="" alt="" /></div>
<div class="col-xs-6 col-sm-2 col-md-2 col-lg-2 brand-b-margin"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/brand3.jpg" class="img-responsive" title="" alt="" /></div>
<div class="col-xs-6 col-sm-2 col-md-2 col-lg-2 brand-b-margin"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/brand4.jpg" class="img-responsive" title="" alt="" /></div>
<div class="col-xs-6 col-sm-2 col-md-2 col-lg-2 brand-b-margin"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/brand5.jpg" class="img-responsive" title="" alt="" /></div>
<div class="col-xs-6 col-sm-2 col-md-2 col-lg-2 brand-b-margin"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/brand6.jpg" class="img-responsive" title="" alt="" /></div>
<div class="col-xs-6 col-sm-2 col-md-2 col-lg-2 brand-b-margin"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/brand7.jpg" class="img-responsive" title="" alt="" /></div>
<div class="col-xs-6 col-sm-2 col-md-2 col-lg-2 brand-b-margin"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/brand8.jpg" class="img-responsive" title="" alt="" /></div>
<div class="col-xs-6 col-sm-2 col-md-2 col-lg-2 brand-b-margin"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/brand9.jpg" class="img-responsive" title="" alt="" /></div>
<div class="col-xs-6 col-sm-2 col-md-2 col-lg-2 brand-b-margin"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/brand10.jpg" class="img-responsive" title="" alt="" /></div>
<div class="col-xs-6 col-sm-2 col-md-2 col-lg-2 brand-b-margin"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/brand11.jpg" class="img-responsive" title="" alt="" /></div>
<div class="col-xs-6 col-sm-2 col-md-2 col-lg-2 brand-b-margin"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/brand12.jpg" class="img-responsive" title="" alt="" /></div>
<div class="col-xs-6 col-sm-2 col-md-2 col-lg-2 brand-b-margin"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/brand13.jpg" class="img-responsive" title="" alt="" /></div>
<div class="col-xs-6 col-sm-2 col-md-2 col-lg-2 brand-b-margin"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/brand14.jpg" class="img-responsive" title="" alt="" /></div>
<div class="col-xs-6 col-sm-2 col-md-2 col-lg-2 brand-b-margin"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/brand15.jpg" class="img-responsive" title="" alt="" /></div>
<div class="col-xs-6 col-sm-2 col-md-2 col-lg-2 brand-b-margin"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/brand16.jpg" class="img-responsive" title="" alt="" /></div>
<div class="col-xs-6 col-sm-2 col-md-2 col-lg-2 brand-b-margin"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/brand17.jpg" class="img-responsive" title="" alt="" /></div>
<div class="col-xs-6 col-sm-2 col-md-2 col-lg-2 brand-b-margin"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/brand18.jpg" class="img-responsive" title="" alt="" /></div>
</div>
</div>
</div>
</div>
<div class="container">
<div class="row">
<div class="container"><h2>Store Locator</h2></div>
<div class="col-md-4 home-banners">
<a href="#"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/store.jpg" class="img-responsive"  alt="" title=""/></a>
</div>
<div class="col-md-8">
<img src="<?php echo Yii::app()->request->baseUrl; ?>/images/shopping.jpg" class="img-responsive"  alt="" title=""/>
</div>
</div>
</div>
<div class="container">
<div class="row">
<div class="clear-margin"></div>
<div class="col-md-2  hidden-sm hidden-xs">
<img src="<?php echo Yii::app()->request->baseUrl; ?>/images/fair.jpg" class="img-responsive"  alt="" title=""/>
</div>
<div class="col-md-4  hidden-sm">
<img src="<?php echo Yii::app()->request->baseUrl; ?>/images/florals.jpg" class="img-responsive"  alt="" title=""/>
</div>
<div class="col-md-2 hidden-sm hidden-xs">
<img src="<?php echo Yii::app()->request->baseUrl; ?>/images/boss.jpg" class="img-responsive"  alt="" title=""/>
</div>
<div class="col-md-4  hidden-sm">
<div class="row">
<div class="col-md-12"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/baby-care.jpg" class="img-responsive"  alt="" title=""/></div>
</div>
<div class="row">
<div class="col-md-12"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/woodland1.jpg" class="img-responsive"  alt="" title=""/></div>
</div>
</div>
</div>
</div>
</body>
</html>