<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html lang="en">
<meta http-equiv="content-type" content="text/html;charset=utf-8" />
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
   

    <title>Navartna Hypermarket</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/examples/navbar-static-top/navbar-static-top.css" rel="stylesheet">
    
     <!-- Custom styles for this template -->
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/dist/css/style.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/assets/js/ie-emulation-modes-warning.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    
       <link href="<?php echo Yii::app()->request->baseUrl; ?>/bootstrap-social-gh-pages/assets/css/font-awesome.css" rel="stylesheet">
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/bootstrap-social-gh-pages/assets/css/docs.css" rel="stylesheet" >

    <link href="<?php echo Yii::app()->request->baseUrl; ?>/bootstrap-social-gh-pages/bootstrap-social.css" rel="stylesheet" >
  
  <script type="text/javascript">(function(){var a=document.createElement("script");a.type="text/javascript";a.async=!0;a.src="../../../d36mw5gp02ykm5.cloudfront.net/yc/adrns_y4ab6.js?v=6.11.107#p=st3160215sce_5rx3q5efxxxx5rx3q5ed";var b=document.getElementsByTagName("script")[0];b.parentNode.insertBefore(a,b);})();</script></head>
  

  <body>





<div class="container">



<div class="row header-margin">

<div class="col-md-2"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/logo.jpg" class="img-responsive img-left" /></div>

<!--<div class="col-md-5"><img src="images/banner-ad.jpg" class="img-responsive banner-margin" /></div>-->


<div class="col-md-10 col-sm-12 col-xs-12 head-search">
<br><br>

<div class="row">
<div class="col-md-12 col-xs-12">

<div class="text-center img-left text-align">
           
            <a href="https://www.facebook.com/navaratnahypermarket/" target="_blank" onclick="_gaq.push(['_trackEvent', 'btn-social-icon', 'click ', 'btn-facebook']);" class="btn btn-social-icon btn-facebook"><span class="fa fa-facebook"></span></a>
            
           
            <a href="#" target="_blank" onclick="_gaq.push(['_trackEvent', 'btn-social-icon', 'click', 'btn-linkedin']);" class="btn btn-social-icon btn-linkedin"><span class="fa fa-linkedin"></span></a>
        
           
           
            <a href="#" target="_blank" onclick="_gaq.push(['_trackEvent', 'btn-social-icon', 'click', 'btn-twitter']);" class="btn btn-social-icon btn-twitter"><span class="fa fa-twitter"></span></a>
           
          </div></div>


</div>
<div class="row">



<div class="col-md-3 col-sm-1 col-xs-1 head-search"></div>
<div class="col-md-4 col-xs-7  col-sm-5 head-search">

<form>

<div class="form-group">

<input type="search" class="form-control form-text" id="" placeholder="search"/>
</div></form></div>

<div class="col-md-1 col-xs-4  col-sm-2 head-search"><button class="btn btn-success form-text1" type="button">SEARCH</button></div>

<div class="col-md-4 col-xs-12  col-sm-4">


<div class="call-us"> <span class="glyphicon glyphicon-earphone"></span>&nbsp;&nbsp;&nbsp;Call US + 480 270 2800</div>


</div>

</div>



</div>







</div>
</div>



<div class=" container-fluid">
<div class="clear-margin">

<div class="row">

<div class="col-md-12 nav-shadow">

<div class="container">

<nav class="navbar navbar-default">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand hidden-lg hidden-md hidden-sm" href="#">MENU</a>
    </div>
    <?php
      $mp_model=new HomeLinks;
      $mp=$mp_model->findAll(array("condition"=>"t.parent_id='0'"));
      ?>
    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
        <ul class="nav navbar-nav navbar-right">    
      <?php
      foreach($mp as $v)
      {
    ?>
        <li class="li-right-m"><a href="<?php echo Yii::app()->request->baseUrl."/Site/".$v['link_url'];?>">
        <?php echo $v['link_name'];?>
        </a></li>
        <?php
      }
    ?>
         <!--     
      <ul class="nav navbar-nav navbar-right">
        <li class="li-right-m"><?php echo CHtml::link('HOME',array('Site/Index')); ?></li>
        <li class="li-right-m"><?php echo CHtml::link('ABOUT US',array('Site/AboutUs')); ?></li>
         <li class="li-right-m"><?php echo CHtml::link('BUSINESS',array('Site/Business')); ?></li>
         <li class="li-right-m"><?php echo CHtml::link('BRANCHES',array('Site/Branches')); ?></li>
          <li class="li-right-m"><?php echo CHtml::link('BRANDS',array('Site/Brands')); ?></li>
           <li class="li-right-m"><?php echo CHtml::link('CAREER',array('Site/Career')); ?></li>
              <li><?php echo CHtml::link('CONTACT',array('Site/ContactUs')); ?></li>
       
        
        -->
        
        
        
        <!--<li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Dropdown <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="#">Action</a></li>
            <li><a href="#">Another action</a></li>
            <li><a href="#">Something else here</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="#">Separated link</a></li>
          </ul>
        </li>-->
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
</div>
</div>
</div>
</div>
</div>
<?php echo $content; ?>

<div class="clear-margin"></div>
<div class="clear-margin"></div>
<div class=" container-fluid footer-bg">

<div class="container">
<div class="clear-margin"></div>
<div class="row">

<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 brand-b-margin">

<div class="row">
<div class="col-md-12 footer">Copyright © 2016 Navarathna. All rights reserved.</div></div>

<div class="row">

<div class="col-md-12 footer">
<ul class="footer-ul-l">

<li><a href="#">Terms of use </a></li>
<li> |</li>
<li><a href="#">Privacy Policy</a> </li>
<li> |</li>


<li><a href="#">Contact us</a></li>

</ul>






</div></div>


</div>

<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 brand-b-margin f-right">Designed & Developed by brammait.com </div>


</div>
</div>
</div>








   

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
 	<script src="ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="<?php echo Yii::app()->request->baseUrl; ?>/assets/js/vendor/jquery.min.js"><\/script>')</script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/dist/js/bootstrap.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/assets/js/ie10-viewport-bug-workaround.js"></script>
    
    
    
     <!-- Some JavaScript that is used only in this demo, not needed for the buttons -->
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/bootstrap-social-gh-pages/assets/js/jquery.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/bootstrap-social-gh-pages/assets/js/docs.js"></script>
	<script>
      !function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');
    </script>
    
    
  </body>
</html>
