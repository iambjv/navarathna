<?php
/**
 * This is the model class for table "product".
 *
 * The followings are the available columns in table 'product':
 * @property string $product_id
 * @property integer $pdt_category
 */
class Packages extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public $image;
	 
	 
	public function tableName()
	{
		return 'packages';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
		    array('main_category_id,sub_category_id,package_title,package_image,days, nights, package_price, package_price_in', 'required'),
			array('main_category_id,sub_category_id', 'numerical', 'integerOnly'=>true),
			array('package_image', 'file', 'types'=>'jpg, gif, png', 'allowEmpty'=>true,'on'=>'update'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('package_id, main_category_id, sub_category_id, package_title,package_image, package_price, package_price_in', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		  'main_category'=>array(self::BELONGS_TO, 'PackageMainCategory', 'main_category_id', "select"=>"main_category.main_category_id,main_category.main_category_name" ),
		  'sub_category'=>array(self::BELONGS_TO, 'PackageSubCategory', 'sub_category_id', "select"=>"sub_category.sub_category_id,sub_category.sub_category_name" ),
		  'package_days'=>array(self::HAS_MANY, 'PackageDays', 'package_id'),
 
		//  'imagesdq'=>array(self::HAS_ONE, 'ProductImage', 'product_id',"order"=>"imagesdq.priority is null,imagesdq.priority asc , imagesdq.id desc  ","condition"=>"imagesdq.status='1'"),
		//  'imagesdq1'=>array(self::HAS_MANY, 'ProductImage', 'product_id',"order"=>"imagesdq1.priority is null,imagesdq1.priority asc , imagesdq1.id desc  ","condition"=>"imagesdq1.status='1'"),
		  

		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'package_id' => 'Package',
			'main_category_id' => 'Package Main Category',
			'sub_category_id' => 'Package Sub Category',
			'package_title' => 'Package Title',
			'days' => 'No of Days/Nights',
			'nights' => 'No of nights',
			'package_route' => 'Package Route',
			'package_facilities' => 'Package Facilities',
			'package_image' => 'Package Image',
			'package_price' => 'Package Price', 
			'package_price_in' => 'Package Price', 
			'package_description' => 'Package Description',
			'package_offer_description' => 'Package Route',
			'what_to_see' => 'What To See',
			'what_to_do' => 'What To Do',
			'inclusion' => 'Inclusion',
			'exclusion' => 'Exclusion',
			'package_meta_title' => 'Title',
			'package_meta_keyword' => 'Keyword',
			'package_meta_description' => 'Description',
		);
	}
	

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('package_id',$this->package_id,true);
		$criteria->compare('package_main_category',$this->package_main_category);
		$criteria->compare('package_sub_category',$this->package_sub_category);
		$criteria->compare('package_title',$this->package_title);
		$criteria->compare('days',$this->days);
		$criteria->compare('nights',$this->nights);
		$criteria->compare('package_route',$this->package_route);
		$criteria->compare('package_facilities',$this->package_facilities,true);
		$criteria->compare('package_image',$this->package_image);
		$criteria->compare('package_price',$this->package_price);
		$criteria->compare('package_description',$this->package_description);
		$criteria->compare('package_offer_description',$this->package_offer_description);	
		$criteria->compare('package_meta_title',$this->package_mata_title,true);
		$criteria->compare('package_meta_keyword',$this->package_mata_keyword,true);		
		$criteria->compare('package_meta_description',$this->package_mata_description,true);	
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Product the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	public function listWithStatus()
	{
		return $this->findAll(array("condition"=>"status='1'","order"=>"package_title ASC"));
	}
}
