<?php

/**
 * This is the model class for table "contact".
 *
 * The followings are the available columns in table 'contact':
 * @property integer $package_booking_id
 * @property string $name
 * @property string $email
 * @property string $phone
 * @property string $comments
 */
class PackageBookings extends CActiveRecord
{
	public $verifyCode;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'package_bookings';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('package_id,name, email, country_id,arrival_date,departure_date,adults,', 'required'),
			array('name, email', 'length', 'max'=>250),
			array('email','email'),
			//array('arrival_date','checkDate'),
			//array('departure_date','checkDate1'),
			array('phone','phoneNumber'),
			//array('contact_number', 'number'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('package_booking_id,package_id,name,email,phone, arrival_date, departure_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'package_booking_id' => 'Package Booking',
			'package_id' => 'Package',
			'name' => 'Name',
			'email' => 'Email',
			'phone' => 'Phone/Mobile No.',
			'arrival_date'=>'Arrival Date',
			'departure_date'=>'Departure Date',
			'comments' => 'Comments (if any)',	
			'adults' => 'Adults',
			'childs'=>'Child',
			'verifyCode'=>'Verification Code'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('package_booking_id',$this->package_booking_id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('phone',$this->phone,true);
		$criteria->compare('comments',$this->comments,true);
		$criteria->compare('arrival_date',$this->arrival_date,true);
		$criteria->compare('departure_date',$this->departure_date,true);
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Contact the static model class
	 */
 
	 public function phoneNumber($attribute,$params='')
	{
		
	if(preg_match("/^\(?\d{3}\)?[\s-]?\d{3}[\s-]?\d{4}$/",$this->$attribute) === 0)
	{   
	$this->addError($attribute,
	'Not a valid phone number' );  
	}   
	}
	public function checkDate($attribute,$params='') 
      {
            $strtime=strtotime($this->arrival_date);
            
            $newtime=strtotime(date('d-m-Y'));
            if($strtime<=$newtime) {
                $this->addError('arrival_date','Please select a valid date');
            }
      }
      public function checkDate1($attribute,$params='') 
      {
            $strtime=strtotime($this->departure_date);
            
            $newtime=strtotime(date('d-m-Y'));
            if($strtime<=$newtime) {
                $this->addError('departure_date','Please select a valid date');
            }
      }
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
}
