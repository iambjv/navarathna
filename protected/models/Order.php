<?php

/**
 * This is the model class for table "order".
 *
 * The followings are the available columns in table 'order':
 * @property integer $order_id
 * @property string $name
 * @property integer $pin
 * @property string $address
 * @property string $town
 * @property integer $district
 * @property integer $state
 * @property string $mobile
 * @property string $order_date
 * @property integer $status
 */
class Order extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'order';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, pin, address, town, district, state, mobile,userid,defaultaddress ', 'required'),
			array('pin, district, state, status,refererid', 'numerical', 'integerOnly'=>true),
			array('name, town', 'length', 'max'=>150),
			array('pin', 'length', 'max'=>6,'min'=>6),
			array('address', 'length', 'max'=>600),
			array('mobile','phoneNumber'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('order_id, name, pin, address, town, district, state, mobile, order_date, status,totalamount,defaultaddress,refererid', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		'orderuser'=>array(self::BELONGS_TO, 'SiteUser', 'userid'),
		'refereuser'=>array(self::BELONGS_TO, 'SiteUser', 'refererid'),
		'sta'=>array(self::BELONGS_TO, 'States', 'state'),
		'dist'=>array(self::BELONGS_TO, 'Districts', 'district'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'order_id' => 'Order',
			'name' => 'Name',
			'pin' => 'Pincode',
			'address' => 'Address',
			'town' => 'Town',
			'district' => 'District',
			'state' => 'State',
			'mobile' => 'Mobile',
			'order_date' => 'Order Date',
			'status' => 'Status',
			'defaultaddress' =>'Make this default delivery Address' ,
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('order_id',$this->order_id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('pin',$this->pin);
		$criteria->compare('address',$this->address,true);
		$criteria->compare('town',$this->town,true);
		$criteria->compare('district',$this->district);
		$criteria->compare('state',$this->state);
		$criteria->compare('mobile',$this->mobile,true);
		$criteria->compare('order_date',$this->order_date,true);
		$criteria->compare('status',$this->status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Order the static model class
	 */
	 public function phoneNumber($attribute,$params='')
	{
	if(preg_match("/^\(?\d{3}\)?[\s-]?\d{3}[\s-]?\d{4}$/",$this->$attribute) === 0)
	{   
	$this->addError($attribute,
	'Not a valid phone number' );  
	}   
	}
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
