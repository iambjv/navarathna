<?php
/**
 * This is the model class for table "product".
 *
 * The followings are the available columns in table 'product':
 * @property string $product_id
 * @property integer $pdt_category
 */
class PackageDays extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public $image;
	 
	 
	public function tableName()
	{
		return 'package_days';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
		    array('package_id,package_day_title,short_description', 'required','except'=>'search2'),
			array('package_id,priority', 'numerical', 'integerOnly'=>true),
			
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('package_day_id, package_day_title,short_description', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		  'package'=>array(self::BELONGS_TO, 'Packages', 'package_id', "select"=>"package.package_id,package.package_title,package.sub_category_id"),
		  'imagesdq'=>array(self::HAS_MANY, 'PackageImage', 'package_day_id',"order"=>"imagesdq.priority is null,imagesdq.priority asc , imagesdq.id desc  ","condition"=>"imagesdq.status='1'"),
		  'imagesdq1'=>array(self::HAS_MANY, 'PackageImage', 'package_day_id',"order"=>"imagesdq1.priority is null,imagesdq1.priority asc , imagesdq1.id desc  ","condition"=>"imagesdq1.status='1'"),
		  'imagehotel'=>array(self::HAS_MANY, 'PackageHotelImage', 'package_day_id',"order"=>"imagehotel.priority is null,imagehotel.priority asc , imagehotel.package_hotel_id desc  ","condition"=>"imagehotel.status='1'"),


		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'package_day_id' => 'Package Day',
			'package_id' => 'Package',
			'package_day_title' => 'Package Day Title',
			'package_day_image' => 'Package Day Image',
			'short_description' => 'Short Description',
			'end_description' => 'End Description',
	 
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('package_day_id',$this->package_day_id,true);
		$criteria->compare('package_id',$this->package_id);
		$criteria->compare('package_day_title',$this->package_day_title);
		$criteria->compare('package_day_image',$this->package_day_image);
		$criteria->compare('short_description',$this->short_description);
		$criteria->compare('end_description',$this->end_description);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Product the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
