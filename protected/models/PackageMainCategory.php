<?php

/**
 * This is the model class for table "product_category".
 *
 * The followings are the available columns in table 'product_category':
 * @property integer $cat_id
 * @property string $category_name
 * @property string $main_image
 * @property string $main_desc
 * @property integer $priority
 * @property string $status
 */
class PackageMainCategory extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'package_main_category';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('main_category_name, main_image', 'required'),
			array('main_category_name', 'length', 'max'=>150),
			array('status', 'length', 'max'=>1),
			array('main_image', 'file', 'types'=>'jpg, gif, png', 'allowEmpty'=>true,'on'=>'update'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('main_category_id, main_category_name, main_image, main_desc, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		  'sub_category'=>array(self::BELONGS_TO, 'PackageSubCategory', 'main_category_id', "select"=>"sub_category.sub_category_id,sub_category.sub_category_name" ),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'main_category_id' => 'Main Category',
			'main_category_name' => 'Main Category Name',
			'main_image' => 'Main Category Image',
			'main_desc'=>'Main Category Description',
			'status' => 'Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('main_category_id',$this->main_category_id);
		$criteria->compare('main_category_name',$this->main_category_name,true);
		$criteria->compare('main_image',$this->main_image,true);
		$criteria->compare('main_desc',$this->main_desc,true);
		$criteria->compare('status',$this->status,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ProductCategory the static model class
	 */
	public function listWithStatus()
	{
		return $this->findAll(array("condition"=>"status='1'","order"=>"main_category_name ASC"));
	}
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
