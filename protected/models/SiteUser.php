<?php

/**
 * This is the model class for table "site_user".
 *
 * The followings are the available columns in table 'site_user':
 * @property integer $user_id
 * @property string $email
 * @property string $first_name
 * @property string $last_name
 * @property string $password
 * @property string $address
 * @property string $phone
 * @property string $status
 */
class SiteUser extends CActiveRecord
{
	
	public $conpassword;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'site_user';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('email, first_name, password', 'required', 'except'=>'login'),
			array('email, password', 'required','on'=>'login'),
			array('email, first_name, last_name, password', 'length', 'max'=>150),
			array('email','unique','on'=>'insert,update,register'),
			array('email','email'),
			array('phone', 'phoneNumber', 'on'=>'register'),
			array('first_name,last_name','match','pattern'=>'/^[a-zA-Z\s]+$/','message'=>'Accept only word characters'),
		//	array('conpassword,password', 'compare', 'compareAttribute'=>'password','on'=>'insert'),
			array('password', 'length', 'min'=>5),
			array('phone', 'length', 'max'=>15),
			array('first_name', 'length', 'min'=>4),
			//array('status', 'length', 'max'=>1),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('user_id, email, first_name,last_name, password, address, phone, status,identifier', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'user_id' => 'User',
			'email' => 'Email',
			'first_name' => 'Name',
			'last_name' => 'Last Name',
			'password' => 'Password',
			'address' => 'Address',
			'phone' => 'Phone',
			'status' => 'Status',
			'conpassword'=>'Password',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('first_name',$this->first_name,true);
		$criteria->compare('last_name',$this->last_name,true);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('address',$this->address,true);
		$criteria->compare('phone',$this->phone,true);
		$criteria->compare('status',$this->status,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	public function cheakpass($email,$pass)
	{
		$main=$this->find('email=?',array($email));
	if($main===NULL)
	  { $this->addError('email',"Invalid Email Or Password");
	    $this->addError('password',"Invalid Email or Password");
	     return; }
	else
	  { if($main->password!= md5($pass))
		      return $this->addError('password',"Invalid Password");
		  else
		  {
			  Yii::app()->user->setState("userid",$main['user_id']);
			  Yii::app()->user->setState("siteuser",$main['first_name'].'&nbsp;'.$main['last_name']);
			   return true; 
		  }      
        }
     }


	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return SiteUser the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	public function phoneNumber($attribute,$params='')
	{
	if(preg_match("/^\(?\d{3}\)?[\s-]?\d{3}[\s-]?\d{4}$/",$this->$attribute) === 0)
	{   
	$this->addError($attribute,
	'Not a valid phone number' );  
	}   
	}
	
}
