<?php 
class Banner extends CActiveRecord
{
    public $image;
    // ... other attributes
 public function tableName()
	{
		return 'banners';
	}
	
    public function rules()
    {
        return array(
            array('image', 'file', 'types'=>'jpg, gif, png'),
            array('ban_id,url', 'required'),
            array('image', 'required')
			        );
    }
    
    public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'ban_id' => 'Select Banner Posistion',
			'image'=>'Uplode Banner image',
			);
	}
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		'banner'    => array(self::BELONGS_TO, 'Banner_pos','ban_id'),
		);
	}
	
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ban_id',$this->ban_id);
		$criteria->compare('image',$this->image,true);
		$criteria->compare('status',$this->status,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	
}
?>
