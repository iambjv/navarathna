<?php

/**
 * This is the model class for table "product_category".
 *
 * The followings are the available columns in table 'product_category':
 * @property integer $cat_id
 * @property string $category_name
 * @property integer $priority
 * @property string $status
 */
class PackageSubCategory extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'package_sub_category';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('sub_category_name', 'required'),
			array('sub_category_name', 'length', 'max'=>150),
			array('status', 'length', 'max'=>1),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('sub_category_id,main_categoru_id, sub_category_name, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		  'PackageMainCategory'=>array(self::BELONGS_TO, 'PackageMainCategory', 'main_category_id', "select"=>"PackageMainCategory.main_category_id,PackageMainCategory.main_category_name" ),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'sub_category_id' => 'Sub Category',
			'main_category_id' => 'Main Package Category',
			'sub_category_name' => 'Sub Category Name',
			'sub_category_icon' => 'Sub Category Icon',
			'meta_title' => 'Title',
			'meta_keyword' => 'Keyword',
			'meta_description' => 'Description',
			'status' => 'Status',
			'priority' => 'Priority',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('sub_category_id',$this->sub_category_id);		
		$criteria->compare('main_category_id',$this->main_category_id);
		$criteria->compare('sub_category_name',$this->sub_category_name,true);
		$criteria->compare('sub_category_icon',$this->sub_category_icon,true);
		$criteria->compare('meta_title',$this->mata_title,true);
		$criteria->compare('meta_keyword',$this->mata_keyword,true);		
		$criteria->compare('meta_description',$this->mata_description,true);
		$criteria->compare('status',$this->status,true);
		$criteria->compare('priority',$this->status,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ProductCategory the static model class
	 */
	public function listWithStatus()
	{
		return $this->findAll(array("condition"=>"status='1'","order"=>"sub_category_name ASC"));
	}
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
