<?php

/**
 * This is the model class for table "user".
 *
 * The followings are the available columns in table 'user':
 * @property integer $id
 * @property string $username
 * @property string $password
 * @property string $first_name
 * @property string $last_name
 * @property string $email
 * @property string $role
 */
class User extends CActiveRecord
{
	public $conpassword;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'user';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('username,  first_name, last_name, email, role', 'required','except'=>'login,changepassword'),
			array('username,password', 'required','on'=>'login'),
			array('conpassword,password','compare','compareAttribute'=>'password','on'=>'changepassword'),
			array('image', 'file', 'types'=>'jpg, gif, png','allowEmpty'=>true),
			array('password,conpassword', 'required','on'=>'insert'),
			array('username, password', 'length', 'max'=>200),
			array('status', 'length', 'max'=>1),
			array('email','email'),
			array('email,username','unique','on'=>'insert,update' ),
			array('first_name,last_name', 'match', 'pattern' => '/^[a-zA-Z\s]+$/','message' => 'Invalid characters  .'),
			array('conpassword,password', 'compare', 'compareAttribute'=>'password','on'=>'insert'),
			array('first_name, last_name, email', 'length', 'max'=>150),
			array('role', 'length', 'max'=>1),
			array('password', 'length', 'min'=>5),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, username, password, first_name, last_name, email, role', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'username' => 'Username',
			'password' => 'Password',
			'first_name' => 'First Name',
			'last_name' => 'Last Name',
			'email' => 'Email',
			'role' => 'Role',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('username',$this->username,true);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('first_name',$this->first_name,true);
		$criteria->compare('last_name',$this->last_name,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('role',$this->role,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	public function checklogin($username,$password)
	{
		$user=$this->find('username=?',array($username));
		if($user===null)
			return $this->addError('username', 'Username invalid');
		else if($user['password']!=md5($password))
			return $this->addError('password', 'Password invalid');
		else
		{
			 
			 Yii::app()->user->setState("roleid",$user['id']);
			 Yii::app()->user->setState("name",$user['first_name'].'&nbsp;'.$user['last_name']);
			 Yii::app()->user->setState("role",$user['role']);
			 Yii::app()->user->setState("image",$user['image']);
			 return true; 
		}
		 
	}
	 

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return User the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
